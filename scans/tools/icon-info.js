#!/bin/node

const
    pandoc=require('pandoc-filter'),

    append_tex = function( val ) {
        val.unshift( pandoc.RawBlock( 'tex', '\\vfill {\\tiny ' ) );
        val.push( pandoc.RawBlock( 'tex', '}' ) );

        return val;
    },

    uline = function( val ) {
        val.unshift( pandoc.RawInline( 'tex', '\\uline{' ) );
        val.push( pandoc.RawInline( 'tex', '}' ) );

        return val;
    },

    action = function(type,value,format,meta) {
        if (type === 'Div' && value[0][1].indexOf('icon-info') >= 0 ) {
            return pandoc.Div( value[0], append_tex( value[1] ) );
        } else if ( type == 'Span' && value[0][1].indexOf('uline') >= 0 ) {
            return pandoc.Span( value[0], uline( value[1] ) );
        }
    };

pandoc.stdio(action);
