#!/bin/bash -e

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

get_abs_dir() {
  # $1 : relative filename
  if [ -d "$(dirname "$1")" ]; then
    echo "$(cd "$(dirname "$1")" && pwd)"
  fi
}

get_abs_dir1() {
  # $1 : relative filename
  if [ -d "$1" ]; then
    echo "$(cd $1 && pwd)"
  else
    echo "$1"
  fi
}

get_base_name() {
    echo "$(basename $(get_abs_dir $1))"
}

pandoc_version() {
    pandoc --version|head -1
}

target_epub() {

    local VERBOSE=0
    local PANDOCOPTS=(
      "--template=${TOOLSDIR}/templates/epub3.html"
      "--css=${TOOLSDIR}/templates/epub3.css"
      -f markdown-fancy_lists+ascii_identifiers+smart
      -t epub3
      --toc
      --toc-depth=2
    )
    local MOBI=0
    local EXTRAOPTS=()
    local EPUBFILE=$BASENAME
    local JSFILTERS=(
        commit-info.js
        dagger.js        
        hardcode_quotes.js
        verse.js
        nbsp.js
        hyphen.js
    )

    while [ "$1" != "" ]; do
      case $1 in
        draft) 
            EXTRAOPTS+=(-V draft -H ${TOOLSDIR}/templates/draft.html --epub-embed-font=${TOOLSDIR}templates/draft.png)
            EPUBFILE=$BASENAME-wersja-robocza
            ;;

        verbose)
            VERBOSE=1
            EXTRAOPTS+=(--verbose)
            ;;

        mobi)
            MOBI=1
            ;;
      esac
      shift
    done

    if [ -s 'bookcnt' ]; then
        source 'bookcnt'
    else
        echo 'bookcnt is missing'
        return 2
    fi
    
    if [ -s "extraopts-$TARGET" ]; then
        source "extraopts-$TARGET"
    elif [ -s 'extraopts' ]; then
        source 'extraopts'
    fi

    if ! [ -s "metadata-$TARGET.yaml" ]; then
        echo "metadata-$TARGET.yaml is missing"
        return 3
    fi

    if [ -s "cover-$TARGET.png" ]; then
        EXTRAOPTS+=( "--epub-cover-image=cover-$TARGET.png" )
    elif [ -s 'cover.png' ]; then
        EXTRAOPTS+=( --epub-cover-image=cover.png )
    fi

    source "$TOOLSDIR/gitcommit"

    pandoc \
      ${PANDOCOPTS[@]} \
      -o "${OUTDIR}/${EPUBFILE}.epub" \
      ${EXTRAOPTS[@]} \
      -M "commithash:${GITCOMMIT}" \
      -M "commitdate:${GITDATE}" \
      -M "producer:${PANDOC_VERSION}" \
      ${JSFILTERS[@]/#/--filter=${TOOLSDIR}/filters/} \
      metadata-$TARGET.yaml \
     ${bookcnt[@]}

    if [ $MOBI -eq 1 ]; then
        kindlegen "${OUTDIR}/${EPUBFILE}.epub"
    fi
}

target_pdf() {
    local VERBOSE=0
    local ENGINE=xelatex
    local DOCUMENT_CLASS=book
    local PANDOCOPTS=(
         "--template=$TOOLSDIR/templates/template.latex"
        -f markdown-fancy_lists+ascii_identifiers+smart
        -V classoption:oneside
        -V fontsize=12pt
        --toc
        --toc-depth=2
    )
    local EXTRAOPTS=()
    local PDFFILE=$BASENAME
    local JSFILTERS=(
        commit-info.js
        ralign.js
        theend.js
        lalign.js
        subtitle.js
        dagger.js
        nbsp.js
        verse.js
        tex-links.js
        mbox.js
        romannum.js
    )

    while [ "$1" != "" ]; do
      case $1 in
        draft)
            EXTRAOPTS+=(-V classoption:draft)
            PDFFILE=$BASENAME-wersja-robocza.pdf
            ;;

        xelatex|xe)
            ENGINE=xelatex
            ;;

        lualatex|lua)
            ENGINE=lualatex
            ;;

        verbose)
            VERBOSE=1
            EXTRAOPTS+=(--verbose)
            ;;
      esac
      shift
    done

    if [ -s 'bookcnt' ]; then
        source 'bookcnt'
    else
        echo 'bookcnt is missing'
        return 2
    fi

    if ! [ -s 'geometry.yaml' ]; then
        echo 'geometry.yaml is missing'
        return 3
    fi
    
    if [ -s "extraopts-$TARGET" ]; then
        source "extraopts-$TARGET"
    elif [ -s 'extraopts' ]; then
        source 'extraopts'
    fi

    if ! [ -s "metadata-$TARGET.yaml" ]; then
        echo "metadata-$TARGET.yaml is missing"
        return 3
    fi

    if [ -s "cover-$TARGET.png" ]; then
        EXTRAOPTS+=( -V "cover-image:cover-$TARGET.png" )
    elif [ -s 'cover.png' ]; then
        EXTRAOPTS+=( -V cover-image:cover.png )
    fi

    if [ $VERBOSE -eq 1 ]; then
        echo "Base directory: $BASEDIR"
        echo "Tools directory: $TOOLSDIR"
        echo "Output directory: $OUTDIR"
        echo "Base name: $BASENAME"
        echo "Pandoc version: $PANDOC_VERSION"
        echo "Engine: $ENGINE"
        echo "Filename: $PDFFILE.pdf"
    fi

    source "$TOOLSDIR/gitcommit"

    pandoc \
        ${PANDOCOPTS[@]} \
        -V "documentclass:${DOCUMENT_CLASS}" \
        ${EXTRAOPTS[@]} \
        --pdf-engine=$ENGINE \
        -o "$OUTDIR/$PDFFILE.pdf" \
        -M "commithash:${GITCOMMIT}" \
        -M "commitdate:${GITDATE}" \
	-M "producer:${PANDOC_VERSION}" \
        ${JSFILTERS[@]/#/--filter=${TOOLSDIR}/filters/} \
        "$TOOLSDIR/libertine-font.yaml" \
        geometry.yaml \
        metadata-$TARGET.yaml \
        ${bookcnt[@]}
}

BASEDIR=$(get_abs_dir $0)
TOOLSDIR=$(get_abs_dir1 $BASEDIR/../../tools)
OUTDIR=$(get_abs_dir1 $BASEDIR/../../output)
BASENAME=$(get_base_name $0)

PANDOC_VERSION=$(pandoc_version)
TARGET=$(basename $0)
TARGET=${TARGET#make-}
EXITCODE=0

pushd "$BASEDIR"

case $TARGET in
   epub) target_epub "$@"
         EXITCODE=$?
         ;;

   pdf)  target_pdf "$@"
         EXITCODE=$?
         ;;

   *)    echo "Unknown target: $ACTION"
         EXITCODE=1
         ;;
esac

popd
exit $EXITCODE
