#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    
    shortwords = [ 'Do', 'W', 'We', 'Z', 'Za', 'Ze', 'I', 'O', 'U', 'A', 'do', 'w', 'we', 'z', 'za', 'ze', 'i', 'o', 'u', 'a'],
    nbsp = String.fromCharCode(160),
    punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~]/g,

    get_lastval = function(value) {
        if (value.t === 'Str') return value.c.replace( punctRE, '' );
        return 'xxxxxxxxxx';
    },

    str_and_space_split = function( value ) {
        if ( !Array.isArray(value) ) return false;

        var checker = function(el) {
            return el.t === 'Str' || el.t === 'Space';
        }

        var i=0, res;
        for( ;i<value.length; i++) {
            if (typeof res === 'undefined') {
                res = [ { 'str': checker(value[i]), 'c': [ value[i] ] } ];
                continue;
            }

            var lastEl = res[res.length-1];
            if ( lastEl.str ) { // str and space
                if ( checker(value[i]) ) {
                    res[res.length-1].c.push( value[i] );
                } else {
                    res.push( { 'str': false, 'c': [ value[i] ] } );
                }
                } else { // others
                if ( checker(value[i]) ) {
                    res.push( { 'str': true, 'c': [ value[i] ] } );
                } else {
                    res[res.length-1].c.push( value[i] );
                }
            }
        }

        return res;
    },

    process_nbsp = function( cnt ) {
        var res = [], el = { 't': 'Unk', 'c': [] }, lastel;

        while( cnt.length > 0 ) {
            lastel = el;
            el = cnt.shift();
            if ( el.t === 'Space' ) {
                var lastval = get_lastval( lastel );
                if ( lastval.length <= 2 && !lastval.endsWith( nbsp ) && shortwords.indexOf(lastval) >= 0 ) {
                    var prev = '', next = '';
                    if ( res.length > 0 && res[ res.length - 1].t === 'Str' ) {
                        prev = res.pop().c;
                    }
                    if ( cnt.length > 0 && cnt[0].t === 'Str' ) {
                        el = cnt.shift();
                        next = el.c;
                    }

                    if (prev.length > 0 || next.length > 0) {
                        var nel = pandoc.Str( prev.concat( nbsp ).concat( next ) );
                        res.push( nel );
                        continue;
                    }
                }
            }

            res.push( el );
        };

        return res;
    },

    append_nbsp = function(value) {
        if (value.length <= 1 ) return value;

        var spl = str_and_space_split(value), res = [];
        spl.forEach(function( el ) {
            if ( el.str ) {
                res = res.concat( process_nbsp( el.c ) );
            } else {
                res = res.concat( el.c );
            }
        });

        return res;
    },

    int_action = function(type,value,format,meta,info) {
        switch (type) {
            case 'Para':
            case 'Plain':
            case 'Emph':
            case 'Strong':
            case 'Strikeout':
            case 'Superscript':
            case 'Subscript':
            case 'SmallCaps':
            case 'BlockQuote':
            case 'Note':
                return pandoc[type]( append_nbsp( value ) );

            case 'Quoted':
                return pandoc[type]( value[0], append_nbsp( value[1] ) );

            case 'Span':
                return pandoc[type]( value[0], append_nbsp( value[1] ) );
        };
    },
    
    action = function(type,value,format,meta,info) {
        if (info.lang === 'pl') {
            return int_action(type,value,format,meta,info);
        };
    }
    
    actions = { epub3: action, html: action, latex: action };

pandocEx.stdio(actions);
