#!/bin/node

const
    pandocEx = require('./pandoc-filter'),
    pandoc = require('pandoc-filter'),
    Hypher = require('hypher'),
    
    hlangs = ["en-us","pl","it","la","fr","cs","grc"],
    
    ld_hyphers = function(langs) {
        var h = {};
        langs.forEach( function(lang) {
            var mlang = lang;
            var p = lang.indexOf('-');
            if (p >= 0)  mlang = lang.slice(0,p);
            var l = require("hyphenation."+lang);
            var hp = new Hypher(l);
            h[mlang] = hp;
        });
        return h;
    },
    
    h = ld_hyphers( hlangs ),

    action = function(type,value,format,meta,info) {
        if (type === 'Str') {
            if(info.path.indexOf('Header') < 0){
                if (h[info.lang]) {
                    return pandoc.Str( h[info.lang].hyphenateText( value ) );
                }
            }
        }
    },
    
    actions = { epub3: action, html: action };

pandocEx.stdio(actions);
