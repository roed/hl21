#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),

    zm_regex = /\b(zm\.(\s))/gu,
    subst = `\u2020$2`,
    
    action = function (type,value,format,meta, info) {
        if (info.lang === 'pl' && type === 'Str') {
            if( zm_regex.test(value) ){
                return pandoc.Str( value.replace(zm_regex,subst) );
            }
        }
    };

pandocEx.stdio(action);

