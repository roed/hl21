#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),

    action = function(type,value,format,meta) {
        if (type === 'Div' && utils.is_in_class( 'lalign', value[0] ) ) {
            return pandoc.Div( value[0],
                utils.wrap_with_tex_code( value[1],
                    '\\begin{flushleft}',
                    '\\end{flushleft}' )
                );
        }
    },
    
    actions = { latex: action };

pandocEx.stdio(actions);
