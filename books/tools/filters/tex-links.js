#!/bin/node

const
    pandoc = require('pandoc-filter'),
    pandocEx = require('./pandoc-filter'),
    utils = require('./pandoc-filter-utils'),

    action = function(type,value,format,meta) {
        if (type === 'Link' && !utils.is_in_class( 'nobold', value[0] ) ) {
            return pandoc.Link( utils.add_class( 'nobold', value[0] ),
                    utils.inline_wrap_with_tex_code( value[1], '\\textbf{', '}' ),
                    value[2] );
        }
    },
    
    actions = { latex: action };

pandocEx.stdio(actions);
