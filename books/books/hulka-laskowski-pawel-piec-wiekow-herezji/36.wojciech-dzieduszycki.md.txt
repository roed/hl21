# Wojciech Dzieduszycki {-}

## Poganizacja i reformacja {-}

Nie ulega wątpliwości, że reformacja położyła kres poganizacji Europy
sprowadzonej przez renesans, owej poganizacji, która psuła sumienia,
osłabiała wolę, rozgrzeszała wszelkie zbrodnie
i omal, że nie sprowadziła przedwczesnego zgonu cywilizacji europejskiej.
W krajach germańskiej północy rozniecono zapał religijny u ludzi,
którzy chcieli samodzielnie szukać prawdy w księgach Ewangelji,
przechodzący w szaleństwo, rodzący u ludu coraz nowe sekty,
ale sprzyjający u wykształconych badaniom naukowym i spekulacji filozoficznej,
popychający do rozpowszechnienia oświaty,
skoro każdy protestancki chrześcijanin
winien był sam samodzielnie czytać Pismo święte,
ale podkopujący wszelką ludzką powagę
i tem groźny, że nie kładł kresu krytyce.
W krajach katolickich, duchowieństwo przywołane do swojego obowiązku,
dawało nieraz przykład cnót najwyższych,
umiało rzesze do głębokiej nakłonić dewocji,
ale przerażone odszczepieństwem tylu narodów,
stało się do zbytku lękliwem,
posłusznem na każde skinienie władzy,
mogącej zwalczać herezję,
ale mogącej także cały naród popchnąć do odszczepieństwa;
bało się indywidualnych natchnień, niezawisłej nauki,
postępu nauk przyrodniczych, nawet swobodniejszej sztuki kościelnej;
utrzymywało wprawdzie wzorowe szkoły,
ale nie dopuszczając żadnej samodzielności umysłów,
powstrzymywało nieraz rozwój oświaty i sprawiło,
że kraje katolickie pozostały
po największej części w tyle za protestanckiemi,
że się trzymały nauk i formuł przestarzałych.
Protestantyzm, mimo swego religijnego ferworu,
był narażony na to, że jego wyznawcy odpadną wreszcie od chrześcijaństwa;
zaraz z początku posunął prawo krytyki osobistej aż do tego,
że odrzucił autentyczność niektórych ksiąg tak starego jak i nowego zakonu,
że zaprzeczył niektórym, powszechnie dotąd uznawanym dogmatom,
a pozostawiony sobie, mógł wreszcie i musiał dopuścić zaprzeczenia wszystkich dogmatów
i powagi całego pisma.
Katolicyzm był na pochyłości, po której krocząc samopas,
mógł poświęcić istotę chrześcijaństwa dla form zewnętrznych
a obowiązki chrześcijanina streścić
w biernem posłuszeństwie dla władzy przełożonej
i w pełnieniu skwapliwem niezliczonych obrządków.
Żadne z dwóch wyznań nie mogło już mieć tej siły
i tych zasług dziejowych, które miał kościół zachodu przed odszczepieństwem Lutra.
Jednak istniejąc obok siebie, przeszkadzały wzajemnie temu,
aby którekolwiek z nich doszło do najostateczniejszych konsekwnecyj.
Wzgląd na protestantów powstrzymywał duchowieństwo katolickie
od utonięcia w zewnętrznym formalizmie,
a przykład katolicyzmu przestrzegał narody protestanckie
przed nowinkami i zaprzeczeniami
znoszącemi istotę chrześcijaństwa.
Równoczesne obu wrogich wyznań istnienie
utrzymało zatem w Europie religję, która europejską cywilizację z siebie wydała.
Inne jeszcze spełniło zadanie w epoce absolutyzmu
i bezwzględnej przewagi państwa nad kościołem;
utrzymywało ciągłą walkę sumień i rozumów,
która przetrwała zakończenie wojen religijnych,
oddając za czasów nowszych tę samą nieocenioną przysługę,
którą wyrządziła w średnich wiekach walka kościoła z państwem.

<div class="ralign">Dokąd nam iść wypada,  
Brodny 1910, str. 102--103.</div>

## Stagnacja katolicka {-}

Kościół katolicki przejęła trwoga,
gdy się dokonało wielkie odszczepieństwo protestanckich narodów.
Wszedł w siebie, przyznał się do winy,
że się lubował w świeckiej wspaniałości i świeckiej nauce;
przywrócił karność i zreformował obyczaje duchowieństwa,
zwrócił się z zapałem ku pełnieniu różnorodnych uczynków miłosierdzia
i ku pielęgnowaniu teologicznej nauki,
ale ze śmiałego niegdyś inicjatora wielkiej nauki,
stał się lękliwym i podejrzliwym tak dalece,
że na każdym kroku hamował rozwój narodów, które pozostały wierne papiestwu.
Niegdyś dawano jawnym panteistom kapelusz kardynalski
a mimo to, że wszystkie uniwersytety kościelnemu ulegały nadzorowi,
zezwalano na nich na wykład bardzo nieprawowiemej filozofji arabskiej;
teraz zakazywano nietylko książki dogmatycznie podejrzane,
ale sprzeciwiano się uporczywie wszelkiej filozofji,
którąby się od średniowiecznej scholastyki w czemkolwiek różniła,
choćby w niczem wierze nie uwłaczała,
nakładając tem samem na myśl ludzką nieznośne pęta;
co gorsza uważano wszelki nauk przyrodniczych postęp za rzecz niebezpieczną,
przeczono systemowi Kopernika i nauce o krążeniu krwi,
dlatego tylko, że w średnich wiekach były nieznane,
a w dziewiętnastym jeszcze wieku nie chciano zrazu zezwolić na wynik badań
paleontologicznych,
sprzeciwiano się później nauce o ewolucji rodzajów organicznych,
a narażano tem samem powagę, której nadużywano, na porażki i na poniżenie.
Lękano się tak dalece herezji,
że prześladowano mistyków katolickich,
wtedy zwłaszcza, jak świeckimi byli ludźmi,
a wysadzano biurokratyczne komisje, aby zbadać, czy cuda były prawdziwe;
sztukę i poezję, o ile się tyczyły świętych przedmiotów, poddano ścisłej cenzurze,
krępując nawet artystyczne i poetyczne natchnienie,
i skazywano posłusznych katolików na oschły formalizm,
który musiał przygasić ogień religijnego uczucia.
Lękając się, żeby wszystkie państwa, na wzór Anglji,
odszczepieńczych, narodowych kościołów nie ustanowiły
pozawierano z monarchicznymi rządami układy,
w których duchownych zamieniono w zwyczajnych urzędników państwa,
strzegących bacznie istniejącego,
coraz bardziej policyjnego porządku rzeczy,
a otrzymujących wzamian dla siebie utrzymanie,
dla kościoła uznanie, że wyobraża religję państwa,
polityczne prześladowanie innowierców,
prawo cenzurowania wszystkich wydawnictw
i nadzoru nad wszystkiemi szkołami...

Stało się zatem, że pod koniec ośmnastego wieku większość
klas oświeconych, po krajach katolickich,
popisywała się jawnem niedowiarstwem,
a obsypywała kościół szyderstwami,
widziąc w nim skostniałego wszystkich zestarzałych nadużyć obrońcę.
Kto tylko chciał reform w państwie, był katolicyzmu
przeciwnikiem, a świat urzędowy opiekował się katolicyzmem
jako instytucją dla wykształconych zbyteczną,
ale zdolną motłoch utrzymywać w posłuszeństwie.
O tem musiał się także miejski przynajmniej "motłoch" dowiedzieć,
a skoro się zaczą buntować musiał kościół uczynić jednyym z głównych
przedmiotów nienawiści.

<div class="ralign">Tamże, str. 185--188.</div>

## Kościół a państwo {-}

Nie trzeba się łudzić.
Stronnictwo katolickie potrafiło dłuższe utrwalić panowanie w jednej tylko Belgji,
a to skutkiem tego, że wyrzekło się w praktyce zasad politycznych, które Kościół
w teorji broni, a mianowicie żądania, żeby się ustrój państwa
zastosował ściśle do przepisów kościelnych.
Takie żądanie wyraźnie postawione musiałoby wszędzie od Kościoła politycznego odstręczyć
wielu wierzących we wszystkie teologiczne dogmata.
Samo papiestwo wie o tem, często przeto nie zezwala na tworzenie
politycznego stronnictwa katolickiego tam nawet,
gdzieby wielkie szanse powodzenia miało.
Katolicyzm szerzy się i kwitnie w krajach, w których wszelkiego urzędowego
charakteru pozbawiony, tylko wspólnej wszystkim wyznaniom wolności używa;
w krajach, w których jest jeszcze religją urzędową albo przynajmniej przez rząd
urzędowo popartą, liczy między statystycznie zapisanymi wyznawcami swoimi wielki
zastęp ludzi obojętnych wobec wszystkiego, co się religją nazywa i nawet
jawnych niedowiarków, mnożących się dziś we wszystkich krajach katolickich,
także śród ludu wiejskiego.
Kościół stracił zupełnie swoją władzę nad państwem i nie może jej odzyskać
jak długo będzie istniał stan społeczeństw i umysłów dzisiejszy,
albo od dzisiejszego pochodny.
Istnienie katolicyzmu wcale zagrożonem nie jest;
pozostanie silnie utwierdzoną wiarą niezliczonych rzesz,
ale czasy, w których władza świecka wyznawanie katolickiej
przynajmniej religii wymuszała, minęły niepowrotnie,
a daje się przewidzieć chwila, w której pozorne należenie
do kościoła obojętnych ustanie.
We wszystkich krajach będą katolicy tworzyć liczną,
najliczniejszą zapewne sektę chrześcijańską,
ale ich wiara nie będzie nigdzie wyłączną religją społeczeństwa,
wszędzie walczyć będzie z innemi wyznaniami
i z jawnym brakiem religji tych,
którzy dzieci swoje chrzcić przestaną...

<div class="ralign">Tamże, str. 190--191.</div>

## Potęga protestantyzmu {-}

Protestantyzm mniej od katolicyzmu sprzyjał rozwojowi sztuk pięknych,
z jedynym wyjątkiem poezji;
nie stawiał zato żadnych przeszkód ani badaniom naukowym,
ani spekulacji filozoficznej i sprawił,
że narody protestanckie stanęły na czele ruchu naukowego.
Że to były narody młodsze,
w których życie lokalne i autonomiczne
posiadało niebywałą w krajach romańskich żywotność,
stały się niebawem najzamożniejszymi i w stosunku do swojej ludności dziwnie potężnymi;
gdy wreszcie wstrząśnienia rewolucyjne stanęły w jawnej sprzeczności
z wewnętrzną istotą wielkich państw katolickich,
stało się, że potęga przeszła w ręce protestantów,
a w chwili, w której piszemy,
Prusy, Anglja i Ameryka są jawnie potężniejszemi od mocarstw katolickich.
Od trzech wieków z górą mało kto z katolicyzmu na protestantyzm przechodzi,
nierównie częściej wracają protestanci na łono katolicyzmu;
mimo to stosunek pomiędzy obydwoma wyznaniami
przesunął się na korzyść protestantów także ze względu na liczbę wyznawców.

<div class="ralign">Tamże, str. 192.</div>

## Pochwała indyferentyzmu {-}

Przezwano tworzenie dowolne nowych religji,
w które nikt naprawdę nie wierzy,
i nowych systematów moralności, czy też właściwie niemoralności,
filozofją, nauką, swobodą ducha,
a naukowo stworzono stan rzeczy,
w którym niemasz ostoi ani dla ludzi, ani dla spełczeństw.

Szczęśliwy tedy naród, który jak najmniej po dzisiejszemu filozofuje
i swobody ducha takiej nie zna.
Nie filozofję wykluczam dla tych, którzy chcą żyć;
nakłaniam ich owszem do tego, aby poznali naturę ducha ludzkiego i naturę społeczeństw;
przestrzegam ich tylko przed logicznem niby wysnuwaniem mrzonek niczem nie stwierdzonych,
których przezwano dziś filozofją...

<div class="ralign">Tamże, str. 498--499.</div>

## Konieczność zgody {-}

Żyć muszą religie w sąsiedztwie bezpośredniem innych religij,
a żyć muszą w spokoju, szanując się nawzajem.
Niektóre posiadają jeszcze przywileje,
dane im wzamian za urzędniczą, niemal wobec swych państw usłużność,
ale muszą wiedzieć, że prędzej lub później te przywileje utracą,
biorąc wzamian swobodę wobec państwa.
Przemocą nie będzie żadna religja wyznawców nabywać,
nie zdoła nawet przemocą wiernych utrzymać,
i prawo świeckie nikogo za religijne odstępstwo karać nie będzie.
Coraz mniej będzie się świeckie ustawodawstwo państw
stosować do przepisów wyznaniowych, a przyjdzie chwila,
w której kościoły będą wieść życie zupełnie od życia państw niezawisłe.
Mogą kościoły potężne protestować przeciw rozdziałowi kościoła i państwa,
ale winny się przygotowywać do stanu rzeczy,
który po tym rozdziale nastanie,
starać się nieuniknioną kiedyś chwilę co najwięcej odroczyć,
aż do dnia, w którym kościół będzie gotów do życia niezawisłego,
ale także niepopartego już przez państwo,
coraz bardziej ograniczające atrybucje swoje.

<div class="ralign">Tamże, str. 502--503.</div>
