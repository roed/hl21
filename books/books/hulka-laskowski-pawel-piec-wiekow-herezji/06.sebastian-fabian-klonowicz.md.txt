# Sebastian Fabjan Klonowicz (1550--1602) {-}

## Worek Judaszów {-}

<div class="verse">
Pod lisiem podobieństwem ci się zamykają,\
Którzy się trochę niiej porząd dotykają;\
Judaszowa drużyna: Naprzód, która prosi\
"Na Boga, na ubogie", skąd korzyść odnosi;\
Sobie to przywłaszczając, co Bogu przysłusza,\
Choć się Bóg o to gniewa, nic jej to nie rusza.\
Przędzie sobie postawę, żebrze płaskim głosem,\
Włóczy się po kiermaszach z Judaszowym trzosem.\
Nosi puszkę żelazną, dzwonek mosiądzowy,\
Prosi rzkomo na szpital i na kościół nowy;\
Prosi chytry nieborak na jakiego świątka,\
Chocia z tamtej jałmużny nie da mu i szczątka.\
Czasem zmyśli na błoniu i w boru zjawienie\
I ślubuje prostakom za pewne zbawienie.\
Widziałem --- pry --- pod lasem miłą Matkę Bożą;\
(A baby się, słuchając onych baśni, trwożą).\
Wielka światłość wynikła w choinowym borku,\
Na pieńku nowo ściętym, na cudnym pagórku.\
Więc on niezbedny oszust twierdzi na rzecz istą,\
Że widział własnem okiem dziewicę przeczystą,\
Która mu rozkazała chwalę bożą mnożyć,\
I tam na onem miejscu kościołek założyć.\
Więc plecie, bredzi, mata i na on kościołek\
Nawyłudza powałek[^powalek], pieniędzy, gomołek.\
I dobrze mu wychodzi matanina ona:\
Idzie mu chleb w kobiałkę i w puszkę mamona.\
Więc też chudzi kleszkowie i książkowie prości,\
Widząc,że tak przybywa do zjawienia gości,\
Opuszczają więc podczas i kościół swój stary,\
Przenoszą się na odpust do łasa od fary,\
Udają się za chlebem za ofiarą głupią,\
Kury, jajca, szelągi, kukle, świeczki łupią.\
Pomagają prostakom po staremu błądzić,\
Nie umieją ubogich ludków dobrze rządzić,\
Ślepi wodzowie ślepych, i wpadną pospołu\
Mistrzowie i uczniowie do jednego dołu.\
Więc nie pytają starszych, jeśli to tak słusznie,\
Lecz to już konkludują, że tam ma być dusznie.\
Kto im gani te brednie, heretykiem zową,\
Świeżej wiarki człowiekiem z zaśnieconą głową.\
Choć dobrze jest katolik, zawaruj ich Boże,\
Jeśli im kto w tej wierze bredzić nie pomoże.\
<span lang="la">**Loci ordinarius**</span>[^ordinarius] własny nie wie o tem,\
Aż się więc z wielkim żalem dowiaduje potem,\
Kiedy już się nadrwili na onem zjawieniu\
Prości ludzie, z uszczerbkiem dusznemu zbawieniu.\
Gdyby nie dla zgorszenia, słyszałbyś **absurda**,\
Które za sobą niesie ona leśna burda.\
Ale żebym się nie zdał być jednym z tej roty,\
Co leda za przyczynką z kościołem drą koty,\
Wolę tu nie obrażać animusów chorych,\
Do ponawiania dawnej wiary bardzo skorych.\
Kładę to na biskupy i na stare głowy,\
Niechaj to pohamuje ich rozsądek zdrowy.\
Niech się worek Judaszów chytry nie bogaci,\
Trzeba pilnie powściągać tego cechu braci,\
Bo psują świat i dla nich cierpi kościół boży\
Przymówiska. Żadna się pobożność nie mnoży,\
Tylko, że się lud bestwi, który nowych cudów\
Zawsze pragnie i wiele podejmuje trudów\
Niepotrzebnych; przychodzi często do ubóstwa;\
Kiedy szuka dzikiego na pustyni bóstwa;\
Odbieży białogłowa krosien i kądziele,\
Nie opatrzy dobytka, marchwi nie wypiele,\
Tak nieboga samopas puści gospodarstwo,\
Już jej zawsze na myśli będzie ono łgarstwo,\
Wlecze się do zjawienia, tuszy: będę w niebie,\
Jeśli pójdę do boru o żebranym chlebie.\
Więc coby jałmużnę dać swoją, żebrze cudzej;\
Jej przykładem prostacy czynią to i drudzy...
</div>

<div class="ralign">Piotr Chmielowski, "Obraz literatury polskiej...", tom\ I, str. 235--236.</div>

[^powalek]: bochenków

[^ordinarius]: miejscowy proboszcz
