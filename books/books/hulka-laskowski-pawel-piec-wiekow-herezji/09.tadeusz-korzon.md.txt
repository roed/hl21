# Tadeusz Korzon {-}

## Chytry Possevino, chytrzejszy car {-}

Iwan Groźny zwątpił nareszcie o potędze swojej;
nowa porażka pod Toropcem stwierdziła wyższość sztuki wojennej polskiej,
utrata tylu zamków świadczyła, że ćwiczona piechota poradzić umie ze ścianami otoczonemi 3-sążniowym nasypem ziemnym.
Wtedy zdobył się na pomysł zadziwiający, jeśli zważymy, jak ubogie wiadomości o prądach umysłowych
Europy zachodniej posiadali on sam i doradcy jego:
wyprawił mianowicie Szewrygina z tłumaczem Inflantczykiem aż do Rzymu, żeby zaskarżyć Stefana Batorego,
"służebnika tureckiego" o przeciwną chrześcijaństwu zawziętość i wynurzyć życzenia,
aby papież doprowadził monarchów chrześcijańskich do sojuszu.

Tym krokiem dyplomatycznym Iwan\ [IV]{.Romannum rnum="4"} zmienił charakter wojny inflanckiej,
wprowadzając do niej interes religijny, katolicki, powszechny.
Kurja rzymska nie zapominała nigdy o zawartej
na soborze florenckim unji kościelnej (1439\ r.).
Już trzy razy w ciągu [XVI]{.Romannum rnum="16"} wieku przez pomyślnie wyprawionych do Moskwy legatów,
próbowali papieże zagaić układy w tej materji, lecz nadaremnie.
Obecnie Grzegorz\ [XIII]{.Romannum rnum="13"} widząc u siebie w Watykanie posła moskiewskiego, powziął najlepsze nadzieje:
nawrócenia cara na katolicyzm i wprowadzenia tak jego jako też Batorego do ligi chrześcijańskiej przeciwko Turcji.
Przyjął więc po ojcowsku Szewrygina i obiecał mu, że wyprawi swojego posła dla pojednania walczących monarchów.
Jakoż powierzył wielkie posłannictwo Antoniemu Possevino, znakomitemu jezuicie,
który niedawno, przed kilkoma dniami, wrócił z misjonarsko-dyplomatycznych podróży do Szwecji...

Possevino zastał króla w Wilnie wśród gromadzących się wojsk i odrazu doszedł z nim do bliskiego porozumienia.
W otoczeniu jego znalazł trzech jezuitów polskich, z których jeden owiany w Rzymie (1568)
najgorętszemi tchnieniami katolicyzmu przez spowiednika swego Ribadeneirę,
ucznia Lojoli, przez jenerała zakonu (Laineza), przez papieża św.\ Piusa [V]{.Romannum rnum="5"},
otrzymujący listy od misjonarzy japońskich, największy kaznodzieja polski,
złotousty ks.\ **Piotr Skarga Pawęski** (1536--1612) już od kilku lat wzywał wyznawców
Kościoła wschodniego do zjednoczenia się z rzymskim.
Pobożność głęboka, słuchanie codziennie mszy, dysputy poobiednie z różnowierczymi panami
ułatwiały przystęp ideom antireformacji i bojującego katolicyzmu do duszy Batorego.
Possevin uznał w nim wybrańca Pańskiego do rozszerzenia wiary prawdziwej.
Jechał za królem aż pod granicę moskiewską, naradzając się z nim i Zamojskim,
budując wojsko wymownemi kazaniami...

Possevino ufny w obietnice Iwana Groźnego i zachęcony świetnem przyjęciem, jakiego doznał w Starycy,
teraz po zawarciu pokoju, pojechał do Moskwy żeby spełnić główne zadanie swej misji.
Na pierwszem posłuchaniu w obecności bojarów i setki znaczniejszych urzędników carskich zagaił sprawę unji.
Wyrażał życzenie, aby nie było kościołów wschodniego i zachodniego, lecz jedność w Chrystusie;
uprzedzał, że nauka wiary jest w obu jednakowa; dużo mówił o soborze florenckim
i księgę uchwał jego, bogato oprawną, złożył w darze od papieża;
obiecywał, że Iwan otrzyma tytuł cesarza wschodnio-rzymskiego,
prosił wreszcie o dysputę teologiczną z duchowieństwem.
Iwan sam wdał się w rozprawę o golonej brodzie Possevina,
o dumie papieża, który każe siebie nosić na krześle i umieszczać krzyż na nodze swojej,
przyczem unosząc się gniewem, nazwał go nie pasterzem, lecz wilkiem.
Zrywał się nawet z siedzenia, potrząsając historycznym kosturem swoim;
obecni bojarowie i dworzanie krzyknęli nawet, że należy utopić heretyka.
Jezuita stał nieustraszony, a potem pokornemi słowy ugłaskał Groźnego tak skutecznie,
iż przy pożegnaniu car podał mu rękę do pocałowania i uściskał go.
Widział go potem jeszcze dwa razy, ale o wierze nie mówił i dysputy z duchowieństwem nie dopuścił.
Wymówił się też od wojny z Turcją.
Zapewniał tylko, że w państwie moskiewskiem mieszkają ludzie różnych wiar swobodnie...

Wyprawił posła do Rzymu (Mołwianinowa), ale snać dał mu już inne instrukcje
bo ten stawił się na audiencji hardo (nakrył głowę) i nie wdał się w żadne układy o kościele.
Wszystkie nadzieje kurji rzymskiej pozyskania potężnej Moskwy dla katolicyzmu znikły bez śladu.
Tak Possevin doznał zawodu niemniej dotkliwego, niż w Szwecji.
Pojechał potem jeszcze raz do Polski na łatwiejsze narady z Batorym.

<div class="ralign">Historja nowożytna [I]{.Romannum rnum="1"}, str. 388 i nast.</div>
