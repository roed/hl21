# Michał Bobrzyński (Dzieje Polski) {-}

## Kult Świętych Pańskich {-}

Pośród społeczeństwa zdziczałego w dziedzinie obyczajów, strwożonego napadami wrogów, żyjącego z dnia na dzień,
pogrążonego w grubej ciemnocie i oddającego się bez różnicy stanu rozpuście, wpośród społeczeństwa,
w którem chciwość łączyła się z rozrzutnością, słabość z okrucieństwem i awanturniczą lekkomyślnością,
spotykamy coraz to liczniejsze postacie, które uciekają od świata, zamykają się w klasztornych murach i oddają pobożności
i miłosiernym uczynkom. Nigdy Polska nie wydała tylu świętych i błogosławionych.
Św.\ Jadwiga, żona Henryka Brodatego, spędza 30-ci lat małżeństwa w klasztorze trzebnickim,
św.\ Kinga prowadzi dziewiczy żywot z mężem swym Bolesławem Wstydliwym,
a kończy go w klasztorze sądeckim razem z błogosławioną Heleną, żoną Bolesława Pobożnego, ks.\ kaliskiego,
w Krakowie słyną cudami św. Bronisława Norbertanka i św.\ Jacek z możnego rodu Odrowążów,
dominikanin, pierwszy Polak, który podejmuje misję apostolską (na Rusi).
W Zawichoście zamyka się w klasztorze św.\ Salomea, córka Leszka Białego,
w Jędrzejowie biskup krakowski Kadłubek.
Kulminacyjnym punktem całego tego ruchu staje się kanonizacja św.\ Stanisława i podniesienie go na patrona Polski w r.\ 1254.
Książęta piastowscy, których tradycja rodzinna piętnowała biskupa Stanisława zdrajcą ojczyzny i tronu,
zjeżdżają teraz osobiście na jego kanonizację, korząc się przed Kościołem i zrywając ostatecznie ze wspomnieniami
dawnej swej autokracji.

Nie pod wpływem przymusu, lecz z wewnętrznego popędu, dla wiecznego zbawienia,
przeprowadzają oni teraz program Kościoła postawiony w przywileju generalnym z r.\ 1210.
Cały deszcz przywilejów spada w połowie [XIII]{.Romannum rnum="13"} wieku na pojedyńcze biskupstwa, kościoły i klasztory,
wszystkie niemal prawa książęce dostają im się w udziale, niektórym nawet prawo wybijania monet.
Z przywilejami idzie w parze szczodrobliwość niezwykła, mnożą się niezmiernie klasztory.

Na nowych podstawach na swojem własnem prawie kanonicznem organizuje się polski Kościół,
a sądownictwo jego rozciąga się nietylko na duchownych lecz także na ludność w dobrach kościelnych osiadłą,
a w wielu sprawach także i na całe społeczeństwo katolickie.
Na straży jego orzeczeń stoi interdykt, klątwa, a w ostateczności pomoc świeckiego ramienia.
Kościół i duchowieństwo zamieniają się w osobny stan polityczny, wyposażony pełnym samorządem.

Książęta utracili wszelki wpływ na obsadzenie kościelnych dostojeństw,
kapituły katedralne chwyciły w myśl ustaw kanonicznych prawo wyboru biskupów,
a od tego też czasu nietylko biskupi przestają się na książąt oglądać,
ale i niżsi duchowni nie ubiegają się już o łaskę książąt, wiedząc,
że tylko w obrębie kościoła strzeżeniem jego ducha i jego interesów mogą sobie znaczenie i godności zdobyć.

<div class="ralign">[I]{.Romannum rnum="1"}. § 37.</div>

## Humanizm {-}

Nauka scholastyczna panowała jeszcze urzędowo na uniwersytecie Jagiellońskim,
ale częste stosunki Polaków z Włochami, zarówno dyplomatyczne, jako też naukowe,
podróże na sobór Bazylejski i na studja do uniwerstytetów włoskich, sprawiły,
że nauki klasyczne rozbudowane we Włoszech wdzierały się
w drugiej połowie [XV]{.Romannum rnum="15"} wieku coraz to silniej do Polski...
Nauka, co było rzeczą najważniejszą, dostała się poza obręb duchownego stanu.

Nowe pokolenie otacza wkrótce króla. Nie był to już gmin szlachecki,
ale potomkowie znakomitych rodzin: Rytwiańscy, Ostrorogowie,
pierwsze pokolenie magnackie w Polsce, które nie wstępuje do duchownego stanu,
najwyższą owoczesną naukę na włoskich uniwersytetach sobie przyswoiło i dla dobra narodu pragnęło ją zużytkować.
Nie przynosili z sobą humaniści moralności w zwykłem tego słowa pojęciu.
Literatura starożytna, pogańska, nie mogła ich w tym kierunku podnieść,
owszem wyrabiała w nich pewną lekkość obyczajów, swobodę i brak zasad moralnych w wyborze środków,
prowadzących najłatwiej do celu. A jednak grono tych ludzi związane ściśle kierunkiem swego wykształcenia,
występuje w imię pewnej idei łącznie i solidarnie, dążące do jej urzeczywistnienia z prawdziwym zapałem,
przynosiło wielką siłę moralną społeczeństwu średniowiecznemu, które na polu zasad swoich religijnych i politycznych
już się było zupełnie rozprzęgło.
Pierwsi nasi humaniści byli to politycy prawnicy, którzy z długich a mozolnych studiów uwielbianego prawa rzymskiego
wynosili nową ideę państwa starożytnego, podniesionego do najwyższej harmonii i potęgi
kosztem swobody społecznej wynosili przedewszystkiem poczucie silnej władzy panującego,
bezwzględnego rozkazu i stanowczej konsekwencji w postępowaniu.
Ludziom tego rodzaju państwo średniowieczne, na przywileju stanów oparte, nie mogło wydawać się ideałem...
zwierzchnia władza papieska i cesarska napełniała ich wstrętem bo
"król polski nikogo wyższego nad sobą nie ma oprócz Boga...".
Jeden z tych nowych ludzi, Jan Ostroróg, postawił nawet plan nowego dążenia w memorjale,
który w sobie cały program reformy mieści, a siłą swej politycznej myśli
stanowi jeden z najpiękniejszych pomników naszej literatury.
Sarkali na tę młodzież nowym duchem przejętą... starzy ojcowie,
gorszyło się nieprzyjaznym kościołowi kierunkiem duchowieństwo.

## Reformacja {-}

W chwili ogólnego omdlenia, które za Zygmunta Starego owładnęło państwem i społeczeństwem,
zjawił się nowy prąd dziejowy, który całym narodem do głębi wstrząsnął,
z bezczynności go wyrwał, wielkie a trudne zadania mu postawił i ku osiągnięciu tych zadań zapalił go i popchnął.
Rolę tę odegrała w Polsce ówczesnej reformacja, ruch religijny,
do które go od dawna w całej Europie zbierały się materiały,
a do wybuchu hasło w Niemczech dał Marcin Luter (1517).

Mówiliśmy już o wielkim rozprzężeniu religijnem, moralnem i umysłowem, które się objawiło ku schyłkowi średnich wieków
jako konieczne następstwo scholastycznego systemu.
Wielkie sobory powszechne, pizański, konstancjeński, bazylejski, odprawiane w ciągu [XV]{.Romannum rnum="15"} wieku, są nam dowodem,
źe w ramach tego systemu i jego środkami nietylko dalszy postęp ludzkości,
ale nawet konieczna reforma kościoła nie dała się uskutecznić.
Widzieliśmy następnie skutki humanizmu, który swobodę myśli ludzkiej ze sobą przyniósł,
nowe poglądy otwierał, nowych środków dostarczał, ale odwracając ludzkość od chrześcijaństwa,
wstrząsał do reszty podstawami jego moralnego zdrowia,
wywoływał swawolę myśli a jałowość czynów.
Najgorsze objawy tego średniowiecznego ale spotęgowanego zepsucia i rozprzężenia przedstawiała, oczywiście instytucja,
w której dotychczas skupiały się najwyższe zadania cywilizacyjne ludzkości, t.\ j. kościół, a sobór lateraneński,
odprawiony w roku 1512, w chwili najwyższego rozkwitu humanizmu, zupełną swą bezsilność w sprawie reformy ujawnił.

Dopiero nadmiar zgorszenia i złego wywołał naturalną, a zbawienną reakcję, powrót do chrześcijaństwa, do religji, do Boga.
Powrót do chrześcijaństwa od pogańskiego humanizmu, to dodatnia strona reformacji, w najszerszem pojętej znaczeniu.
Wielki ruch religijny starał się na podstawie chrześcijańskiej naprawić to, co się od dawna zepsuło,
podnieść, co upadło, a wydzielając z humanizmu to, co w niem było dobrem i pięknem
stopić je nierozerwalnie z cywilizacją chrześcijańską i dla dalszego zużytkować rozwoju.
Był ten ruch zjawiskiem dziejowem powszechnem w łonie społeczeństw wychowanych na cywilizacji kościoła rzymskiego
a humanizmem dotkniętych, był dążeniem tem większem, że celu swego ostatecznie dopiął.
Zbudził zagasłe już uczucie religijne a za nim inne wzniosłe i szlachetne porywy,
natchnął energją woli i czynu, przywrócił społeczeństwu zdrowie.

Rozwiązano wielkie zadanie dziejowe dopiero po wielu walkach i po wielu błędach,
ale bez walki nie ma nigdy postępu, a przez błędy najczęściej droga do prawdy prowadzi.
Walki wewnętrzne, niejednokrotnie niszczące i krwawe, były objawem myśli sięgającej głębiej,
przekonania potęgującego się do fanatyzmu, energii zbudzonej do najwyższych poświęceń i zamachów.
Błędy wyjaśnić łatwo. Za długo ludzkość czekała na reformę legalną, wychodzącą z góry,
a zniecierpliwiona posunęła się do niej sama i porwała zuchwale.
Pojedyńcze narody, prowincje, gminy a nawet jednostki starały się naprawić kościół,
jego naukę i organizację na swoją rękę.
Działo się to tem łatwiej, że humanizm wszelkim objawom myśli i uczucia zupełną wyrobił swobodę.
Pod tym ogólnym wpływem zbudził się jednak i kościół katolicki do nowego działania
a legalni jego reprezentanci nie zardzewiałą bronią scholastyczną, ale nową bronią humanizmu i reformaci
dokonali na soborze trydenckim wielkiej reformy wewnętrznej
i rozwiązali zagadnienia, bez których rozwiązania społeczeństwo chrześcijańskie nie mogło się już dalej skutecznie rozwijać.
Legalna ta reforma przyszła jednak po części zapóźno;
wiele państw i narodów dokonawszy reformy na własną rękę,
nie chciało uznać legalnej i od powszechności kościoła na długo odpadło...

----

Związana od wieków z kościołem i cywilizacja zachodu,
odczuwająca wszystkie prądy, w których cywilizacja ta naprzód kroczyła,
nie mogła Polska pozostać obcą i zamknąć się przed wstrząśnięciem religijnem, które mieściło się w reformacji.
Musiała je przejść, musiała je przetrawić, musiała z niego dla dalszego swego rozwoju wyciągnąć korzyści.
Inaczej zrywała związek z Zachodem, a zbliżała się lub wpadała wprost w objęcia Wschodu.
Pytanie polegało więc w tem, czy i w jaki sposób Polska zdołała z reformacji skorzystać.

Zjawiła się u nas reformacja za panowania Zygmunta Starego,
wkrótce po pierwszym swoim wybuchu w Niemczech,
i znalazła żywioły przyjazne i w ogromuem zepsuciu duchowieństwa,
mianowicie episkopatu i w ludności niemieckiej,
gęsto w Prusach polskich i w miastach naszych osiadłej,
i we wspomnieniu sympatji husyckich,
które niedawno temu hierarchja kościelna z trudem tylko
w społeczeństwie szlacheckiem zdołała powstrzymać...

Dopiero w drugiej połowie panowania Zygmunta, kiedy już humanizm rozszerzył się na szlachtę
i do żywszego umysłowego pobudził ją ruchu, zaczyna reformacja działać na wyobraźnię.
Młodzież mimo zakazów coraz to tłumniej wyjeżdża na uniwersytety zagraniczne,
mianowicie niemieckie, zarażona protestanckim duchem,
książki protestanckie przybywają do kraju,
na dziedzińcach uniwersytetu krakowskiego sprzedawane, rozchwytywane,
i nie minęło kilkanaście lat, a cała szlachta polska, mieszczaństwo,
a nawet znaczna część duchowieństwa zaczęła się do nowego kierunku nakłaniać...

Ogół szlachty nie tykał zasad katolickiej wiary i obawiał się, aby ich nie obrazić,
nie pałał też niemiecką nienawiścią ku duchowieństwu,
pragnął zachować dotychczasową hierarchję kościelną, biskupią, a przedewszystkiem obrządek katolicki,
tak poetyczny i do uczucia jego i wyobraźni żywo przemawiający.
Ale ogół ten przyzwyczaił się oddawna uważać stolicę apostolską za obcą sobie,
uciążliwą, przez Włochów opanowaną władzę, tak samo więc,
jak za czasów Zbigniewa Oleśnickiego żądał teraz pewnej niezawisłości dla kościoła polskiego
i sobór kościelny polski pod prezydencją króla za najwyższą w państwie władzę kościelną
rad sobie przedstawiał.
Zniknąć miały różnice rzymskiego i wschodniego obrządku,
a środkiem prowadzącym do tego celu było zniesienie celibatu księży,
komunja pod obiema postaciami i zaprowadzenie języka polskiego w całej liturgji polskiego narodowego kościoła.
Nie było więc w ruchu reformacyjnym polskim pierwotnie burzących, anarchicznych pierwiastków.
Nie myślał nikt o swobodzie wyznania, o tolerancji,
żądano nadal kościoła jedynego, panującego bezwzględnie,
popartego przez państwo, karzącego odstępców świeckiej władzy ramieniem.
Rozprawiano wiele o artykułach wiary, bo w czasach soboru trydenckiego,
przed ukończeniem jego dzieła, któż tego mógł nie uczynić, kto o tem nie mówić, nie myśleć;
ale zamierzoną reformę przedstawiano sobie w Polsce tylko na drodze legalnej przez
duchowieństwo polskie i rząd, ze współudziałem, z zezwoleniem o ile można stolicy apostolskiej.
Nie rozstrzygnęła się jeszcze kwestja w łonie samego kościoła na soborze powszechnym,
czy dalszy jego rozwój pójdzie drogą autonomji kościołów narodowych,
czy też drogą bezwzględnej centralizacji;
żądania reformy, objawiające się w Polsce, nie wydawały się przeciwnemi wierze katolickiej,
ale wydawały się nawet łatwemi do przeprowadzenia w obrębie katolicyzmu i organizacji katolickiego kościoła.
Dążenie to religijne szlachty tem śmielej zresztą mogło się w Polsce rozwinąć, że niedwuznacznie sprzyjało mu duchowieństwo...

<div class="ralign">[II]{.Romannum rnum="2"}. § 73.</div>

Faktem jest, że ruch religijny ogarnął wszystko, co w narodzie było zacnem, inteligentnem
i przystępnem religijnemu uczuciu,
faktem jest, że z łona tego ruchu wyszedł jedyny program polityczny,
jedyne solidarne stronnictwo, na które aż do sejmu czteroletniego zdobyliśmy się w nowożytnych dziejach...
...W [XVIII]{.Romannum rnum="18"}-tym wieku starano się, ażeby reformację z dziejów naszych zupełnie wymazać i istnieniu jej zaprzeczyć.
Dziś tą drogą iść niepodobna...
Dziś nie możemy jej ograniczyć do aspiracji ku kościołowi narodowemu i do polemiki religijnej,
a odmówić jej programu politycznego i zbudzenia literatury, choćby dlatego,
że nie wiedzieliśmy, jakiemu czynnikowi mamy te korzyści przypisać...
Możemy tę reformację uznać za błąd, z jej niebezpieczeństw zdać sobie w dziejach naszych sprawę,
ale nie potrzebujemy jej czernić i dobre strony zasłaniać.
Nietylko zadanie historji, ale i duma nasza narodowa na to nie pozwala...

<div class="ralign">Z glosy do § 73 t.\ [II]{.Romannum rnum="2"}.</div>

## Polityka Religijna {-}

Syn Jana Wazy, który obstawaniem za katolicyzmem u całego protestanckiego narodu szwedzkiego się znienawidził,
wychowany przez jezuitów, przejął się Zygmunt taką żarliwością religijną,
wszystkie swoje uczucia, przekonania i siły do tego stopnia w sprawie przywrócenia katolicyzmu utopił,
że mógł się stać najznakomitszym szermierzem kościoła, ale musiał się stać najgorszym politykiem,
najgorszym stróżem doczesnego dobra państw zdanych na jego pieczę.
Nie dla dobra i pomyślności Polski związał się też z Habsburgami,
lecz dla walki z protestantyzmem poza granicami kraju;
przyjął polską koronę przedewszystkiem w tym celu, ażeby oparłszy się na potężnej a przeważnie
katolickiej Polsce, protestantyzm szwedzki przemocą obalić.

Wielkim bodźcem dla polityki Zygmunta byli dawni jego nauczyciele, jezuici,
którym i teraz bezwzględnie zaufał i zupełnie się oddał.
Najgorliwszy zwolennik i uczeń jezuitów, Jerzy Radziwiłł kardynał biskup krakowski,
był na dworze Zygmunta ministrem,
inni, a mianowicie O.\ Bernard, jezuita, wpływowe zajmowali funkcje.
Zrozumiemy też politykę jezuitów.
Stowarzyszenie religijne, kosmopolityczne, dla interesów narodowych Polski nie mogło mieć wyrozumienia
i w celach wyłącznie religijnych pragnęło je użytkować.
Na wielkiej szachownicy europejskiej była dla jezuitów Polska pionem,
który należało obrócić i posunąć dla ogólnej sprawy,
nie pytając się bynajmniej o to, czy się ten pion nie złamie i nie skruszy.
Politykę tę popierała z rzadką zresztą nieznajomością czy lekceważeniem stosunków Polski kurja rzymska;
dla niej również Polska stanowiła tylko prowincję kościelną, którą dla dobra kościoła można było,
a w danym razie należało nawet poświęcić.
Inaczej jednak sądzić musimy króla, który w Polsce widzieć miał naród i państwo,
a pierwszy je dla interesu kościoła poświęcał.
Zygmunt\ [III]{.Romannum rnum="3"} dalej nawet szedł od jezuitów i kurji rzymskiej,
bo w ślepem swem uwielbieniu Filipa\ [II]{.Romannum rnum="2"}, tak się do walki z protestanckiemi państwami zapalił,
tak bezwzględnie w walce z Holandją go wspierał, że nawet w tych chwilach on jeden go nie opuścił,
kiedy Rzym w inną stronę, bo ku Turcji pragnął zwrócić uwagę chrześcijańskiego świata
i z Filipem na złej stanął stopie...

----

Przyjmując reformację, stawała się niegdyś Polska ogniskiem całego okolicznego protestanckiego rozwoju i ruchu,
przerzucając się teraz na ściśle katolicką podstawę,
miała stać się warowną twierdzą do odzyskania krajów i narodów od Rzymu odpadłych.
Wiążąc się z Habsburgami, miała im Polska dopomódz do zgniecenia protestantyzmu w Czechach,
Węgrzech i Rzeszy niemieckiej; dopomagając Zygmuntowi w prawach jego do szwedzkiej korony,
miała Szwecję do kościoła rzymskiego nawrócić; mieszając się w sprawy wewnętrzne Moskwy,
miała i tam katolicyzm zaszczepić...

Polska w rękach Zygmunta\ [III]{.Romannum rnum="3"} do szwedzko-katolickiej  polityki była tylko wygodnem narzędziem,
o którego los tak dalece się nie troszczył, że z Anną, arcyksiężniczką austrjacką,
zawarł małżeństwo (1592\ r.), pomoc Habsburgów dla utrzymania się na tremie szwedzkim
odstąpieniem im Polski czy to po swojej śmierci, czy też nawet za życia pragnął sobie okupić
i w tym celu tajemne z nimi zawierał umowy.
Była to już otwarta zdrada kraju, który go na swój tron, dla obrony swoich interesów powołał.

<div class="ralign">[II]{.Romannum rnum="2"}. § 84.</div>

Najciekawszą, powiedzmy lepiej, najsmutniejszą rolę,
odegrali wśród rokoszu Zebrzydowskiego jezuici.
Wiedzieli, że katolicyzm już odniósł zwycięstwo, że do osiągnięcia zupełnego na cały naród wpływu
o wiele lepiej posłuży im apoteoza szlacheckiej wolności, niż absolutnej władzy.
Nie wahają się też ani na chwilę, w nowych wydaniach kazań sejmowych Skargi
wypuszczają głośne kazanie o monarchji, stają odważnie przed wzburzoną szlachtą,
tłumaczą się, że nigdy za monarchją nie przemawiali,
przedstawiają, że niemasz gorętszych od nich zwolenników swobód szlacheckich, elekcyj i jednomyślności.
Pierwsi jezuici występują teraz z doktryną, że kościół katolicki najlepszym swawoli szlachty jest stróżem,
że złota wolność a katolicyzm są to dwa jednoznaczne pojęcia.

Nie trzeba dodawać, jak strasznym był ten jad, który na umysły szlacheckie się wylał.
Powrócił naród do katolickiego kościoła w tem przekonaniu, że go ten kościół przed rozterką reformacyjną ustrzeże
i wzmocni, ale niedołężni apostołowie katolicyzmu w Polsce zawiedli jego nadzieje
i dla własnej chwilowej korzyści sami trujący podsunęli mu napój...

Zmarnowała Polska bezpowrotnie dwa wielkie prądy dziejowe, które niosły ze sobą silny rząd i rozwój,
to jest reformację, a następnie katolicką reakcję,
a tak na nowy prąd dziejowy, którym była dopiero rewolucja francuska, musiała czekać przez dwa wieki.

<div class="ralign">[II]{.Romannum rnum="2"}. § 85.</div>

## Polityka Kościelna {-}

Korzyści z przymierza francuskiego były dotykalne i przemawiały do każdego nieco trzeźwiejszego umysłu,
nachylała się ku niemu nawet i szlachta, obawiając się wielkich tureckich wojen z sojuszu z Austrją wynikających,
a tak polityka francuska miała zrazu wielką w narodzie przewagę, a uchwycona stanowczo przez króla,
mogła ją nadal taksamo utrzymać. Odwrócił się jednak od niej Sobieski.

Musiał się król liczyć najpierw ze złotą wolnością szlachty, która uświęcona w zasadniczych ustawach,
krępowała mu wszelkie swobodniejsze działania i każdy krok więcej polityczny,
a mniej popularny, przedstawiała jako zbrodniczy zamach podejrzliwym umysłom obałamuconej i bezmyślnej szlachty.
Musiał się także król liczyć z zupełnym upadkiem i rozstrojem moralnym narodu, który już doprowadził do tego,
że każdy obcy dwór pochlebstwem i pieniędzmi mógł sobie w Polsce zorganizować stronnictwo gotowe go popierać,
zrywać sejmiki i sejmy, udaremniać wykonanie najzbawienniejszych uchwał i własnemu rządowi najsroższe stawiać przeszkody.
Stronników takich umiał sobie zjednać nawet taki elektor brandenburski, o wiele łatwiej przyszło tego celu dopiąć
zagrożonej przymierzem polsko-francuskim, Austrji i wspierającej Austrję przeciw Turcji kurji rzymskiej.
Biskupi -- senatorowie słuchali rozkazów Rzymu i nuncjusza, kilkunastu magnatów pozyskano zwykłemi środkami,
w szlachcie poruszono jedyne jeszcze, jakie jej pozostało,
bo religijne uczucie, zapalono ją do wojny świętej przeciw Turkom,
a stolica apostolska użyła całego swego wpływu,
ażeby politykę francuską jako niechrześcijańską w Polsce zohydzić i do ratyfikacji
[pokoju żórawińskiego](http://pl.wikipedia.org/wiki/Rozejm_w_%C5%BBurawnie)
nie dopuścić.
Pod niemałą więc presją posłów austrjackich, nuncjuszów papieskich, duchowieństwa i szlachty
zostawał Jan\ [III]{.Romannum rnum="3"}, i ostatecznie uległ.

<div class="ralign">[II]{.Romannum rnum="2"}. § 97.</div>
