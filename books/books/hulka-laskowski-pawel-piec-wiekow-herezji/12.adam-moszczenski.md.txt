# Adam Moszczeński {-}

## Cuda i cudactwa reakcji {-}

Za panowania Jana\ [III]{.Romannum rnum="3"} i Augustów obydwóch Sasów,
nie masz śladu, aby któren świecki lub zakonnik napisał książkę,
w którejby była prawdziwa moralna nauka lub uczącej rzeczy jakiej pożytecznej w społeczeństwie.
Biskupi zaniedbywali edukację księży świeckich, kleryków, którzy wyszli ze szkół od Jezuitów,
nic nie umiejąc, seminarjów było mało i te bez dozoru Ci księża lepiej uczeni byli,
którzy słuchali teologji u księży Misjonarzy i Teatynów w Warszawie,
tych zaś niewiele było, bo trzeba było mieć do odebrania takowej edukacji majętnych rodziców...

Pisali księża panegiryki pełne pochlebstw, książki do nabożeństwa dziwaczne i śmieszne,
żywota świętych Polaków, a świętych dlatego, że klasztory fundowali, ogłaszali cuda i zjawienia,
np. jakoby jednej figurze w Kcyni w Wielkopolsce włosy rosły i że je co rok ustrzygać muszą,
u Dominikanów w Poznaniu, że zęby rosną, a w innych miejscach, że płacze;
w Gosławicach w województwie gnieźnieńskiem, mając proboszcz kollatora łatwowiernego,
mało oświeconego pana Łąckiego, podkomorzego kaliskiego, skomponował historję,
jakoby Lubrański, zmarły przeszły dziedzic Gosławic, na koniu ognistym w nocy przez sufit w izbie jego stanął
i mówił, żeby powiedział dziedzicowi, by fundusz wrócił na utrzymanie pięciu księży do
jego kościoła, bo inaczej Lubrański za zaniedbanie tego funduszu
dotychczas w czyścu zostaje;
ten ksiądz mówiąc do Lubrańskiego, że wiary mu nie dadzą o tem zjawieniu,
zapewnił Łąckiego, że Lubrański obiecał w tej postaci, w jakiej mu się pokazał, stanąć na komisji;
jakoż Łącki u prymasa Łubieńskiego komisję wyrobił;
jam widział i czytał książkę in 4-to opisującą to zjawienie,
potem Łącki o ten fundusz ułożył się z proboszczem i komisja jako już niepotrzebna, upadła.

Nie było domu magnatów lub majętnego obywatela,
żeby po śmierci pana lub pani, dusza nie pokazywała się to księżom,
to sługom obojga płci, to zakonnikom bawiącym w tym domu, i nie żądała,
by sukcesorowie czynili fundusze i hojne ekspensa na nabożeństwa,
ku poratowaniu jej zbawienia;
częstokroć te dusze zostawiały znaki wypalonej na stołach ręki
i straszyły swem pokazywaniem się ludzi i kobiety łatwowierne i bojaźliwe.

Obywatele przykładem Augusta\ [III]{.Romannum rnum="3"} bawili się polowaniem,
a żony ich jeździły z odpustu na odpust, odwiedzając miejsca cudowne,
miały one na czele księdza, a lud z chorągwiami z różnych klas złożony,
wraz z żebrakami i kalekami z wszech stron zebrany im towarzyszył.

Na każdym odpuście w kościele widzieć można było opętanych,
krzyczących głosem przeraźliwym i po kilka słów mówiących różnymi językami,
napastujących kobiety, które postraszone mdlały lub dostawały słabości;
tu znowu księży egzorcystów, zaklinających czartów opętanych do milczenia,
a kiedy na nich kładli relikwje lub wodą święconą kropili,
niesłychany krzyk i jęk i ryk wydawali, i w ciele kontorsje[^kontorsja] i łamaniny robili.

Przez cały Wielki Post widzieć można było po wszystkich parafjalnych kościołach i klasztorach,
na mszą wielką i na nieszpory wchodzącą procesję kapników,
na których czele jeden niósł krzyż z figurą Zbawiciela ukrzyżowanego,
a kończyli procesję dwaj kapnicy z laskami czarno malowanemi,
ci wszedłszy do kościoła parami, w dwóch rzędach klęknąwszy,
wzdłuż kościoła rozciągnęli się krzyżem,
kładąc się na ziemi z dyscypliną w ręku.
Ci kapnicy okryci byli worem z grubego płótna różnego koloru,
szeroko zrobionym, otwartość mającym na plecach,
dla łatwiejszego obnażania piec,
za podniesieniem zasłony zakrywającej tę otwartość.
Głowę nakrywali workiem kończatym u góry,
z dziurami na oczy i prosto ust dla oddychania,
z kołnierzem spadającym i przykrywającym ramiona.
Ci kapnicy za uderzeniem laską na ziemię przez kapników kończących procesję,
kładli się na ziemię, wstawali, podnosili zasłonę z pleców, zaczynali się biczować i przestawali.

Jedni mieli dyscypliny rzemienne z przypiekanemi końcami,
drudzy mieli druciane, a niektórzy z przyprawionemi metalowemi gwiazdeczkami na końcu,
co ciało rwało, tak się zaś mocno biczowali,
że blisko ich w ławkach siedzących lub klęczących krwią pluskali,
na co było przykro i obmierźle patrzeć.
W Wielki Piątek takaż procesja z kapników obchodziła stacje ze śpiewaniem o męce Pańskiej,
biczując się przy każdej stacji.
Jednego z kapników ubierano w komżę i w kapę, której księża do procesji używają,
na głowę jego kładziono velum od kielicha, a potem z ciernia uplecioną koronę.
Ten kapnik miał wyobrażać naszego Zbawiciela, niosącego krzyż na górę Kalwarję,
dla czego z drzewa dość ciężkiego miał położony krzyż na ramieniu,
z tym postępując krok za krokiem, przyklęka jednem kolanem,
kapnik zaś unoszący koniec tego krzyża,
łańcuchem przybitym bił weń, mówiąc za każdem uderzeniem:
"Postępuji Jezu!"
Tegoż dnia po wszystkich kościołach w różne wzory,
różnymi konceptami ubierano groby Pańskie,
co miało minę teatralnych reprezentacji.
Te groby od kościoła do kościoła, chodził odwiedzać lud obojej
płci przez Wielką Sobotę aż do Rezurekcji,
w czem mało było nabożeństwa a romansów wiele.

Na Boże Narodzenie, Nowy Rok i Trzech Króli stawiano w kościele różne figury,
które miały reprezentować tajemnicę, narodzenia, obrzezania i przybycie trzech królów z darami do Betleem;
w tych widzieć można było budowane szopy, stojące w nich muły, osły, woły, tamże i pastuszki w różnych ubiorach
i osoby do tych tajemnic należące z aniołami,
i zwali to jasełki, które odwiedzał lud i obywatele,
tak jak groby w Wielki Piątek i Wielką Sobotę,
a księża, zakonnicy i zakonnice,
sadzili się na przepych tych jasełek,
przywiązując chlubę stąd dla siebie, czyje ładniejsze były.
Widziałem te jasełki otoczone ludem klęczącym i modlącym się,
nie zważając na Najświętszy Sakrament na ołtarzu wielkim wystawiony...

Można było spostrzegać pielgrzymów w właściwych ich
sukniach, idących do Komposteli, Jerozolimy i Rzymu,
a wracając się, przynoszących z sobą dostatek relikwiarzy, agnus Dei, paciorków i medalików.
Znałem pana Wolskiego, pielgrzyma, któren wstępował do ojca mego;
ubrany był w żupanie aksamitnym czarnym, krzyż na lewym boku czerwony,
podszyty lamą srebrną, na butach krzyże haftowane,
przy płaszczu na srebrnym łańcuszku koncha morska zawieszona,
pas czerwony, za tym paciorki i w ręku laska pielgrzymska wysoka,
i ten w takim ubiorze prezentował się obojga królestwu.

Królowa przez pobożność świadczyła mu wiele,
a król bawił się z nim, żartując z niego...
On uroił sobie prowadzić wojnę przeciw barbarzyńcom,
zebrawszy znaczne pieniądze, wybudował sobie fregatę i nająwszy kapitana,
podobnego sobie awanturnika i majtków,
zawiesiwszy banderę hiszpańską, popłynął atakować Algierczyków;
ta okoliczność ledwie wojny nie zrobiła między Hiszpanją a Algierem.
Wolski z swoim statkiem umknął do portu papieskiego, gdzie mu skonfiskowano fregatę,
a osobę uwolniono na instancję królowej, jakąś niewielką za statek nagrodą.

Był pan Komornicki, obywatel podolski, który wyuczył kozy do ciągnienia wózka,
a stado kóz tak do wózka, którym jeździł i do siebie przyzwyczaił,
że go nigdy nie odstępowały;
nic nie jadał, tylko od nich mleko, które sam doił.
Udawał się za proroka i za człowieka świętego,
jeździł po kraju z przepowiadaniem różnych rzeczy,
bałamucił nietylko pospólstwo,
ale i obywateli i przymusił biskupa kamienieckiego Krasińskiego,
że mu włóczyć się zakazał, kozy odebrał i nastraszył zamknięciem.

<div class="ralign">Pamiętnik do historji polskiej.</div>

## Kołtuny i kołtuństwo {-}

W całej Wielkiej Polsce, Mazurach i Krakowskiem,
nietylko lud obojej płci, kołtunami głowy okryte mieli,
i to przypisywali skutkowi czarodziejstw lubo doktorowie choroby tej dwojakie kładli przyczyny,
pierwsze nieochędóstwo, drugie przypisywali wodzie i klimatowi kraju naszego.

Gdym chodził do szkół w Poznaniu, był doktór księcia Adama Czartoryskiego i kanoników katedry,
nazwiskiem Viner, ten był razem doktorem domu ojca mego, miał nietylko w leczeniu doświadczenie
wielkie, ale zastanawiał się i dochodził fizycznych przyczyn różnych chorób.
Uważał, że wsie złożone z gmin wyznania luterskiego i kalwińskiego,
tudzież szlachta nie podlegali chorobie kołtuna.
Wszedł tedy w egzaminowanie, dlaczego katolicy tak brzydkiej i nieszczęśliwej ulegali
chorobie i doszedł, że z jedzenia oleju lnianego w czasie postów, w którym
rodzi się powój, mający ziarna do lnu nasienia podobne i trudne do oddzielenia.
To ziarno jest trucizną, zarażającą humory człowieka do tego stopnia,
iż nietylko włosy się wiją, guzy po junkturach[^junktura] formuje,
ale nawet przechodzi z pokolenia w pokolenie i staje się chorobą sukcesjonalną (!).

Opisał Viner tę chorobę i przyczyny, z jakich pochodzi, oddał ją
biskupowi księciu Czartoryskiemu, któren przekonany uwagami doktora,
udał się do Rzymu i wyrobił bullę papieską, by posty nie na oleju,
ale na nabiale obserwowane były,
i tę bullę w swej diecezji poznańskiej i w Warszawie publikować z ambon rozkazał.
Ta bulla nie była przyjęta po większej części przez klasztory i fanatyków,
koniecznie utrzymujących, że choroba kołtunów pochodzi z czarów, bo gdyby tych zabrakło,
nie byliby potrzebni egzorcyści, zdejmujący kołtuny,
ustałyby przyczyny ofiarowania się na miejsca cudowne i czynienia ofiar w różnych wotach.

Znałem podkomorzynę poznańską, pozostałą wdowę ostatniego (?) z domu Czartoryskich,
panią bardzo majętną, którą opanowali jezuici i wszystkie intraty z dóbr jej zabierali.
Mimo wielu pochlebstw, które jej czynili, wyperswadowali jej, że chodząc po pokoju,
może odbyć pielgrzymkę do grobu Pańskiego do Jerozolimy,
z rzetelnym odpustem przeznaczonym dla tych, którzy pieszo tę podróż odbywają,
i wyrachowali na wiele mil jej dom od Jerozolimy jest odległy,
ile ta podróż zabierze czasu, aż do godzin wyliczono,
a w Rzymie wyrobili dla niej odpusty, jak gdyby istotnie tę podróż odbywała.
W Poznaniu ulice całe zamieszkałe były przez dewotki jezuickie,
wdowy bogate.

Jezuici w ręku swoim trzymając publiczną edukację młodzieży z
familji najznaczniejszych, najlepiej uczących się i najdowcipniejszych
namawiali do swego zakonu, a gdy którego namówili,
choćby przeszedł wszystkie klasy nauk, które dawali, wstąpiwszy do zakonu,
musiał pójść do pierwszej klasy i stopniami postępować aż do skończenia teologji,
co dowodzi szczerości nauk w ich publicznej edukacji.
Klasztory wszystkie z wielkim zapałem ucząc się jedni filozofji Arystotelesa,
drudzy Scota, inni Bakona, zajadliwe między sobą wiodły dysputy,
które rozrywać między nimi musiały trąby.

<div class="ralign">Tamże.</div>

## Zawsze masoni {-}

Wiedzieć potrzeba, że na sejmie 1766\ r., kiedy protestanci królowi na sejm podawali prośby,
aby mieli wolność w zgromadzeniach swoich budować zbory dla gmin
i mieszczan ku swemu nabożeństwu i by szlachta podług wyznania Lutra i Kalwina,
przez wybór na sejmikach należeć mogła do obrad publicznych i sądownictwa,
będąc równymi w kraju obywatelami i Polakami;
natenczas Czacki, podczaszy koronny, poseł na tym sejmie,
najwięcej się temu sprzeciwiał i innych kolegów zapalonemi mowami do sprzeciwiania się ich prośbom pobudzał,
tudzież hetman polny Rzewuski i książę biskup krakowski Sołtyk i biskup kujawski Załuski.

Czacki był hipokrytą nabożnym, posty zachowywał ściśle na oleju,
modlił się gorąco i wiele, mimo święta Bożego Ciała zwyczajnego,
wyjednał sobie u stolicy apostolskiej przywilej z odpustami,
by ostatnich dni zapust odbywało się u niego w Porycku nabożeństwo Bożego Ciała,
z czytaniem czterech ewangelji przy ołtarzach z procesją.
Krytykował cudze postępowanie i sposób życia, a kto mu się nie podobał,
głosił go być masonem bez żadnego na to dowodu,
bo loże masońskie nie egzystowały jeszcze w Warszawie, a za granicę mało kto wojażował.
Masonerję obwiniał najobrzydliwszemi występkami przeciwnemi Bogu i tajemnicom wiary chrześcijańskiej;
był skąpy, stół jego ordynaryjny; zawsze u niego przemieszkiwało
par kilka zakonników z różnych klasztorów dla nabożeństwa codziennego,
od rana do południa trwającego w kościele,
któremu on przytomny, z wielką asystencją pobożności modlił się,
a potem na obiedzie pił z nimi wiele i wszystkich do picia różnemi wynajdywanemi zdrowiami znaglał,
dysputował z nimi o teologji i kontrowersji, omawiając i krytykując wszystkich,
kto mu się tylko nie podobał w kraju, biskupów, senatorów, obywateli, damy.
Tym sposobem żywiąc i pojąc wszystkich u siebie, robił się popularnym u ludzi słabego umysłu i łatwowiernych.

Powstając więc na mosonerję i obwiniwszy za pomocą zakonników,
że synowcowie dziedzica Brusiłowa są masonami, dokazał,
że stryj pobożny, łatwowierny, Bogu duszę winny, wydziedziczył, i oddalił od sekcesji swoich rodzonych synowców,
a jego dziedzicem zrobił klucza Brusiłowskiego, którzy synowcowie nietylko,
że masonami nie byli, ale nawet że takowi egzystują, o nich nie wiedzieli.

Kościół w Porycku wymurował, ale fundusz tak skąpy zrobił,
że na  utrzymanie wikarego nie był dostarczający...
Sąsiad był niespokojny w granicach i lubił nabywać od ubogiej szlachty cząstki po wsiach,
przycisnąwszy ich wprzódy procesami do taniego zbycia.
Ubierał się po polsku, podobny więcej do Chińczyka niż do Polaka,
nosząc pasek zakonu św. Franciszka pod pasem i paciorki na nim.

<div class="ralign">Tamże.</div>

[^junktura]: Junktura --- staw, połączenie.

[^kontorsja]: Kontorsja --- skręcenie, wykrzywienie; grymas twarzy.
