# Władysław Smoleński {-}

## Umysłowość w dobie reakcji katolickiej {-}

Po wielkich wstrząśnieniach reformacji ogarnęła Polskę reakcja katolicka,
której panowania żaden przez długie czasy nie zmącił rokosz.
Duch teologiczny przeniknął wszystkie pory stosunków:
zarówno na aktach myśli i uczuć, jak działań praktycznych człowieka wycisnął swe piętno.
Duchowieństwo niestrudzenie pracowało nad przystosowaniem społeczeństwa do celów kościelnych.
Dzięki klerowi dostroił człowiek myśl swoją do akordu dogmatów,
według komendy artykułów katechizmowych maszerował przez życie od kołyski do trumny.

Przed podaniem pokarmu rączęta dziecka składano w znak krzyża na czole, ramionach i piersiach;
uczono je pacierza, gdy pierwsze słowa poczęło wymawiać.
Nie dano nic spożyć, dopóki młodsze nie przeżegnało się, starsze jakiej części pacierza nie powiedziało na pamięć.
W szkołach wdrażano przedewszystkiem do pobożności.
Oprócz nabożeństw w kościele i spowiedzi miesięcznych,
których niewolno było zaniedbać pod karą plag, śpiewać kazano codziennie litanję o najświętszej Pannie,
słuchać <span lang="la">**sacrum officium B.\ M.\ V.**</span>, odprawiać <span lang="la">**rosarium**</span>,
nucić pieśni religijne, w święta prawić oracje.
Dwunastoletni uczeń retoryki szkół jezuickich musiał przemawiać publicznie
na temat niepokalanego poczęcia najświętszej Panny...
Żeby przysposobić uczniów na szermierzy katolicyzmu, oprócz dysput tygodniowych
o różnicy pomiędzy religjami urządzali jezuici dwakroć w roku rozprawy
publiczne wystawniejsze.
Jeżeli nie stanął do walki teolog dysydencki, miejsce jego zastępował uczeń klas wyższych.
Zachęcali mistrze wychowańców swoich i do czynnego "wykorzeniania heretyków w ojczyźnie".
Burzyli uczniowie jezuiccy zbory i domy ministrów ewangelickich,
palili księgi, znęcali się nad żywymi, profanowali ciała umarłych...

Logikę nazywano "instrumentem, sposobnym i potrzebnym do nabycia wszelkiej mądrości",
chociaż nie zgadzali się wszyscy: czy jest <span lang="la">*scientia*</span>, <span lang="la">*practica*</span>,
czy <span lang="la">*speculativa*</span>?
Podręczniki do niej nie zawierały systematycznego wykładu przedmiotu,
lecz traktowały o dystynkcjach, uniwersałach, konstytutywach, konnotatach, relacjach;
podawały dysputacje, kwestje, argumenta <span lang="la">*pro et contra*</span> w najróżnorodniejszych materjach
teologicznych i nieteologicznych, nie mających żadnego związku z logiką.
Nasuwały im się takie kwestie, jak: czy kowal bez rąk może być dobrym rzemieślnikiem?;
czy Abel był istotnie synem Adama?
czy człowiek jako człowiek istnieje na świecie?;
czy prawda może być fałszem; czy pies może być kozą?;
czy Chrystus, siedzący na ośle, jest częścią osła, lub też osioł częścią Chrystusa?
Odpowiadały na nie wykrętami sofistycznemi lub sentencjami teologicznemi.
Na pytanie: czy może ktoś być swoim ojcem? -- przyrodzonym sposobem, -- brzmiała odpowiedź,
-- nie, ale mogłoby to być przez cud.
Czy całość jest większa od części? -- niezawsze, albowiem Trójca święta nie jest
większa od Boga Ojca, jak naucza św.\ Augustyn...

W niezliczonych okazjach działał sam Pan Bóg, Matka boska, lub święci.
Za Jana Kazimierza "królestwo polskie z triumfem wypędziło nieprzyjaciół,
niosąc Opatrzności boskiej hieroglifikę, iż niesfornych Polaków ona tylko
utrzymuje i broni".
Król "na pomocy wszechmocnego Boga i Panny przenajświętszej fundując się bardziej,
niż na ludzkich sukcesach, powrót swój do królestwa Opatrzności boskiej przypisywał;
wzywając Boga i Panny przeczystej, konfederację uczynił z niebem,
na upodobanie i dyspozycję boską poruczając się całkiem".
Podobnież Pan Bóg "eliberował lud swój" za panowania Augusta [II]{.Romannum rnum="2"},
"bo w takie nieszczęścia uwikłaną była ojczyzna, że się to nie zdało rozumem ludzkim,
aby naturalnemi sposobami przejść miała do swego bytu i zostać w całości".
Widocznie Pan Bóg ujawniał dla narodu swą sympatję,
gdy podczas bitwy pod Beresteczkiem zasiadł w obłokach na złotym tronie w otoczeniu aniołów,
w koronie i z mieczem, a opodal, w chmurach, umieścił ogród
i napis w środku: "Salvator mundi";
widomie mu i Matka Boska sprzyjała, skoro obozy nakrywała swym płaszczem
i wielokrotnie orężowi polskiemu zapewniała zwycięstwo.
Najświętszej Pannie zawdzięczał oblężony w Moskwie Gosiewski,
że w dniu pewnym stracił tylko dwudziestu ludzi,
gdy spodziewał się "tysiącami głową nałożyć".
Zwycięstwo pod Zborowem przypisywał Jan Kazimierz obrazowi Panny przeczystej
z klasztoru bełskiego bazyijanów, "przed którym w nocy, krzyżem leżąc modlił się, aby
Obronicielka królestwa polskiego z toni wyprowadziła ojczyznę...".

Teologicznie objaśniano i zdarzenia drobne.
Pożar gasnął dlatego, że go księża z najświętszym sakramentem obeszli;
że go Pan Bóg "wejrzawszy z miłosierdzia swego na wzdychania pobożnych zakonników, cudownie uśmierzył",
że strzegł św. Wawrzyniec lub Florjan.
"Opatrzność boska, za przyczyną iwiętych patronów", dawała wdowcowi żonę;
cudownym sposobem uchodzili ludzie z więzienia;
ojcowie jasnogórscy cudem boskim poznali szpiegów;
nawet cudownym sposobem zachował Pan Bóg naładowaną żytem, a tonącą wicinę.
Udała się deputatowi mowa trybunalska, bo mu "Pan Bóg
krzywdę, przez Grabowskiego, pijanego w skomatycznych słowach na
obiedzie u Sołłohuba uczynioną, gdy się pokornie Panu Bogu przez intercesję
św. Jana Nepomucena ofiarował, sowicie nagrodzić raczył".
Niekiedy miłosierdzie boskie ująć się dało winem, lub wódką.
"Chcąc, -- powiada pamiętnikarz, -- kaptować afekt i łaskę wojewodzianów,
kłaniałem i częstowałem, ile możności mojej być mogło,
a tak za miłosierdziem boskiem zacząłem mieć w województwie afekt i łaskę..."

...Od Mieczysława zasługiwali sobie Polacy, oprócz chwały wiecznej,
na korzyści doczesne, do których dopomagała im najświętsza M. Panna i święci.
Związkiem chrześcijańskiej miłości spojeni Litwini, Polacy i Rusini za grunt
trwałości i szczęścia królestwa wzięli cnotę, pobożność i szacunek dla wiary;
jedno było mówić: Polak i katolik, -- nie uznawali za synów ojczyzny, lecz za
odrodków od wszystkiej poczciwości tych, którzy powstawali przeciwko religii starodawnej.
Jagiełło i Witołd wzgardzili koroną, którą ofiarował im naród,
uporczywie utrzymujący kacerstwo Husa.
Prawowierni Polacy po wszystkie wieki
i lata ku obronie wiary świętej stanowią prawa pobożne,
krew dla niej oddają, majętności i życie;
błędy kacerskie wytępiają odważnie i mocno.
Wypędzili arjan, upokorzyli kalwinów i lutrów.
"I ta jest przyczyna, że tyle nad insze narody ze krwi swojej pozyskali świętych obrońców przed
Bogiem: śś:\ Stanisława, Jadwigę, Salomeę, Kunegundę, Kazimierza, Jozafata Kuncewicza, Jacka,
pięciu męczenników, dwóch pustelników, Stanisława Kostkę, Jana Kantego, Jana z Dukli,
Szymona z Lipnicy, Władysława z Gielniowa i inszych.
Osobliwsza dla narodu naszego przychylność nieba, że Chrystus, że przeczysta Matka i Panna,
że tylu świętych założyło sobie w Polsce szczególniejsze mieszkanie;
że nas ratują, utrzymują w wierze prawdziwej i w złotej wolności".

Oprócz królującego nad światem Boga wpływały na losy człowieka złe duchy,
-- upadli aniołowie, posiadający siłę olbrzymią i rozum, czyhający na nieszczęście ludzkie
na ziemi i na męczarnie duszy za grobem...
Wierzono w peregrynacje niewiast starych na łopatach lub miotłach;
nie wątpiono o umiejętności czarownic przenoszenia nabiału z miejsca na miejsce,
oprzyrządzaniu trunków mlecznych, zażegnywaniu i odżegnywaniu uroków i o rozlicznych,
działanych przez nie przykrościach.
Przez czary psuło się szczęśliwe pożycie małżeńskie,
przez nie nawiedzała ludzi śmierć nagła.
Niekiedy miano wątpliwości, czyjemu wpływowi przypisać dany wypadek:
"ledwom nie umarł, -- powiada pamiętnikarz, -- czy z przepicia, czy też z uroków..."

Przewodniczyli w praktykach nabożnych księża, szczególniej zakonnicy,
których liczba w stuleciu [XVII]{.Romannum rnum="17"} wzrosła olbrzymio.
Do istniejących w czasach dawniejszych przybyli: reformaci, bonifratrzy,
oratorjanie, misjonarze, karmelici bosi i bazyljanie,
-- rozrzuconych było po kraju około 600 klasztorów.
Zakładali mnisi bractwa nabożne, w konfesjonale i z ambony,
w celach swoich i w wycieczkach za kwestą zachęcali do pielgrzymek, rekolekcyj i ćwiczeń.
Działali za pomocą misyj, fundowanych przez panów, a odprawianych hałaśliwie;
wpływali przez liczne odpusty, przywiązane do ołtarzy, i przez jubileusze powszechne.
Zuchwałym grzesznikom grozili niedopuszczeniem do sakramentów,
odmówieniem pogrzebu i klątwą.
Przychodziło duchowieństwu z pomocą i państwo.
Za przeżycie przez rok i sześć niedziel w klątwie kościelnej,
podobnie jak za bluźnierstwo przeciwko religji katolickiej
i niewykonywanie przez lat kilka spowiedzi wielkanocnej,
pociągano od odpowiedzialności sądowej...

Przewidując śmierć bliską dygnitarze rezygnowali z urzędów i schyłek
życia przepędzali w klasztorach.
Marszałek w.\ kor., Opaliński, "gotując się na śmierć, abdykował to ministerjum,
resztę życia dla Boga poświęciwszy w Leżajsku u bernardynów,
przy cudownym obrazie Panny przeczystej".
Jan Kazimierz na radzie senatu w r.\ 1668, na sejmie i w akcie abdykacji
oświadcza, że "składa koronę, ażeby miał czasu trocha wolnego przygotowania się na tamten świat".
W ostatnich chwilach baczono pilnie, iżby ducha swego, <span lang="la">*omnibus sacramentis*</span> dobrze uzbrojonego,
Panu Bogu oddać...

Zalecano w testamentach, żeby pogrzebowi towarzyszyło jak najwięcej
księży zakonnych i świeckich.
Za trumną hetmana Koniecpolskiego w Brodach w r.\ 1644
szło karmelitów szarych i franciszkanów par 16,
bernardynów par 23, dominikanów par 32 i niemało księży świeckich.
Na pogrzeb hetmana w.\ kor., Radziwiłła, zebrała się tak duża liczba zakonników,
księży świeckich i prezbiterów unickich, że się w Nieświeżu nie mogła pomieścić.
Szlachcic średniej zamożności na pogrzeb ojca sprowadzał 324 księży <span lang="la">*ritus latini*</span>,
150 <span lang="la">*ritus uniti*</span> i bractwa...

Zalecali ojcowie w testamentach swym dzieciom wytrwałość w
katolicyzmie dla różnowierstwa nienawiść.
"Napominam was, dziatki moje, -- pisał Koniecpolski, -- najprzód przy wierze świętej katolickiej rzymskiej,
w której ja umieram, abyście, mocno stojąc, heretyctwa i nowszych nauk,
którekolwiek w jakimkolwiek sposobem się wznoszą, strzegli... Każdy masz
się cieszyć, iżeś jest synem tej matki, t.\ j. kościoła powszechnego, który
wszystkich świętych matką wspólną jest i na której łonie umrzeć szczęśliwsza
rzecz jest, aniżeli się od początku urodzić, gdyż nigdy się nie urodzić lepsza
jest, aniżeli w tym kościele nie umrzeć".
-- "Wnuku mój, -- pisał inny Koniecpolski w rodowodzie swego domu, -- to było w pradziadach twoich,
że byli, żal się Boże, ewangelikami..."
Dzieci spełniały ściśle zalecenia  ojców.
Z żarliwości katolickiej usuwały różnowierców z urzędów, tamowały im na sejmach wolność głosu,
nie pozwalały zasiadać w sądach.
W r.\ 1648 upominali się dysydenci o krzywdę Niemirycza, że go jako arjanina,
nie dopuszczono do podkomorstwa w województwie kijowskiem,
w r.\ 1748 domagano się, żeby generalnego dyrektora poczt w Rzeczypospolitej,
jako dysydenta <span lang="la">*in religione*</span>, zrzucić z urzędu.
Na sejmie w r.\ 1718 krzyknęli Mazurowie i Litwa,
że poseł Piotrowski, "nie może mieć głosu w państwie katolickiem,
ponieważ jest sektarz";
w 1717 siedmiu deputatów dla różnicy w religji musiało opuścić trybunał.
W punktach zgody z kozakami w r.\ 1648 i w umowie hadziackiej zastrzeżono,
aby w projektowanych akademjach: kijowskiej i litewskiej
"żadnych sekt: arjańskiej, kalwińskiej, luterskiej profesorów, mistrzów i studentów nie było".
Obawiano się, żeby obecność dysydentów nie wywołała gniewu bożego i nie naraziła kraju na klęski.
Więc Zygmunt\ [III]{.Romannum rnum="3"} polecał hetmanowi Koniecpolskiemu,
"aby w obozie polskim nie było protestanckich ministrów,
a nawet w mieście, pod którem blisko wojsko leży obozem".
Podczas oblężenia w Zbarażu żołnierze katoliccy predykantów kalwińskich,
że w namiocie śpiewali, chcieli rozsiekać.
W r.\ 1750 stolica Rzeczypospolitej po raz pierwszy ujrzała pogrzeb luterski,
za który oficjał warszawski, Grzegorzewski,
za wpływem pobożnej żony Augusta\ [III]{.Romannum rnum="3"} złożony został z urzędu...

Ludzie wieku [XVII]{.Romannum rnum="17"} o zasługach swoich religijnych mieli
najlepsze przeświadczenie i nie taili,
że czynili niekiedy dla Boga nad powinność.
Koniecpolski nie bez pewnej chełpliwości powiada w testamencie:
"uczyniłem ja za pomocą najwyższego Boga dosyć,
nie z samej tylko powinności,
ale z należytej ode mnie ku wywyższeniu niewidzialnej
w Bóstwie imienia Trójcy przenajświętszej ochoty".
Nie dziw, że ludziom, którzy woleli
"na sto kul flintowych iść, niżeli na jedną różańcową paciorkę",
i którzy tyle mieli pewności o nadmiarze zasług swoich,
nie szczędził Pan Bóg dobrodziejstw;
nie dziw, że ich obdarzał zdolnością cudotwórstwa.
Stanisław z Bogusławic Sierakowski, opat świętokrzyski (zm.\ 1662),
"jeszcze żyjący", uzdrowił modlitwami wielu chorych.
Pani Pstrokońska, pacierzami i postem karmiona,
rozpędzała krzyżem świętym nawałnice i chmury.

Temu samemu prądowi teologicznemu uległo i piśmiennictwo.
Duch kościelny rozparł się w tysiącach panegiryków,
w stosach kazań, w niezliczonem mnóstwie żywotów świętych.
Najlepsze talenta owych czasów układają:
Hymny i pieśni na różne teksty ewangeliczne.
-- Siedem psalmów albo hymnów na pacierza części siedem,
-- Ogród panieński, pod sznur pisma świętego i doktorów wysadzony.
-- Podręczniki i dzieła naukowe podawały absurda najpotworniejsze.
Najpoważniej rozbierano w nich kwestje:
Czemu to Pan Bóg nie stworzył człowieka z djamentu, albo z złota, ale z podłej gliny?
Dlaczego natura i dawca jej przydał człowiekowi dwie oczy?
Czyli bestje i inne zwierzęta stworzył Pan Bóg z ziemie?
Czemu też to Ewa stworzona jest, czyli bardziej zrodzona z Adama, nie Adam z Ewy?
Dlaczego to Ewa nie jest stworzona z ciała Adamowego, ale z kości?
Dlaczego Ewa nie jest z inszej kości stworzona, ale z żebra?
Z którego żebra jest stworzona Ewa?
Czemu też to Ewa nie na jawie, ale we śnie stworzona z Adama?
Co się natenczas Adamowi śniło, gdy z niego Pan Bóg stwarzał Ewę?
Jestże też niewiasta człowiekiem?
Jakie też to jabłko było, które zerwała Ewa?
Wiele razy zgrzeszyli w raju pierwsi rodzice nasi?
Dlaczego to Pan Bóg nie sprawił pierwszym rodzicom naszym delikatnych jakich sukien, ale proste kożuszki?
Odpowiedzi godne były tematów.
Na pytanie, co się Adamowi śniło, gdy Pan Bóg stwarzał Ewę?
-- "Podobno mu się śniło, -- brzmiała odpowiedź, -- jakby grał w kości,
bo obudziwszy się, z temi zaraz dał się słyszeć słowy:
<span lang="la">*hoc nunc os ex ossibus meis*</span>.
Domyślił się na jawie, że we śnie jednę kostkę przegrał.
Bardziej jednak wierzyć potrzeba, że mu się to śnić musiało,
co natenczas Pan Bóg z nim robił.
Racja tego jest naturalna: bo Adama nikt w tem nie informował,
że Ewa jest od Boga stworzona z kości jego;
a przecie Adam, jak się prędko obudził, zaraz mówił:
<span lang="la">*hoc nunc os ex ossibus meis*</span>.
Skądżeby to wiedział, jeżeli nie ze snu?"
Jestże niewiasta człowiekiem?
"Jabym sądził, -- odpowiada autor, -- że nie jest:
bo człowiek jest stworzony na obraz boski,
niewiasta zaś jest stworzona na wyobrażenie człowieka.
A jako Adam przez to, że był stworzony na wyobrażenie boskie,
nie był Bogiem, tak i niewiasta Ewa przez to,
że była stworzona na wyobrażenie człowieka, nie jest człowiekiem.
Rzetelniej jednak mówiąc,
nietylko że niewiasta jest człowiekiem, bo ma tę samą istotę, co mężczyzna,
t.\ j. ciało i duszę, ale też, co większa, jest niby doskonalszem stworzeniem, niżeli mężczyzna,
bo Adam był stworzony z prostej gliny, Ewa zaś jest uczyniona z kości serdecznej Adama.
On był stworzony na świecie, Ewa zaś, gdyby coś droższego, w raju."

W ruchu umysłowym Europy Polska udziału nie brała,
nie przyswajała sobie nawet najcelniejszych jej zdobyczy.
Dopiero około połowy [XVIII]{.Romannum rnum="18"} wieku usiłuje otrząsnąć się z martwoty scholastycznej,
nawiązuje stosunki z wiedzą europejską...

<div class="ralign">Przewrót umysłowy w polsce wieku [XVIII]{.Romannum rnum="18"}.\
Studia historyczne, PIW 1949 r., wyd.\ [III]{.Romannum rnum="3"}, str. 1--20.</div>
