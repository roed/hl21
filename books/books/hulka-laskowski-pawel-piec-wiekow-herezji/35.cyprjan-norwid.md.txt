# Cyprjan Norwid {-}

## Fraszka petersbursko-wiedeńskim papistom przypisana {-}

### I

<div class="verse">
--- Broń lepiej Syksta, Klemensa, Piusa,\
Człowieka, mówię w papieża osobie:\
Lecz nie broń władzy, co jest od Chrystusa\
Najwszechmocniejszą potęgą na globie.\
Bo cóż ty jesteś?... Albo czemże ona!...\
Wszechmocność władzy, gdy taka obrona!...\
Stąd uczą dzieje wieków dziewiętnastu,\
Że z tych, co władzy papieskiej bronili\
Wątpię, czy wiernym był choć jeden na stu.\
A dziś?... Dziś może już się nawrócili!...
</div>

### II

<div class="verse">
O! Katolicy szanowni... ta wasza\
O Chrystusową potęgę obawa\
To jeszcze resztki wnętrzności Judasza,\
Co się po świecie rozwlekły jak lawa ---\
To Piotrowego odłamek pałasza,\
Co przed zaparciem się z pochew dostawa!
</div>

### III

<div class="verse">
Gdybyście wiarę mieli, to już dawno\
Widzielibyście, że glob jest Kościołem,\
Który ma ową bazylikę sławną\
Piotrową --- niby ołtarzem i stołem...\
Ale wam trzeba kościół w ołtarz wcisnąć\
I zamknąć i straż postawić przy grobie,\
Żeby za prędko nie mógł Bóg wybłysnąć...\
Czekajcie!... Wstanie On w cało-osobie.
</div>

### IV

<div class="verse">
Wstanie On i już świtają promienie\
Przez rozsadzone grobowca kamienie\
I już chorągwi rąbek się wybiela\
Ze szpar... i ranek bliski Zbawiciela...\
[...]
</div>

### V

<div class="verse">
Zapytaj niewiast, gdy same na drodze,\
Gdy rozrzewnione gubią się w płakaniu...\
Zapytaj mędrców... albo na podłodze\
Leżących prochów, bo skorsze w uznaniu ---\
Albo zapytaj siebie, ale w chwili,\
Gdy pod stopami glob ci się zakręci,\
I cudza w tobie waga się przesili,\
I w swojej staniesz mierze, bez pamięci.\
Zapytaj --- ---
</div>

### VI

<div class="verse">
Dokąd będziem się zwodzili?
</div>

## Fraszka {-}

<div class="verse">
Dewocja krzyczy: "Michelet[^michelet] wychodzi z Kościoła!"\
Prawda; dewocja tylko tego nie spostrzegła,\
Że za kościołem człowiek o ratunek woła,\
Że kona --- że, ażeby krew go nie ubiegła,\
To ornat drze się w pasy i zawiązuje rany.

∗\ ∗\ ∗

A Faryzeusz mimo idzie zadumany...
</div>

[^michelet]: [Michelet Juliusz](http://pl.wikipedia.org/wiki/Jules_Michelet) (1798--1874) --- historyk i pisarz francuski;
    wspólnie z Mickiewiczem szerzył w <span lang="fr">*Collège de France*</span> idee republikańskie.
    Napisał m.\ in.\ dizeło "Polska umęczona". Znany ze swoich antyjezuickich wystąpień.
    Za swoje postępowe poglądy został pozbawiony katedry.
