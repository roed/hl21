# Artur Górski {-}

## Kultura i religia {-}

Każda kultura ma u swego początku wybuch żywiołu religijnego.
Kiedy żywioł religijny danej kultury lub danego jej okresu wyczerpuje się,
wyczerpuje się tem samem i kultura.
Czegóż bowiem owocem jest kultura? Jak się tworzy?
Tworzy się pod działaniem dwóch sił:
z jednej strony, pod naciskiem rzeczywistości świata na duszę,
a z drugiej strony na skutek prężności duchowej w człowieku,
która tę rzeczywistość odpiera i przekształca.
Kiedy prężność owa ulegnie wyczerpaniu wówczas kultura staje się w treści swojej,
pod naciskiem rzeczywistości fizycznej,
zmysłową albo brutalną mimo wszystkie jej formy przyswojone.
I wtedy przychodzi czas na przejaw nowy tegoż to żywiołu religijnego.

<div class="ralign">Na nowym progu.</div>

## Reformacja a reakcja {-}

W wieku XVI jeszcze katolicyzm nie był u nas tak mocno utrwalony.
Naród zrósł się z obyczajowością religijną daleko więcej
niż z organizacją kościoła,
a w obyczajowość tę wchodziło mnóstwo tradycyj "starej wiary"
z czasów lechickiego pogaństwa.
Przytem obecność innych wyznań na obszarze Rzeczypospolitej nakłaniała do oględności
i do szukania związków ponad różnicami obrzędów...

----

Katolicyzm nie był wówczas tem, czem jest dzisiaj,
naród gotów był do nowych przetworzeń,
do zbudowania innego organizmu kościelnego.
Duchowieństwo niższe nie cieszyło się wielkim szacunkiem,
widzimy to z fraszek Kochanowskiego.
Nie miał w Polsce powodzenia delegat Leona X do rozprzedawania odpustów
i wywiózł puste sakwy.
Przytem od czasów Kazimierza Jagiellończyka zdawano sobie u nas jaśniej sprawę z tego,
że Rzym zabiegał jedynie o rozszerzenie katolicyzmu,
nie biorąc zupełnie w rachubę czasowego układu interesów polskich.

Jedności przeto szukano... --- --- --- Kościół zachodni a narodowy
byłby może usunął zgubne odium między kościołem polskim a ruskim,
między duchownym łacińskim a duchownym greckim...
Jak nas... uczą dzieje unji i wojen kozackich,
swar i ucisk wyznaniowy prowadzony był przez sługi kurji rzymskiej,
nie przez członków rządu polskiego.
Rząd polski potępiał jak najostrzej działanie tych,
którzy w szerzeniu unji dopuszczali się przymusu.
Toż samo, kiedy po śmierci Chmielnickiego, który się poddał carom,
ataman Wyhowski zrywa z Moskwą i zawiera z Polakami ugodę hadziacką,
ugoda ta nie otrzymuje zatwierdzenia sejmu,
ponieważ bulla papieska powstrzymała duchowieństwo katolickie
od przyjęcia do senatu metropolity kijowskiego i władyków prawosłanych.
Jedno to z wielu następstw wysługiwania się Rzymowi...

----

Katolicyzm  straciwszy narazie swe poczucie wyższości moralnej,
przerzucił walkę z protestantyzmu na połę polityki, na
drogę knowań gabinetowych,
popierał absolutyzm u góry,
a poduszczał masy u dołu.
W ten sposób w sprawach sumienia poczęto się uciekać w krajach katolickich do gwałtu.
Ludwik\ XVI odwołuje edykt nantejski o swobodzie wyznań pod wpływem jezuity
-- spowiednika, księdza La Chaise;
w Polsce książka jezuity Cichowskiego twierdząca,
że szlachta podgórska djabła za Boga ma,
stanęła na równi z odezwami Chmielnickiego, wzywającemi do rzezi szlachty.
I rzeź arjan nastąpiła w tych okolicach z okazji najścia Szwedów,
na skutek kazań duchowieństwa, które powtarzało nauki swego preceptora.
Utworzył się też za wpływem zakonu jezuitów nieszczery stosunek rządu do społeczeństwa,
czego obrazem była Polska Zygmunta Wazy;
rząd dążył do celów stronnictwa katolickiego w Europie,
wbrew nawet żywotnym interesom narodu i jakby poza jego plecami;
naród natomiast nauczył się stawać do rządu w opozycji,
na tej drodze zaprzepaszczać swoje sumienie publiczne,
szkoła bowiem wychowania religijnego prowadziła do cnót domowych i dewocji,
ale nie dbała zgoła o rozwój sumienia publicznego,
pobłażając wybrykom byle wzięcia nie tracić u szlacheckiego demosu.
Był to żywot publicznie niezdrowy.

<div class="ralign">Ku czemu Polska szła, str. 85--89.</div>

## Dwa sumienia {-}

Oparciem dla poczucia narodowego może być tylko sumienie religijne ludu.
Tymczasem spotykałem ludzi o życiu religijnem rozbudzonem,
sprawiedliwych, uczciwych, ofiarnych pełnych nawet zaparcia się siebie,
subtelnych w swej wiedzy o sumieniu,
słowem chrześcijan, którzy w twardych warunkach życia
umieli zachować prawie świątobliwość -- i równocześnie pazbawionych etycznej świadomości narodowej.
W tej dziedzinie panowało w ich duszy religijnej *vacuum*.
Rozmawiając z nimi usłyszałem takie sądy na temat przynależności do narodu,
że można było przestraszyć się.
Punktem wyjścia tych sądów było przeświadczenie,
że byle być dobrym katolikiem, to mniejsza o narodowość.

Osoby, o które tu chodzi, miały równocześnie wyrobione poczucie plemienne,
umiały przeciwstawić się Niemcom, a jedna z nich przeszła Wrześnię,
stawiając czoło po bohatersku prześladowaniom.

Czego to dowodzi? Chyba tego, że religijna dusza ludu nie pozostaje w łączności z jego duszą narodową,
a tem samem nie ma jedności w sobie.
Poczucie narodowe tego ludu stoi poza jego pionem religijnym,
nie znajduje w nim etycznego oparcia.

...(Trzeba) wiarę wzmocnić potęgą duchową narodu
a naród wzmocnić potęgą duchową wiary.
Wtedy zniknie podział społeczeństwa
na obywateli narodowych z jednej strony,
a na chrześcijan katolików z drugiej;
wtedy ustaną takie przerażające objawy,
że im człowiek z ludu jest więcej chrześcijaninem,
tem więcej nie wie, gdzie i jak pomieścić etykę narodową
w swoim religijnym kanonie...

----

U nas słowo narodowe nawet wtedy gdy ma szatę odświętną,
stoi dotąd jak Kopciuszek w przedsionku religijnej duszy narodu.

Któż nam otworzy jej podwoje?
Kto dwa sumienia ludu złączy w jedno?

<div class="ralign">Kurjer Poznański, 23.XI.25.</div>
