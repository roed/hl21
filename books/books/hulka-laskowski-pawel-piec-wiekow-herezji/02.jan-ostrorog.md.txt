# Jan Ostroróg (zm.\ 1501) {-}

## Przeciw wyzyskowi rzymskiemu {-}

Bolesna i nieludzka uciążliwość dręczy także królestwo polskie ze wszech miar wolne, równie i w tem, że nieustanną chytrością Włochów
tak dalece łudzić się dajemy, iż pod pozorem pobożności i fałszywej nauki, a raczej istotnego zabobonu, tak wielkie sumy pieniędzy
do dworu, jak nazywają, rzymskiego, corocznie dozwalamy wyprowadzać, w opłacie ogromnej daniny, którą *sakrą* czyli *annatami* zowią.
Ilekroć w djecezji nowy biskup zostaje obrany, nie odbiera sakry jak za opłatą poprzednio papieżowi w Rzymie złożoną z kilku tysięcy czerwonych złotych,
lubo święte kanony uczą, że nowo obrany biskup powinien być poświęcany i potwierdzany przez arcybiskupa i biskupów.
Chytrzy i podstępni Włosi przywłaszczyli sobie władzę, gdy my tymczasem poziewamy i zasypiamy.
Wiadomo, że niemieccy i polscy panowie tylko do lat kilku stolicy apostolskiej pozwolili na wybieranie annat, tym celem,
aby napastnicy wiary chrześcijańskiej hamowani byli i okrutny Turczyn w napadach swych wstrzymany został.
I to pewna, że te kilka lat wyznaczonych dawno upłynęło i że annaty wcale na inne cele, jak były przeznaczone, bywają obracane.
Potrzeba więc zaprzestać tej zmyślonej pobożności, a papież nie powinien być tyranem pod płaszczykiem wiary, lecz przeciwnie łaskawym ojcem,
jak jest miłosiernym ten, którego on się mieni być namiestnikiem na ziemi.

Chociaż papież może mieć najsłuszniejszy powód wymagania takich opłat od innych narodów pod pozorem obrony wiary powszechnej przeciw
niewiernym, wszelako, że Polska od tych danin wolną być powinna, tak słuszność, jako i rozumna zasada wymaga, albowiem od dawna na ciągłe
boje wystawiona, walcząca przeciw Turkom, Moskwie, Wołochom, dla obrony kraju stanowiącego przedmurze wszystkich państw chrześcijańskich,
tak jest z pieniędzy i skarbów wycieńczona i wyssana, iż jej prawie nie dostaje w tem potrzebnego zasobu na obronę sprawiedliwości
i utrzymanie spokojności domowej.
Ponieważ tedy Polska graniczy z takimi nieprzyjaciółmi, od których nietylko swych mieszkańców, ale i Śląsk, Morawy, Czechy
i całe prawie Niemcy kosztem własnym nieustannie zasłania i broni, słuszną jest, aby papież miał to na uwadze i nie wymagając
z Polski nadal takowych annatów, jakie po śmierci biskupów dla jego dworu mogłyby przypadać, skarbowi królestwa owszem oneż zostawiał,
aby bez uciemiężenia ubogiego ludu można pokój mieszkańców i spokojność domową i publiczną utrzymać dla wzrostu nawet innych państw
wyżej wymienionych.

<div class="ralign">Uwagi o naprawie Rzeczypospolitej,  
Piotr Chmielowski, "Obraz literatury polskiej w streszczeniu i celniejszych wyjątkach",
Warszawa 1898, tom\ I, str. 51--52.</div>

## Duchowne nieuki {-}

Jak źle dotychczas się dzieje, iż nieuków niezdatnych w stanie duchownym święcą, chociaż Paweł przestrzega, że niewypada
nikogo z nierozwagą wyświęcać. Jak wiele zabobonów i zgorszeń z ich niedołęstwa się wywiązuje,
pozna każdy dobrze myślący, z uwagą nad tem się zastanawiający.
Tylko postrzyżonym czubkiem różni się duchowny od świeckiego.
Aleksandra Galla, ledwie Donata^[podręczniki śrdniowieczne gramatyki łacińskiej] może lada jak czytać ów wyniesiony na księdza
przez zawdzianą sutannę tylko i postrzyżoną głowę, a chce cały świat naprawiać według swoich bredni.
Wrzeszczy, ba ryczy na kazalnicy, bo nikt mu się nie sprzeciwi.
Uczeni mężowie, panowie zamożni, nawet niższego stanu ludzie, nie ladajak oświeceni, z boleścią serca przysłuchują się bredniom tych,
co naukę wiary udzielać mają, a raczej bluźnią. Co za zgorszenie! Co za nieprzyzwoitość haniebna, że uczniowie uczeni,
nauczyciel nieuk, słuchacze pobożni, uczący zaś bezbożny i nieuk.

<div class="ralign">Tamże, t.\ I, str. 53.</div>

## Klasztory {-}

Panowie rządzący Rzecząpospolitą! Jakżeście ograniczeni, żeście dotychczas cierpieli, iż z klasztorów,
dobrami i dochodami przodków naszych uposażonych, żyjący na ziemi polskiej i z niej przez Polaków karmieni
waszych ziomków wyłączają i do zakonów nie przypuszczają, chyba dlatego może, że ich ustawa wiąże, aby samych tylko Niemców
do zakonu przyjmowali.
Ustawa jest śmieszną i prawom kościelnym przeciwną. Któż bowiem może się poważyć wolnemu królestwu polskiemu,
którego król niczyjego nie uznaje zwierzchnictwa, narzucać takie jarzmo pod fałszywym pozorem ustawy?
Nie dopuszczajcież nadal, waleczni mężowie, jeżeli za takich uchodzić chcecie, aby Niemcy, a zwłaszcza owe proste i zniewieściałe mnichy,
naigrawali się z narodu polskiego i zwodzili go fałszywą pobożnością.

<div class="ralign">Tamże, t.\ I, str. 53.</div>

## Trybunał Rzymski {-}

Ponieważ złoto i skarby kościelne zachowują się na potrzeby publiczne i nie powinny być ruszane z kościołów chyba tylko w
ostatnim niebezpieczóstwie kraju, przeto należy temu złemu zapobiegać, aby pod jakimbądź pozorem nie były wyprowadzane za granicę,
co się bardzo często zdarza, ilekroć chytrością kortezanów ^[dworaków, tutaj księży rzymskich] lub zaciętością pieniaczów nie tylko nagany,
ale i pozwy najrozmaitsze wynoszą do dworu rzymskiego.
Po trzy lub cztery lata zostaje tam sprawa bez rozpoznania, niekiedy wlecze się ona przez lat trzydzieści, póki jedna ze stron życia nie zakończy.
Lecz gdy dwór rzymski nie bierze, podług przysłowia, owcy bez wełny, trzeba chyba być z rozumu obranym, aby nie pojmować,
jak ogromne skarby wyprowadząją prawujący się, ile na tym ponosi uszczerbku królestwo, gdy za to do kraju bulle niewiedzieć jakie bywają sprowadzane.
Piękna w istocie zamiana! Wszelako nasi poczytują się za bardzo pobożnych, gdy takie brednie z czerwonymi pieczęciami i konopnianymi sznurkami
na drzwiach kościelnych przybite, uwielbiają.
Panowie Polacy, nie dajcie się dłużej zwodzić przebiegłym Włochom. Mamy w królestwie biskupów, mamy arcybiskupa i zarazem prymasa,
tamci niech rozpoznają sprawy, a ten niech ostatecznie wyrokuje, jeśli tego potrzeba.

<div class="ralign">Tamże, t.\ I, str. 52.</div>

## Odpusty {-}

I to nie jest bez obłudy, że papież, kiedy mu się podoba, pomimo woli nawet króla i panów, niewiedzieć jakie bulle jubileuszowymi zwane,
przysyła do Polski, dla wydrwienia pieniędzy, pod pozorem odpuszczenia grzechów, chociaż Bóg przez proroka powiedział:
"Synu! podaj mi serce twoje" -- nie rzekł: "daj pieniędzy"!
Zmyśla papież, że gotowizna tak zbierana, ma być obracana na budowę niewiedzieć jakiego kościoła.
gdy tymczasem skarby te idą, co jest pewnem, na potrzeby prywatne krewnych i powinowatych, na dwór, na stajnie, że nie powiem na co gorszego.
Żywe kościoły Boga, ludzie, są podstępnie łupieni na to, aby kościoły martwe wznosić.
Nie zbywa nawet na tak pobożnych, którzy nie rumienią się pochwalać takie brednie,
a sami nawet kapłani i spowiednicy wysyłają pełnomocników, aby ci tem większe mogli zjednać wpływy.
Złupiwszy lud, oddają się sami rozpuście i rozwiązłości!
O jakże się dajemy zwodzić. Polacy!
Zupełnie dostatecznego odpustu dostępuje każdy, kto pracuje i zbiera aby wesprzeć ubogiego
i kto na sprawę pospolitą wszystkich choć grosz składa.

<div class="ralign">Tamże, t.\ I, str. 52--53.</div>

## Tytuły Rzymskie {-}

Z niemałem to dla Kościoła dzieje się ubliżeniem, że wielu z majętniejszych gdy udadzą się do Włoch i trzy lub cztery tygodnie
u dworu rzymskiego jak go nazywają, pobawią, umieją wyjednywać dla siebie pewne tytuły i oznaki doktorskie.
Powraca stamtąd zawołanym doktorem ten, który niedawno był głupiuteńkim osłem.
Trzeba więc postanowić, aby takowi doktorowie uprzywilejowani, ani do godności, ani do prelatur,
choćby nawet byli szlachtą, nie byli dopuszczani, oprócz tych, którzy podług prawideł szkoły wyegzaminowani i wyzwoleni zostaną.

<div class="ralign">Tamże, t.\ I, str. 54.</div>
