# Stanisław Staszyc {-}

## Dobra Duchowne {-}

Ten każdy obywatel, który kocha swój kraj, wewnętrzną spokojność jego i biskupów cnotę,
winien podziękowanie wiecznemu w dziejach naszych sejmowi za porównanie intrat tych urzędników,
których dostojeństwo równe;
za usunięcie pokusy złego od duchowieństwa, na które cały lud gdyby na żywe cnoty obrazy patrzy.
A gdy widzi po tej stronie biskupów, po której złe broją,
po której na zgubę kraju rady;
po której cudzoziemskie, albo złych królów intrygi;
lud pospolity, nie widząc skrytej przyczyny,
sądząc, iż sama cnota świątobliwe męże prowadzi,
rzuca się ślepo, za szanownym przykładem, w sidła swojej zdrady.

Lud oświeceńszy gorszy się, znając powodem tej podłości, brzydką chciwość bogatszego biskupstwa.

Przez zrównanie intrat biskupów sejm w 1790\ r. z przyjaciół i niewolników tronu
powiększył liczbę poważnych przyjaciół i obrońców kraju.

----

Prócz plebana i biskupa, każdy inny duchowny jest niepotrzebny.
A tym samym szkodliwy. Bo tylko z cudzej pracy je i pije.
Takiego wydatku utrzymywać Rzeczpospolita nie jest w stanie,
kiedy związkiem politycznym przymuszona utrzymywać i żywić wojsko.

<div class="ralign">Przestrogi dla Polski.</div>

## Pańszczyzna {-}

Nieludzcy Polacy, odmieniliście w tym bliźnim waszym naturę ludzką.
Nierozumni, z niewolnika, który mógł być krajowi użytecznym, uczyniliście machinę nieczynną...

Pańszczyzna, ten dzikich hord wynalazek i to feudalnego nierządu straszydło,
a plemienia ludzkiego pochłoń, nie cierpi ludności.
Poddaństwo tak nierządne wali szaniec, aby w Polsce nigdy więcej
nad trzecią część ziemi nie mogli posiadać ludzie.
Inne urządzenie, inny sposób w lat kilka powiększyłby w dwójnasób obsady;
podwoiłby liczbę rolników w kraju,
pomnożyłby drugie tyle pracy i korzyści;
czynsz tam, gdzie pańszczyzna pięćdziesiąt chłopów umieszcza, osadziłby stu gospodarzy.
Wkrótce ta wieś powiększyłaby na swoich polach robotę i staranność,
rok w rok wzmagałyby się urodzaje i ludność...

Polaku, którykolwiek masz duszę i myślisz, tu zawstydź się i zadrżyj.
Zniszczyliśmy miliony ludzi bez pożytku dla kraju.
Ustawą pańszczyzny uczyniliśmy i tych wszystkich ludzi,
którzy się nad wymiar gruntu urodzą, niepotrzebnemi w naszej wsi...

Macież wy serce! I wy jesteście chrześcijanie!
Oszczercami są, nie nauczycielami wiary Chrystusa ci wszyscy kapłani,
którzy wam taką naukę podali i którzy wam powiadają,
że chociaż tak bezecnemi, tak okrutnemi żyjecie, możecie jednać się z Bogiem
i stać się uczestnikami łask tego,
który jedynie z miłości człowieka umarł.
Zapowiadam wam, że jeżeli Bóg jest sprawiedliwy,
nie może być w oczach jego zbrodni większej nad zbrodnię waszą.

Macież wy miłość ojczyzny? -- Nie obywatelami, ale nieprzyjaciółmi Polski jesteście.
Więcej szkodzicie temu krajowa, niżeli szkodzili
Moskale, Szwedzi, Niemcy, Turcy i Tatarzy.
Tych okrucieństwo padało na niektórych i skończyło się w lat kilka.
Wasze okrucieństwo trapi nietylko żyjących, ale nadto kładzie
przeszkodę wieczną do podobieństwa, aby Polska mogła ze sławą powstać.
Bo tamuje ludność, wstrzymuje powiększenie urodzajów,
rozciąga niewolę na pokolenia dalekich wieków.

Upamiętajcie się. Nie bierzcie na złe cierpliwości nieba.
Czas poprawy! Czas ludzkości!
Oddajcie człowieka Bogu.
Oddajcie człowiekowi prawo.
Niechaj rośnie i mnoży się.
Widzę straszne nieszczęście nad głowy waszemi...

Gdy już prawo oznaczy wydziałową robotę i zachęci, doradzi,
zapewni nagrodę jako dobrodziejom Rzeczypospolitej,
tym wszystkim, którzy w dobrach swoich ustanowią czynsze.
Gdy dla zachowania sprawiedliwości rolnikowi wyznaczony będzie między dziedzicem i rolnikiem sąd.
Gdy toż prawo nakaże, aby każdy paroch, każdy prebendarz, altarzysta,
miał za pierwszy swojego urzędu obowiązek uczyć czytać i pisać dzieci chłopskie.
Potym zaraz toż prawo urządzi rolnika stan polityczny: powróci człowiekowi wolność naturalną,
to jest każdy gospodarz, który obejmie grunt, obejmie go na całe życie.
Przeto dziedzic nie może go z tego gruntu zrzucić, bez okazania mu w sądzie,
iż nieposłuszny prawu nie odbywał porządnie wydziałowej roboty,
albo w dobrach czynszowych iż czynszu nie płacił...

<div class="ralign">Tamże.</div>

## Bóg i pokój {-}

<div class="verse">
Bóg chce, abyśmy byli wszyscy szczęśliwemi;\
Że nieszczęśliwym człowiek, to ludzie zdziałali.\
Zimna uwaga władnąć będzie myślą, słowy,\
Gdzie szukać będę wiecznych stosunków ustawy,\
Gdzie wykażę, że od nih zdalali się ludzie,\
Gdzie ich zbłąkały zmysły, zwiodło uprzedzenie.\
Dam miejsce czuciu kiedy ujrzę zwodzicielów\
Lub samodzierżców chciwych, nieczułych oszustów.\
Niechajże nic przede mną nie doła kryć prawdy!\
Zapominam ród, nie mam stanu, ni ojczyzny,\
Z przesądów za usilną otrząsam się pracą;\
Ten jedyny głos tylko w mojem słyszę sercu,\
Żem człekiem, a bliźnimi wszyscy, którzy cierpią.\
Z tem ućzuciem zdalon z zgiełku, w spokojnem ustroniu\
Nad namiętności wszystkich wznosiłem się poziom;\
W głębokiem zamyśleniu zbiegłem okrąg ziemi;\
Sto części ludzi bronię, --- jedna mi przeciwną:\
Niechajże rodzaj ludzki moim będzie sędzią!\
Boże odwieczny! Twojej wszechmocności godna\
Dziś z tej ziemi do Ciebie podnosi się prośba:\
Wsparcia Twojego nie jeden człowiek nieszczęśliwy,\
Lecz uciemięźony błaga rodzaj cały.\
Ty znasz mą chęć, Ty widzisz w tej chwili me serce:\
Ono tak ku moim bliźnim miłości jest pełne.\
Oświeć mój rozum: niech poznam; tłumaczę się jaśno:\
Bogdajby pojęli wszyscy, żeś ich nie stworzył na to,\
By nieszczęśliwi byli; bogdajby poznali\
Rzeczywiste swojego nieszczęścia przyczyny!\
Zniszcz ciemności, światła Stwórco, rozszerz w ludziach światło:\
A człek nieprzyjacielem nie będzie człowieka,\
Swe zbójeckie przekuje w sierp, lemiesz żelazo.\
Od tąd się nie przybliży do Twego oblicza\
Próżniak burzliwy z ciała, a z duszy nikczemny;\
Ani to człowieczeństwa bezecne straszydło,\
W którem duch złakomiony, oraz pychą dęty\
Znieważa, napastując, Bóstwo, by wspierało\
W niszczeniu ludów tysiąc srogie czyny jego;\
Ani się pozuchwali wejść do Twej świątyni\
Z opluszczonemi świeżo krwią ludzką rękami\
Ten wojarz, co za pomoc do zabójstw, łupiestwa,\
Dziękując niebu, gromów nie zląkł się, bluźnierca,\
I zdzierstw połowę kładzie na Bóstwa ołtarzu.
</div>

<div class="ralign">Z poematu "Ród ludzki", z księgi I.</div>

## Oświecenie {-}

<div class="verse">
Wiaro o konieczności oświecań się rodu,\
Ty wielkością powołań wznosisz duszę w człeku,\
Wskazujesz cel prawdziwy w jego przeznaczeniu;\
Miłością szczęścia zmuszasz wszystkich do porządku.\
W tobie, gdzie cnoty triumf, spostrzegać tę przyszłość,\
W której złych jawność, w powszechnym sądzie sprawiedliwość,\
Wszystkich ciemności przepaść, dobrych chwała wieczną.\
Do tych osiągnień tylko rozum, cnoty wiodą\
I tylko te ludzkości czyny, myśli, dzieła,\
Co zwiększają rodzaju doskonałość, szczęście.\
Dobroczynnej otuchy święta mędrców wiaro!\
Z tobą te czasy dojrzeć przynajmniej mi wolno,\
W których niweczeć będą wszech gwałtów układy,\
Rachuby, przedsięwzięcia zburców mej ojczyzny.\
Choćby co tak rozsądnem stawiasz, było marą,\
I wtenczas bym cię nazwał jeszcze dobroczynną,\
Bo najdotkliwszy ciężar ulży wasz mi życia,\
Wróżąc gwałcących zgubę, zgwałconych powstania.
</div>

<div class="ralign">Tamże, ks.\ XVI.</div>

## Zaboboncy {-}

<div class="verse">
Gdy duch jednodzierżstw zmienił w poddaństwo hołdowców,\
Mniej potrzebną czuł dalszą pomoc zabobońców.\
Zaprzestał wierzyć... Wkrótce na gromy, przeklęstwa\
Złościł się, rwał przymierze z tronami ołtarza.\
Uznał, że niepotrzebną w rządzie zaboboństwa wiara.\
Wnet on się na duchownych pierwszy rzucił dobra,\
On rozhełzywał mówców uczonych języki,\
On odkrycie muftyzmu oszustów i zbrodni,\
On oszczerzył, upodlił, głowę moschi shydził,\
I dotąd przeniewierców w trójkoron ubierał,\
Dopokąd lamów kornych przy nogach nie ujrzał.\
Pierwszy znienawidził, zatrząsł lamismu filary,\
Porozpędzał derwiszów, zburzył mnichów gmachy,\
A na ich miejsca wskrzeszał zgromadzenia mądrych...\
Dobywając się z poddaństw lamy, do swych tronów\
Zwoływał wolnowierców, mędrców i rzeczników.\
Dawał do zrozumienia, co go słuchać chciało,\
Owszem do ludów wielu przemawiał sam głośno:\
"Już czas, aby ukrócić zuchwałość muftyzmu.\
Zjarzmiłem, dzierżców memu poddałem mocarstwu:\
Już mi posłuszne mirzy, a krocie w krom ostrza\
Panowanie warują stalą mego berła.\
Jeden bonz śmie natrząsać, grozić mej potędze!\
Napuszony łakomstwem i pychą on pragnie\
Przerzucać berły, stąpać po karkach panów, władców!\
Uczcie świat, że on nigdy chęcią dobra ludów,\
Nie był wiedziony szczęścia, swobód ich zamysłem;\
Lecz żądzą panowania nad światem, rozumem;\
Rzucał przeklęstw gromy, albo sztylet mordu\
Na mocarzów, którzy w nim hamowali dumę,\
Albo łakomstwu jego chcieli stawić miarę...".
</div>

<div class="ralign">Tamże.</div>
