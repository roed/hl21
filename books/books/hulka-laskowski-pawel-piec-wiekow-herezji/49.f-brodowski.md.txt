# F. Brodowski {-}

## Polak to katolik {-}

Można sobie wyobrazić Polaka sprzedawczyka,
renegata, wyzbywającego się wyznania dla pełnej misy,
-- ale pomyśleć Polaka wyznania mojżeszowego, prawosławnego,
nawet luteranina nie można.
Wierzę, że to będzie najzacniejszy człowiek,
przywiązany i poświęcony syn tej ziemi.
Jednak coś mu brak, i to najważniejszego
-- możności uzyskania bezpośredniego kontaktu z duszą narodu.
Niech przyjmie katolicyzm i na pierwszej mszy katolickiej,
w najskromniejszym kościołku wiejskim,
otworzy mu się furtka dotąd przed nim zabita.
Do przekroczenia zaś jej nie może tęsknić,
gdy w nim tkwi choć źdźbło polskości...
Pierwiastkiem ducha myśmy daleko od Francuzów bogatsi,
i jeśli kiedy Bóg sprawi, że przewodnictwo intelektualne i duchowe
przejdzie od nich do nas,
to będzie to za sprawą wierności naszej tym pierwiastkom,
które z nas ostoję katolicyzmu uczyniły.
To nie wyznanie, to polot ducha...
Nie, katolicyzm w Polsce nie jest tylko wyznaniem,
gdyż wrósł w rdzeń naszej kultury duchowej,
nasza dusza nim obrosła,
a to się stało dlatego, że nie katolicyzm Polskę,
lecz Polska wzięła katolicyzm w siebie i uczyniła go polskim.

<div class="ralign">
Moja biografia, 1915.\
O duszę Polski, 1916.
</div>
