# Stanisław Szczepanowski {-}

## Idea polska wobec prądów kosmopolitycznych {-}

### I

W ciągu ostatnich wyborów nieraz zabrzmiało hasło:
precz z kosmopolityzmem!
Hasło to jednostronne jest nadzwyczaj wygodne, ale niebezpieczne.
Wygodne, bo prostą negację otacza aureolą patrjotyzmu
i upatruje kwintensencję polityki narodowej w tem,
ażeby nie dzielić i nie rozumieć prądów światowych.
Hasło to jest bezskuteczne, bo historja nie daje przykładu,
ażeby jakikolwiek naród obronił się od wrogich mu prądów
światowych w inny sposób,
jak przez unarodowienie tych prądów
i wcielenie ich treści zbawiennej do własnej polityki narodowej.
Prąd światowy chrześcijaństwa zawitał do nas w wieku X,
a do Litwy w wieku XIV w postaci zaboru niemieckiego.

Niemcy zwyciężali dopóki byli w wyłącznem posiadaniu tej broni duchowej,
której im dostarczyło chrześcijaństwo.
Zwycięstwo się odwróciło w chwili, gdy Polska i Litwa zamiast beznadziejnego hasła:
precz z kosmopolityzmem! -- sprzymierzyła się z chrześcijaństwem
i utworzyła kościół narodowy chrześcijański.
Odtąd ta wielka siła światowa działała na naszą korzyść
i potęgowała nasze własne siły narodowe.
To jest jedyny wzór, godzien naśladowania
wobec każdego prądu światowego,
czy on się pojawia na polu
religijnem, naukowem, artystycznem, ekonomicznem lub społecznem.
Jedynem zadaniem zawsze będzie przyswoić sobie siły nowego prądu,
bez zatracenia indywidualności narodowej
-- zapanować nad nim,
a nie stać się ślepem narzędziem polityki innych narodów lub organizacji.
Światowe prądy społeczne ześrodkowują się wszystkie
około zadania reformy społecznej,
czyli w języku chrześcijańskim: zaprowadzenia królestwa Bożego na ziemi.

Nie ten naród wyjdzie zwycięsko z próby ogniowej walki
z rozhukanymi żywiołami społecznymi,
który się zasklepia w samym sobie i tych nowych prądów znać nie chce,
ale ten, który ich treść zbawienną najprędzej urzeczywistni
i nada im najdoskonalszą formę.
Kwestja reformy społecznej przychodzi do nas w kilku postaciach.
Naprzód we wstrętnej postaci materjalistycznej socjalizmu żydowsko-niemieckiego Lassala i Marxa,
który godzi wprost w duszę narodu,
tak jak przed stu laty materjalizm polityczny zeszłego wieku,
materjalizm Katarzyny, Fryderyka W. i Józefa II ugodził w ciało narodu
i doprowadził do zagłady politycznej Polski.

Ale jedyną bronią przeciwko tej morowej zarazie
jest nie negacja kwestji społecznej,
ale jej samodzielne rozwiązanie na podstawie idei polskiej.
Trzeba czynem pokazać, że w zapomnianem "Ojcze nasz"
Augusta Cieszkowskiego i w naszem wielkiem objawieniu narodowem XIX wieku
tkwi głębszy zarodek przyszłości, niż w sofizmatach współczesnych,
i że narodowa a uniwersalna zarazem idea obowiązku obywatelskiego
lepsze może wydać owoce
od obcej i nam wstrętnej idei przymusu państwowego.
Prądy światowe, tak często nam wrogie, dzisiaj przychodzą nam w pomoc.
Materjalizm socjalistów przedstawia się wobec dzisiejszego stanu literatury jako zacofanie,
jako ostatnia fala wielkiego ruchu umysłowego wszechpotężnego w XVIII wieku,
a który jako moda dawno porzucona, dotarł dopiero dzisiaj do
szerokich warstw ludowych,
podczas, kiedy te warstwy, w których powstaje twórcza literatura,
już dawno przeszły do innych zapatrywań.

Cechą wybitną obecnych czasów jest odrodzenie się wszędzie
myśli chrześcijańskiej i przewaga wpływu uczuć nad racjonalizmem.
Nawet w odrodzeniu się chrześcijaństwa można dopatrzyć się
przewagi formy uczuciowej, t.\ j. katolicyzmu nad formą
racjonalistyczną, t.\ j. protestantyzmem.
Wątpić nie można, że w całości ten prąd jest dla nas korzystny,
ale i wobec niego należy zachować naszą samodzielność narodową,
ażeby Polska nie stała się znowu,
jak za dynastji Wazów, ślepem i nieuświadomionem
narzędziem kosmopolitycznej polityki jezuickiej.

I tu znowu tylko idea polska może nas wyratować.
Nasza wielka literatura jest jedyną,
która podniosła ideę Ojczyzny do potęgi ideału religijnego
i zrobiła z narodowości świętość nienaruszalną,
a jako cel ludzkości postawiła nie zagładę narodów
lub triumf jednego z nich,
ale harmonijny rozwój wszystkich,
każdego w służbie swojego posłannictwa.
Toteż obok wszystkich kwestyj polityki potocznej z dnia na dzień,
pozostaje nam jako pierwszy obowiązek
rozwój naszych idei narodowych
jako najpewniejsza podstawa postępu
i ochrona samodzielności narodowej
wobec wszelkich wrogów zewnętrznych czy wewnętrznych.

----

Wobec prądów kosmopolitycznych więc,
nie prowadzi do celu ani opór bierny,
ani ślepe naśladownictwo,
tylko samodzielne przetrawienie obcej myśli na rodzime soki żywotne.
Potrafi to tylko naród myślący samodzielnie,
posiadający swe własne ogniska rozwoju umysłowego.
Polska za czasów swej niepodległości
nigdy się nie podniosła do tego poziomu
i w tem leży głębsza przyczyna jej upadku plitycznego.
Za czasów odrodzenia akademja krakowska zabłysła na chwilkę własnem światłem,
wydając Kopernika, Grzegorza z Sanoka
i innych mistrzów myśli samodzielnej i przodującej światu.
Z tym wyjątkiem Polska była raczej pobojowiskiem,
na którem obce myśli staczały walkę,
świeciła często, ale światłem pożyczanem,
niejednokrotnie posiadała plejady wykształconych obywateli,
jak za czasów Zamojskiego i Kochanowskiego,
ale wykształconych na obczyźnie, bo Polska nie wyrobiła sobie własnego ogniska umysłowego.
Chcąc poznać rozwój umysłowy Polski w wiekach XVI, XVII i XVIII,
trzeba poznać ruch umysłowy Włoch, Francji i Niemiec,
skąd Polska zapożyczała idei,
wznosząc się lub upadając razem z ruchem umysłowym tych krajów,
od których umysłowość polska prawie niewolniczo zależała.

Nieszczęście wywiera w życiu narodów niejednokrotnie ten sam wpływ,
co w życiu jednostek.
Wywołuje żal za zmarnowaną przeszłością,
skruchę za błędy i przewinienia,
które doprowadziły do upadku,
a w skupieniu ducha, które następuje,
powstaje zamiar nowego i szlachetniejszego życia.
Katastrofa, która zamknęła dzieje Polski historycznej,
stała się wprost wstępem do dojrzałości duchowej narodu polskiego.
W literaturze romantycznej i we wspaniałym rozwoju myśli polskiej
na całym obszarze pracy duchowej naród odkrył samego siebie,
poraz pierwszy zrozumiał własną treść i własne posłannictwo,
i wyzwolił myśl narodową z więzów naśladownictwa i z więzów bierności.
Wszystkie twórcze i dodatnie pierwiastki myśli światowych
przyswoiła sobie i na nich wzniosła się myśl polska.

Ale znowu realizacja nie dorównała zapowiedzi.
Posiadamy wprawdzie wspaniałą literaturę przyszłości,
ale naród jej nie zrozumiał,
unosi się nad jej pięknością estetyczną,
upaja się także jej dążnością patrjotyczną,
ale nie chce poznać jej treści uniwersalnej.
Wypadki roku 1846, a jeszcze bardziej 1863,
zrodziły zwątpienie o sile idei narodowych,
i, posiadając objawienie szczytniejsze od objawienia innych narodów,
znowu udaliśmy się w służbę do cudzych bogów.
Polska i dzisiaj nie posiada ogniska samodzielnego myśli narodowej
i znowu jest tylko pobojowiskiem,
na którem walki staczają obce prądy:
klerykalizm, ultramontanizm, liberalizm, pozytywizm,
materjalizm i socjalizm.

Na obszarze ziem polskich istnieją trzy uniwersytety,
w Warszawie, w Krakowie i we Lwowie.
Wychowują one urzędników, profesorów i lekarzy,
niema zakładu do wychowywania obywatela polskiego.
Nie zabraknie nam sieczki prawniczej, filologicznej,
lub recept aptekarskich,
ale nie mamy filozofji, przedewszystkiem nie mamy filozofji polskiej.
W mozolnem dziele Henryka Struvego:
"Wstęp krytyczny do filozofji",
z niesłychanym trudem i aleksandryjską pracowitością
zachowane są od zapomnienia głosy
wszystkich polskich pracowników na niwie filozofji,
a raczej wszystkie echa obcych myśli,
wszystkich "papug i małp",
któremi się nasza literatura wykazać może,
a starannie ukryte jest to,
co cechuje samodzielną myśl polską.
W spisie polskich autorów filozofji znajdujemy nazwiska, jak
Załęski i Masłowski, a brak Mickiewicza i Krasińskiego!

Pierwszym krokiem do skutecznej walki z obcymi prądami
politycznymi i społecznymi jest wyzwolenie się z tej niewoli
duchowej,
w której od upadku powstania 1863 r. myśl polska jest pogrążona.
Znowu wstępujemy na tę samą drogę,
jak w końcu XVI wieku,
kiedy reakcja jezuicka zaczęła opanowywać Polskę.
Znowu zagraża nam niewola umysłowa i anarchja polityczna,
podczas kiedy przeciwnie cechą zdrowych narodów
jest swoboda myśli, a karność w działaniu.

Nawet sąd o przeszłości,
o strasznej epoce zastoju umysłowego w Polsce w wiekach XVII i XVIII
stał się dzisiaj chwiejnym i niepewnym.
Cała falanga duchowych przywódców narodu,
pracujących od Konarskiego nad duchowem odrodzeniem Polski,
była jednego zdania co do zgubnego działania wychowania jezuickiego.
Kołłątaj, Staszyc, Tadeusz Czacki, Mickiewicz,
Słowacki, Krasiński, Trentowski, Cieszkowski, Libelt,
jednozgodnie upatrują w niem zgubę Polski.
Ale dziś O.\ Załęski nam przeciwnie tłumaczy, że nie jezuici zgubili Polskę,
ale że Polacy byli narodem tak niedojrzałym i niedołężnym,
że własną ciasnotą i anarchją zepsuli i spaczyli
nawet szlachetność i cnotę tak niezawodną,
jak cnota i szlachetność jezuitów!

To zwichnięcie myśli polskiej przed trzema wiekami pod
wpływem jezuityzmu zasługuje na bliższe rozpatrzenie,
bo maluje nam najlepiej niebezpieczeństwo
takiego poddania się obcym prądom umysłowym,
jak to, które nam grozi w czasach obecnych.

### II

Polska była i jest narodem katolickim.
Są Polacy innego wyznania,
najlepsi patrjoci nieraz do nich należą.
Na Śląsku austrjackim ruch narodowy rozpoczął się u protestantów,
a nie u katolików.
Kalwin Potworowski był długoletnim przywódcą narodu polskiego pod zaborem pruskim;
mamy patrjotów żydów w Warszawie.
Z tem wszystkiem oczywistem jest dla każdego,
który ma oczy, ażeby widział
-- że Polska jako naród jest katolicką --
staje się coraz bardziej katolicką.
Jeżeli głębokie przyczyny psychologiczne nie dopuściły u nas
do reformy religijnej w wieku XVI,
to stokroć bardziej myśl podobna jest wykluczoną
w XIX lub XX wieku.
Polska więc będzie i nadal katolicką, albo jej nie będzie.
Twierdzenie to można przyjąć jako pewnik.
Polityk, lub mąż stanu polski,
któryby tego pewnika nie uznał,
musiałby chyba wyjść z potwornego założenia,
że społeczeństwo może się rozwijać bez religji,
ale w tej chwili stanąłby nietylko poza obrębem własnego narodu,
ale też poza obrębem ogólnego doświadczenia całej ludzkości.

Polska więc będzie katolicką
i będzie w przyszłości dzieliła losy katolicyzmu,
jak je dzieliła w przeszłości.
A te losy straszne są od trzech wieków,
od chwili, w której powstała reformacja i dojrzała rewolucja.
Gdy patrzymy na to, co się od tego czasu dzieje z narodami katolickimi,
to serce się ściska i strach przejmuje na myśl,
że my do nich należymy -- należeć musimy.
Gdyby przyszłość nie miała być lepszą od przeszłości,
to los taki promienia nadziei lepszego bytu nie zostawiłby.
Bo w tym okresie narody katolickie żyją jakby pod klątwą bożą,
wydziedziczone od wszelkich nabytków postępu i cywilizacji,
tem bardziej upośledzone, w tem głębszą przepaść pogrążone,
im bardziej, im wyłączniej są katolickimi.

Rozpatrzmy się na początku XVIII wieku w chwili,
gdy prawowierność katolicka doszła do ostatniego kresu swej potęgi,
kiedy jezuici rządzili wszystkimi krajami katolickimi.
Każdy Hiszpan był prawowiernym katolikiem,
samo posądzenie o innowierstwo lub niedowiarstwo
pociągało za sobą śmierć na stosie
-- ale Hiszpanja, płynąca mlekiem i miodem za panowania Maurów,
stała się pustynią.
Obok prawowierności rozpostarły się:
ciemnota, próżniactwo i ubóstwo po całym kraju,
pomimo wszystkich skarbów nowego świata.
Tak samo w Portugalji.
W prawowiernych Włoszech i w Neapolu ohydne żebractwo
gnuśnych lazaronów w stolicy,
a rozbójnicy i Camorra na prowincji.
Nie lepiej w Rzymie pod okiem samego papieża.
W Niemczech, gnuśna Austrja w przededniu wiekowej walki
z protestanckimi Prusami,
która się miała skończyć zupełną klęską strony katolickiej
i wyrzuceniem Habsburgów z Niemiec.
O prawowiernej Polsce, za Sasów, za boleśnie jest mówić.
W Irlandji, owej niegdyś wyspie świętych,
skąd szli misjonarze na nawrócenie całego zachodu Europy,
katoliccy Irlandczycy, kryjący się w norach i jaskiniach
przed panującymi protestantami.
Francja, po wypędzeniu Hugenotów i stłumieniu Jansenistów,
uzyskała Wprawdzie prawowiemość,
ale równocześnie popadła za regencji Ludwika XV
w bezprzykładną rozpustę,
od której ją ta prawowierność zachować nie potrafiła.
A jednak we wszystkich tych krajach
każde skinienie jezuity było rozkazem,
każde jego życzenie co najprędzej w czyn się wcielało.
A niech mi kto w tym okresie pokaże
choć jedno społeczeństwo katolickie,
któreby nie obudzało litości, wstrętu, lub pogardy.
Cały ruch cywilizacyjny przeniósł się do protestantów,
a kiedy nareszcie narody katolickie obudziły się z letargu,
to postęp Francji, Austrji, Hiszpanji, nawet Neapolu i Polski,
stoi w prostym stosunku do wzrostu niedowiarstwa,
i co się tylko w tych krajach w wieku XVIII dokonało cywilizacyjnego,
to było zarazem antykatolickie.

Ta niższość cywilizacyjna narodów katolickich trwa do dzisiaj.
W protestanckiej Ameryce jak potrzebują robotnika nieuczonego, na służbę,
na ciężką i mało intratną robotę,
na to, co w Biblji uważane jest jako piętno niewoli,
na "noszenie wody i rąbanie drzewa",
to pan i przedsiębiorca protestancki zaraz znajdzie katolików trzech narodowości:
Irlandczyków, Włochów i Polaków -- do czyszczenia butów,
do noszenia wody i rąbania drzewa;
katolików, którzy się jeszcze biją i gryzą o te
ochłapy i suche kości, których się rodowity protestant, Amerykanin,
tknąć nie chce.
Zaprawdę, zaprawdę Polska dzieli losy katolicyzmu!
Nie jest nawet ostatnią pomiędzy narodami katolickimi.
Psychologiczne podobieństwo jest także aż nadto przerażające
w losach katolicyzmu i w losach Polski:
ta sama przepaść pomiędzy wspaniałą zapowiedzią,
a mizemem wykonaniem,
to samo spaczenie najwznioślejszych idei.

Bo przecież w myśli religji katolickiej,
to jest uniwersalnej -- religji obejmującej całą ludzkość,
jest coś wielkiego i wzniosłego,
coś, co jest siłą nieprzepartą,
zniewala wszystkie wyższe umysły.
Tak, jak za czasów Bossueta,
tak i dzisiaj, kto rozglądnie się w protestantyzmie,
dostrzeże tam sekty -- sekty bez liku,
szacowne, użyteczne, pobożne
-- ale nic, coby mogło zasłużyć na nazwę kościoła powszechnego,
kościoła ludzkości.

Ta idea wyższa, która stanowi zagadkową siłę żywotną katolicyzmu,
tak jak pociąga dzisiaj,
tak samo pociągała za czasów powstania reformacji.
W Niemczech największy umysł XVI wieku, Erazm z Rotterdamu,
mąż uniwersalnego wykształcenia, reformator z usposobienia,
karciciel nieubłagany ciemnoty, swawoli i rozpusty,
które się za jego czasów rozpostarły w kościele
i dosięgły aż do samej stolicy apostolskiej, pomimo tego pozostał katolikiem.
To samo w Anglji,
Tomasz Morus, autor "Utopji",
znowu reformator światły i prawy,
najpotężniejszy umysł ówczesnej Anglji,
nietylko pozostał katolikiem, ale śmierć poniósł za swoją wiarę.
Największy umysł tego samego wieku we Francji, Montaigne,
dochodził do krańcowej swobody myśli
-- był osobistym przyjacielem wielu protestantów,
ale uważał protestantyzm za coś ciasnego i krępującego.
Zaprawdę, jeżeli u nas Kochanowski i Zamojski pozostali katolikami,
to znaleźli się w dobrem towarzystwie.

Ale jeżeli ludzie tej miary,
jak Erazm, Morus, Montaigne i Zamojski pozostali katolikami,
to z pewnością ich pojęcie katolicyzmu musiało być inne,
jak pojęcia jezuitów,
którzy od końca XVI wieku opanowali, a raczej opętali,
wszystkie społeczeństwa katolickie.
Bo też czas największego poniżenia narodów katolickich
najdokładniej zgadza się z wszechpotęgą jezuitów.
Od ich upadku w połowie XVIII wieku
datuje się to odrodzenie uczuć religijnych,
które trwa dotąd.

Cechą tego odrodzenia jest fakt,
że tak samo, jak Erazm
(był pierwotnie księdzem, ale odrzucił kardynalstwo
i od Juljusza\ II otrzymał dyspensę od ślubów kościelnych),
Morus, Montaigne i Zamojski byli to ludzie świeccy,
a pomimo to wysoko się wznosili nad całym kościołem urzędowym swego czasu,
tak wszyscy główni twórcy obecnego odrodzenia religijnego,
a mianowicie katolickiego, także byli świeccy.

Zwrot od niedowiarstwa zaczął się we Francji od wyznania
wiary proboszcza Sabaudzkiego --
<span lang="fr">*Confession de foi du vicaire Savoyard*</span>,
t.\ j. od protestanta Rousseau‘a.
Rewolucyjną Francję powołał do wiary nie ksiądz, nie jezuita,
ale świecki Chateaubriand, napisawszy
<span lang="fr">*Le Génie du Christianisme*.</span>
Dogmatykę społeczną katolicyzmu postawił świecki De Maistre.
Ruch katolicki prowadził dalej świecki Montalambert,
a dziś najbardziej wpływowym katolikiem we Francji jest świecki hrabia de Mun.
Tak samo ruch katolicki w Anglji zaczyna się od radykalnego polityka Cobbeta,
a w Niemczech od dziennikarza Gorresa,
którego imię dotąd służy wielkiemu stowarzyszeniu,
prowadzącemu ruch katolicki w Niemczech.

----

W tej wyższości pierwiastka świeckiego nad pierwiastkiem
kościelnym w sprawach najżywotniejszych dla samego kościoła,
która się pojawia od pewnego czasu, okazuje się tylko wyższość
prawdy życiowej nad subtelnościami teologicznemi.
Na ludzi stojących w wirze wypadków,
wśród rozhukanych fal społecznych i politycznych,
spory teologiczne robią to samo wrażenie,
jak szwargot zacietrzewionych mnichów bizantyńskich
podczas ostatniego oblężenia Konstantynopola,
jeszcze zapamiętale rozprawiających o literze "i"
w *homousios* i *homoiousios*,
podczas kiedy Mahmud\ II już wkraczał do miasta przez zdobytą bramę.

Każda ważna chwila wymaga czynów, a nie teorji lub sofizmatów,
i to czynów, zastosowanych do potrzeb chwili.
Siła chrześcijaństwa, a mianowicie katolicyzmu,
polega właśnie na łatwości takiego zastosowania się do potrzeb chwili.
W tej jego zmienności,
w tej elastyczności historyk widzi zaletę,
a tylko skostniali teologowie uroili sobie jakiś nieistniejący,
skostniały kościół ten sam zawsze i wszędzie.

I najstarsza organizacja żyje tylko przez nowatorów,
a ginie, jak tylko popadnie w rutynę.
A sekretem skuteczności działania nowatorskiego
jest sympatja i zgodność z duchem swoich czasów.
Chateaubriand przeciwstawiając Francji rewolucyjnej religję,
jako źródło natchnienia dla sztuk pięknych,
prawdopodobnie był kiepskim teologiem,
znalazł się w namacalnej sprzeczności z świętym Augustynem,
który w arcydziełach sztuki klasycznej widział inkamację szatańską.
Od świętego Augustyna do Rafaela jedenaście wieków,
od Rafaela do Chateaubrianda znowu trzy wieki.
Tyle czasu trzeba było na dojrzenie jednego z najważniejszych
dogmatów religji powszechnej, t.\ j. zgodności ideału piękna z ideałem etycznym.
Każdy dogmat, objawiający jakąś nową prawdę życiową,
staje się nowym zarodkiem czynów,
przeistacza życie -- podczas kiedy cechą dogmatu teologicznego
jest to, że jest martwą i pustą literą,
nie zmieniającą na włos działania ludzkiego.

Chateaubriand otoczył religję czarem poezji,
opromienił urokiem piękności,
na nowo pobudził do życia wyobraźnię twórczą
która składa zarodek cudów w sercu ludzkiem,
w woli ludzkiej -- cudów o wiele donioślejszych
od pospolitej taumaturgji gminnej.
Od czasów Chateaubrianda, każda ruina gotycka,
każda melodja wspaniała, kżda powieść bohaterska,
stała się krzewicielką i roznosicielką katolicyzmu,
stokroć skuteczniejszą od kazuistyki i scholastyki jezuickiej,
przed któremi cały świat wykształcony sobie uszy zatyka.
Purytanizm angielski, dzisiaj jeszcze desperacko imający się
słusznych w swoim czasie wyobrażeń św. Augustyna,
przedstawia wobec nowożytnego katolicyzmu
zacofanie i nietolerancję.

Objawienie narodowe polskie -- objawienie
Mickiewicza, Krasińskiego, Słowackiego, Cieszkowskiego
-- wniosło do Kościoła powszechnego ideę Ojczyzny jako świętości,
na równi ze świętościami dawniej uznanemi.
Zgorszyłby się na to św. Grzegorz Nazjanzeński,
walczący w IV wieku z Juljanem Apostatą i z przemocą państwową.
Piorunująca jego wymowa przedstawia marność ojczyzny doczesnej,
a piękność jedynie prawdziwej ojczyzny niebieskiej.
Pomimo tego, naród polski słusznie
łączy wyobrażenia religijne
ze zbawieniem swej ojczyzny ziemskiej
i ze zwycięstwem sprawiedliwości bożej
w stosunkach narodowych i społecznych.
Tędy idzie jedyna droga do krzewienia wyobrażeń religijnych u Polaków,
i dlatego ks.\ Dr. Golian, powtarzający to, co dla św. Grzegorza
za czasów cesarstwa rzymskiego było bohaterstwem
-- obudzą tylko wstręt,
bo namawiając naród do zapomnienia o ojczyźnie ziemskiej,
namawia go do podłości i nikczemności.

Kościół powszechny, gdyby do dogmatu piękna,
bohaterstwa i patrjotyzmu mógł dołączyć jeszcze dogmat
o wolności ducha,
o świętości prawdy i wiedzy,
o naczelnym obowiązku nieustraszonego poszukiwania jej,
naprzekór wszelkim tyranjom i wszelkim przemocom,
toby zaprawdę stał się niezwyciężonym
i wszystkie piekła daremnie by się przeciwko niemu wysilały.
Jeszcze ten jeden szczebel do zdobycia,
a zwycięstwo ostateczne będzie niechybnem i niewątpliwem.

Historja kościoła powszechnego, pojęta w ten sposób,
zupełnie się inaczej przedstawia,
niż w postaci zbutwiałych i omszonych subtelności teologicznych.
Kościół urzędowy i oficjalny rzadko się też zdobywa
na uchwycenie żywych prądów czasu.
Stąd jego martwota i nieporadność w krytycznych chwilach,
o której tak często mówi Mickiewicz,
a której świeżą mamy ilustrację w nieporadności naszych biskupów
i księży wobec ruchu ludowców i wobec niesubordynacji księdza Stojałowskiego.

Zresztą ospałość kościoła urzędowego jest zjawiskiem,
które się ciągle w historji powtarza.
Gdzież jest udział kapłanów i arcykapłanów żydowskich w tworzeniu Pisma świętego?
Wszystko, co jest w nim najszczytniejszego,
jest dziełem nieoficjalnych proroków
-- owych niepowołanych rewolucjonistów i agitatorów swojego czasu.
Tak samo historja chrześcijaństwa,
przedstawia się jako historja wpływu,
pojawiających się od czasu do czasu, nieurzędowych założycieli zakonów,
którym kościół urzędowy nie wystarcza,
a którzy z czasem piętno swojej gorącej indywidualności wyciskają
na dawnej organizacji i napełniają ją nowym duchem.
-- Największą zaś ilustracją martwoty i nieporadności urzędowego kościoła
w nowszych czasach jest powstanie i historja zakonu jezuitów.

<div class="ralign">[Myśli o odrodzeniu narodowem](http://archive.org/details/mylioodrodzeniun00szcz).
Zebrane przez Helenę Szczepanowską i Antoniego Plutyńskiego,  
Lwów 1907, wyd.\ 2, tom\ I, str. 193--206.</div>
