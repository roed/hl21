﻿# Kościół około r.\ 1209

Święty Franciszek był w najlepszem znaczeniu słowa człowiekiem
natchnienia, ale byłoby grubym błędem wyrwać go z jego stulecia i
studjować poza temi warunkami, śród których żył.

Prawda, że dążył świadomie do naśladowania Jezusa, lecz tego, co
wiemy o Chrystusie, jest zbyt niewiele, abyśmy życiu świętego
Franciszka odebrać mieli jego oryginalność. Przeświadczenie, że jest
naśladowcą, miało ten skutek, że ochroniło go przed wszelką
możliwością pychy. Pozwoliło mu to głosić swe myśli z siłą
niezrównaną, przyczem nie zdawał sobie sprawy, że głosi samego siebie.

Nie należy tedy ani wyosabniać go, ani przedstawiać zbyt zależnym.
Właśnie w okresie jego życia, do którego doszliśmy (1205--1206),
położenie religijne Włoch miało zaważyć na jego myśli i skierować go
na drogę, którą miał kroczyć.

Kler był w obyczajach swoich zepsuty jak nigdy, i przez to
uniemożliwiał jakąkolwiek reformę poważną. Wśród odszczepieństw nie
brakło czystych i godziwych, lecz było też wiele niedorzecznych i
wstrętnych. Tu i ówdzie odzywały się głosy protestujące, lecz zarówno
proroctwa Joachima de Florę, jak świętej Hildegardy, nie zdołały
powstrzymać złego. Łukasz Wadding, nabożny annalista
franciszkański, rozpoczyna dzieło swoje tym odstraszającym obrazem.
Rozwój badań historycznych pozwala nam na odtworzenie wielu
szczegółów, ale wniosek pozostaje ten sam: bez świętego Franciszka
Kościół byłby niezawodnie uległ zaćmieniu, a Katarowie byliby zostali
zwycięzcami. *Biedaczyna*, przepędzony przez służbę Inocentego\ III,
ocalił chrześcijaństwo.

Na tem miejscu nie możemy przedsiębrać studjów, wyczerpujących nad
stanem Kościoła w początkach trzynastego wieku; wystarczy nam
przedstawienie tej sprawy kilku rysami zasadniczemi.

Już pierwszy rzut oka na kler świecki zastrasza widokiem grasującego
świętokupstwa: kupczenie godnościami kościelnemi odbywało się z jawnem
zuchwalstwem; godności kościelne były wystawione jak gdyby na stałą
licytację i Inocenty\ III przyznał, że trzebaby żelaza i ognia, aby
zleczyć tę ranę. Jako zdumiewające wyjątki przedstawiano prałatów,
którzy nie pozwalali kupić się za *propinae* --- napiwki! O
urzędnikach kurji rzymskiej mawiano, że "są z kamienia, gdy chodzi o
zrozumienie czego, z drzewa, gdy idzie o wydanie sądu, z ognia, gdy
popadają w gniew, z żelaza, gdy mają przebaczyć; że są nadęci pychą,
jak buhaje, a chciwi i nienasyceni jak Minotaur"!

Pochwały, oddawane papieżowi Eugenjuszowi\ III za to, że odtrącił
księdza, który na początku pewnego procesu ofiarował mu sztukę złota,
mówią aż nadto o obyczajach Rzymu pod tym względem.

Biskupi znajdowali ze swej strony tysiące środków do wymuszania
pieniędzy od prostych księży. Gwałtowni, pieniaccy i wojowniczy, byli
osławieni śród ludu z jednego końca Europy po drugi.

Księża znowu starali się gromadzić beneficja, zapewniali sobie
spadkobranie i chwytali się najniecniejszych środków dla
zabezpieczenia bytu swym bękartom.

Zakony nie były wcale lepsze pod tym względem. Powstało ich bardzo
wiele w jedenastem i dwunastem stuleciu, lecz niebawem rozgłos
świątobliwości doprowadził do tak
hojnego ich obdarowywania, że następstwem tego stał się fatalny
upadek.

Niewiele kongregacyj zachowało ostrożność pierwszych mnichów Zakonu
Grammoteńskiego (diecezja Limoges): gdy mianowicie Etienne de Muret,
założyciel tego zakonu, zaczął ujawniać swą świętość, uzdrawiając
sparaliżowanego rycerza i przywracając wzrok pewnemu ślepcowi, uczni
jego przeraziła myśl o bogactwach i rozgłosie, które im skutkiem tego
groziły. Piotr z Limoges, stawszy się przeorem po zmarłym Murecie,
udał się do grobu swego poprzednika, wołając:

"O, sługo Boży, ty ukazałeś nam drogę ubóstwa i oto chcesz nas teraz
sprowadzić z prostej i uciążliwej ścieżki zbawienia na szeroką drogę
śmierci wiecznej. Zalecałeś nam samotność, a oto zamieniasz ją na
miejsce zbiegowisk i targowisko. Wierny przecie, że jesteś święty! Nie
potrzebujesz czynić cudów, któreby nas o tem przekonywały, lecz
jednocześnie zniszczyłyby naszą pokorę. Nie bądźże tak zawistnym o
swoją cześć, abyś ją powiększał kosztem naszego zbawienia! O to
ciebie prosimy i tego oczekujemy od twej miłości. Jeśli nie uczynisz
naszej prośbie zadość, tedy w imię posłuszeństwa, przyrzeczonego ci
ongi, oświadczamy, że wykopiemy twe kości i wrzucimy je do rzeki."

Etienne okazał się posłusznym aż do chwili kanonizacji swej (1189),
lecz od tej chwili żądza czci, chciwość i wszeteczeństwo tak owładnęły
pustelnią Grammontu, że z mnichów jej uczyniły przysłowie i
pośmiewisko świata chrześcijańskiego.

Nie bez powodu obawiał się Piotr z Limoges, że klasztor jego stanie
się targowiskiem; w cieniu prawie że wszystkich katedr członkowie
kapituł utrzymywali istne handle win, w pewnych zaś klasztorach nie
wahano się przyciągać spożywców przez wszelakich skoczków, a nawet
dziewki publiczne.

Aby wytworzyć sobie wyobrażenie o upadku większości zakonników,
trzeba czytać nie nawoływania kaznodziejów, częstokroć krasomówczo
przesadne, bo pragnące porywać i wzruszać, lecz przejrzeć zbiory buli,
śród których
wezwania na sąd do Rzymu za zabójstwa, gwałty, kazirodztwa,
cudzołóstwa, spotyka się niemal na każdej stronicy.

Staje się zrozumiałem, że nawet Inocenty\ III czuł się za słabym do
walki z takim nadmiarem zła i że ogarniało go zniechęcenie.

Najlepsze umysły zwracały się ku Wschodowi, wyczekując, czy Kościół
grecki nie ruszy się nagle, aby wytępić zło i zagarnąć dziedzictwo
swej siostrzycy.

Kleru nie poważano już zgoła, lecz ulegano mu przez zabobonną trwogę
wobec jego potęgi. Tu i owdzie dawały się dostrzegać oznaki,
zapowiadające straszliwe bunty; drogi, wiodące do Rzymu, roiły się od
mnichów, śpieszących wzywać opieki świętej Stolicy przeciw ludności,
śród której mieszkali. Papież wydawał zwykle interdykt, ale trudno
było przecież powracać doń bezustannie.

Aby utrzymać swoje przywileje, papiestwo było częstokroć zmuszone
otaczać opieką takich, którzy byli jej najmniej godni. Klienci
papiescy niezawsze bywali tak interesującemi osobami, jak
nieszczęsna Ingelburga[^ingelburga]. Łatwiejby było podziwiać bez zastrzeżeń
zachowanie się Inocentego\ III, gdyby się miało przekonanie, że
papieżowi chodziło wyłącznie o obronę sprawy biednej porzuconej
kobiety. Lecz wszystko wskazuje aż nadto wyraźnie na to, że
nadewszystko chodziło mu o podtrzymanie przywilejów kościelnych. Widać
to najlepiej na jego interwencji na korzyść Waldemara, biskupa
Szlezwiku.

[^ingelburga]: Ingelburga, królowa Francji, żona Filipa\ II Augusta. Gdy mąż jej
    odtrącił ją, aby pojąć, za żonę Agnieszkę Merańską, papież Inocenty\ III
    wziął Ingelburgę w opiekę przeciw Filipowi\ II. --- (Przyp. tłum.)

Nie trzeba przecież sądzić, że w łonie Kościoła wszystko było
zepsute; lecz, jak zwykle, zło czyniło więcej hałasu, niż dobro, i
głos tych, którzy domagali się reformy, osiągnął tyle tylko, że
wywoływał ruchy przemijające.

Wśród ludu panowała zabobonność nieprawdopodobna: wygłaszanie kazań,
które mogłyby były szerzyć
niejakie światło, było przywilejem biskupów, lecz nieliczni pasterze,
którzy nie zapominali o swojej pod tym względem powinności, mogli
byli uczynić niewiele wobec mnogości swych obowiązków. Dopiero
powstanie zakonów żebrzących zmusiło cały kler świecki do stałego
wygłaszania kazań.

Kult, sprowadzający się do ceremonij liturgicznych, nie zachował nic z
tego, coby mogło przemawiać do umysłu; to też obrzędy stawały się
powoli czemś w rodzaju formuł magicznych, działających automatycznie.
Znalazłszy się raz na tej drodze, dochodziło się niebawem do
absurdu. Ludzie, uważający się za pobożnych, opowiadali o cudach,
zdziałanych przez relikwje bez czyjegokolwiek moralnego aktu wiary.

Oto np.\ papuga, porwana przez kanię, zaczęła wykrzykiwać wezwanie,
jakie częstokroć powtarzała jej pani:
<span lang="it">"Sancte Thoma, adjuva me"</span>
i została cudownie uratowana.
Gdzie indziej znowu pewien kupiec z
Groningen ukradł ramię świętego Jana Chrzciciela i bogacił się jak na
zawołanie, dopóki ramię to przechowywał ukryte u siebie, lecz zeszedł
na dziady, gdy kradzież jego wykryto, rewikwję mu odebrano i
umieszczono ją w kościele.

Trzeba atoli zaznaczyć, że opowieści takie nie pochodzą od pobożnych
prostaczków, mieszkających kędyś po zapomnianych wioskach; przekazał
je nam jeden z najświatlejszych mnichów owego czasu, który opowiadał
je nowicjuszowi w celu oświecenia jego umysłu.

Relikwje działały tedy niby jakieś talizmany. Nietylko że czyniły
cuda, choć doznający ich nie znajdował się w osobliwym stanie wiary,
lub pobożności, lecz najpotężniejsze z pośród nich uzdrawiały chorych
wbrew ich woli. Kronikarz pewien opowiada, że ciało świętego Marcina z
Tours zostało potajemnie wywiezione gdzieś daleko w r.\ 887, a to z
obawy przed najazdem duńskim. Gdy nadszedł czas odwiezienia tej
relikwji, znadowali się w Turencji dwaj ułomni, którzy dzięki swemu
kalectwu otrzymywali dużo pieniędzy. Dowiedziawszy się, że relikwia
zostanie przywieziona
zpowrotem, zatrwożyli się bardzo: święty Marcin uzdrowi ich niechybnie
i odbierze im ich pewny zarobek. Obawy ich były aż nadto uzasadnione.
Zaczęli uciekać, lecz z powodu kalectwa nie mogli biec dość szybko i
zanim przekroczyli granicę Turenji, święte ciało nadeszło i uzdrowiło
ich!

Opowiadań takich możnaby przytoczyć setki, podawać statystyki, które
wykazałyby, że w chwili wstąpienia na tron Inocentego\ III większość
stolić biskupich zajęta była przez ludzi niegodnych, klasztory były
zaludnione mnichami leniwymi i rozwięzłymi, lecz --- byłbyż to
dokładny obraz Kościoła owej epoki?

Nie sądzę. Przedewszystkiem trzeba wziąć pod uwagę dusze wybrane,
liczniejsze bezwątpienia, niżby się przypuszczało. Pięciu
sprawiedliwych mogło było uratować ongi Sodomę, lecz Bóg ich tam nie
znalazł. Byłby może znalazł, szukając sam i nie powierzając szukania
Lotowi. Kościół XIII stulecia miał swych sprawiedliwych i dlatego
burza herezji nie zgotowała mu zagłady.

Lecz jest jeszcze inna sprawa; Kościół przedstawiał wówczas piękny
widok wielkości moralnej. Trzeba odwrócić oczy od nędz dopiero co
opisanych i skierować je na tron papieski, aby ujrzeć piękno walki
rozpoczętej: władza czysto duchowna sięga po rządy nad królami
ziemskimi, aby panować nad nimi, jak dusza panuje nad ciałem, i rządy
te zdobywa.

Niezawodnie, żołnierze i generałowie tej armji byli częstokroć
prawdziwymi bandytami, lecz aby wydać sąd sprawiedliwy, trzeba poznać
cel, do którego zmierzali.

W owych czasach zupełnej bezwzględności, kiedy to siła brutalna była
wszystkiem, Kościół, pomimo swych ran, ukazywał światu chłopów i
robotników, odbierających korne hołdy od najwyższych władców ziemi,
dlatego jedynie, że zasiadali oni na stolicy Świętego Piotra i byli
przedstawicielami prawa moralnego.

Oto dlaczego Alighieri i tylu innych przed nim i po nim, przeklinając
złych ministrów, dochowują jednakże w głębi serca ogrom szczerego
uczucia i gorącą miłość dla
tego Kościoła, którego nie przestawali nazywać swoją matką.

Nie wszyscy jednak naśladowali ich, a występki kleru tłumaczą nam
nieskończoną ilość odszczepieństw. Wszystkie one miały powodzenie,
poczynając od tych, które były prostym krzykiem wzburzonego sumienia,
jak na przykład sekta Waldensów, a kończąc na najbardziej szalonych,
jak sekta Eonu Gwiazdy. Śród wielkiej liczby sekt nie brakło pięknych
i świętych spraw, ale prześladowania heretyków nie powinny zaciemniać
nam sądu o nich. Byłoby oczywiście lepiej dla Rzymu, gdyby był
triumfował łagodnością, nauką, świętością, lecz niestety, walczący
niezawsze szuka broni właściwej, a gdy idzie o życie, chwyta za broń
pierwszą z brzegu. Papiestwo stawało nietylko po stronie zacofania i
ciemnoty; na przykład, gdy powaliło Katarów, to zwycięstwo jego było
zwycięstwem rozsądku i rozumu.

Spis odszczepieństw XIII stulecia jest już dość długi, wydłuża się on
jednakże niemal z dnia na dzień ku wielkiej radości erudytów, którzy
podejmują wiele trudów, aby uporządkować ten chaos mistycyzmu i
szaleństwa. Herezje w owym czasie były bardzo żywotne, z konieczności
mocno powikłane i obdarzone wyrazistością zadziwiającą. Można śród
nich oznaczać prądy, wskazywać kierunki; iść jeszcze dalej znaczyłoby
to zagubić się w tym wirze odruchów, namiętności, dziwactw, rodzących
się, rozwijających, upadających, wedle kaprysu tysiącznych
okoliczności nieuchwytnych. Jeszcze dziś w pewnych hrabstwach Anglji
istnieją wsi, które na kilka setek mieszkańców miewają po osiem lub
dziesięć kościółków. Większość tych ludzi zmienia sektę co trzy lub
cztery lata, powraca do opuszczonej, aby ją znowu rzucić i znowu do
niej powrócić, i to tak w ciągu całego życia. Wodzowie tych sekt
świecą przykładem, śpiesząc za każdą nowinką, którą zarzucają
niebawem. Jedni i drudzy znaleźliby się w wielkim kłopocie, gdyby
mieli przedstawić rozumne powody takiego postępowania. Powiadają, że
kieruje ich krokami duch, a byłoby niegrzecznie nie wierzyć ich
słowom. Lecz historyk, który chciałby zbadać to wszystko, straciłby
głowę, o ile nie zrobiłby
specjalnej szufladki dla każdego z tych Proteuszów. Zresztą nie byłoby
to warte zachodu.

Stan ten przypomina trochę położenie wielkiej części świata
chrześcijańskiego za Inocentego\ III, lecz podczas gdy sekty, o których
dopiero co mówiłem, kręcą się w dość ciasnem kółku dogmatów i
pomysłów, w XIII stuleciu wszystkie wybryki zbiegają się z sobą i
wypływają jedne z drugich. Przerzucano się bezpośrednio z jednego
krańca na drugi.

W całym tym chaosie dostrzegamy jednak dwa lub trzy rysy zasadnicze:
przedewszystkiem herezje te nie są już, jak dawniej, oparte na
subtelnościach metafizycznych; Arjusz i Pryscyljan, Nestorjusz i
Eutyches pomarli i nie wskrzesną.

Po wtóre źródłem ich nie są klasy wyższe i kierownicze, lecz
przedewszystkiem niższy kler i lud. Ciosy groźne dla Kościoła wieków
średnich, zadawane były przez ciemnych robotników, ubogich i
uciśnionych, którzy w nędzy swej i poniżeniu czuli, że Kościół minął
się ze swojem posłannictwem.

Gdy tylko ozwał się głos, wzywający do wyrzeczeń i prostoty, wnetże
odpowiadali nań nietylko laicy, lecz i członkowie kleru. Tak na
przykład niejaki Pons poruszył ku końcowi XII stulecia cały Perigord,
głosząc jeszcze przed świętym Franciszkiem ubóstwo ewangeliczne.

Ujawniają się dwa wielkie prądy: z jednej strony Katarowie, z drugiej
niezliczone sekty, które przez wierność dla chrześcijaństwa buntują
się przeciw Kościołowi i pragną zawrócić ku chrześcijaństwu
pierwotnemu.

Śród sekt tej drugiej kategorji, koniec XII stulecia zrodził we
Włoszech sektę *Biedaków*, która niezawodnie wiązała się z usiłowaniami
Arnolda z Brescji i zaprzeczała skuteczności sakramentów,
sprawowanych rękoma niegodnemi.

Próba rzeczywistej reformy podjęta była przez Waldensów; dzieje ich,
aczkolwiek lepiej znane, pozostają dość ciemnemi co do pewnych
szczegółów; imię ich, *Ubodzy z Lijonu*, przypomina Biedaków, z
którymi łączyły ich
bardzo ścisłe stosunki, podobnie jak z *Pokornymi*. Wszystkie te imiona
przypominają mimowoli nazwę, którą Franciszek da swemu Zakonowi.
Podobieństwa między pobudkami Piotra Valdo i świętego Franciszka są
tak liczne, iż możnaby wprost przypuszczać, że chodzi tu o przypadek
naśladownictwa. Przypuszczenie takie byłoby błędnem: te same przyczyny
wywołują zawsze te same skutki; pomysły reformy, powrotu do ubóstwa
ewangelicznego, unosiły się w powietrzu i to ułatwia nam zrozumienie,
dlaczego kazanie Franciszkowe w przeciągu niewielu lat mogło było
zbudzić tak potężne echo w całym świecie. Jeśli pierwsze wystąpienia
obu tych mężów były podobne do siebie, dalszy ciąg ich życia różnił
się bardzo: Valdo, popadłszy w odszczepieństwo niema wbrew woli,
musiał wysnuć wnioski ze swoich założeń, podczas gdy Franciszek,
pozostawszy uległym synem Kościoła, wszystkie wysiłki skierował ku
rozwinięciu w sobie i swych uczniach potęgi serca.

Zresztą jest bardzo prawdopodobnem, że przez ojca swego Franciszek
wiedział o wszystkich usiłowaniach *Ubogich z Lijonu*. Stąd częste jego
rady uległości, zalecanej braciom względem kleru.

Jasnem jest, że gdy udał się do Inocentego\ III z prośbą o aprobatę,
to prałaci, z którymi miał do czynienia, uprzedzając go o
niebezpieczeństwach, złączonych z jego dziełem, wskazywali mu właśnie
Piotra Valdo, jako przykład odstraszający.

Valdo przybył do Rzymu w roku 1179 razem z kilku towarzyszami, aby
żądać jednocześnie aprobaty dla tłumaczenia Pisma świętego na język
ludowy i zezwolenia na wygłaszanie kazań. Zostali wysłuchani z
warunkiem, aby na kazania wyjednywali zezwolenia proboszczów. Gautier
Map (zm.\ 1210), który ich przesłuchiwał z urzędu, chociaż podrwiwał z
ich prostoty, musiał jednakże podziwiać ich ubóstwo i gorliwość w
krzewieniu zasad życia apostolskiego.

We dwa lub trzy lata później nie spotkali się już z podobnem w
Rzymie przyjęciem, a w roku 1184 zostali wyklęci przez sobór w
Weronie. Od tej chwili nic już nie mogło
ich powstrzymać na drodze, wiodącej aż do utworzenia kościoła nowego.

Mnożyli się z szybkością, która bodajże nie została prześcignięta
przez franciszkanów. Od końca XII stulecia są oni rozsiani od Węgier
aż do serca Hiszpanji, gdzie właśnie zaczęto ich tępić. Gdzie indziej
poprzestawano zrazu na unikaniu ich jako wyklętych.

Zmuszeni do ukrywania się i pozbawieni możności zwoływania swych
kapituł, które miały zbierać się raz, lub dwa razy do roku, a które
jedynie utrzymać mogły pewną jedność doktryny, Waldensi przeobrażali
się szybko zależnie od środowiska: jedni z uporem uważali się za
dobrych katolików, inni posunęli się aż do głoszenia zasady, że
hierarchię należy obalić i że sakramenty są zbędne.

Stąd mnogość szybko powstających odłamów, zgoła przeciwnych, a nawet
wrogich sobie.

Tymczasem wspólne prześladowania zbliżyły ich z Katarami i sprzyjały
pomieszaniu się ich pojęć. Ruchliwość ich była wprost zdumiewająca.
Pod pozorem pielgrzymek do Rzymu wędrowali ci ludzie prości i wnikliwi
z miejsca na miejsce, a sposób podróżowania ówczesnego ułatwiał
wszelką propagandę. Opowiadając nowiny tym, z których gościny
korzystali, mówili im o smutnym stanie Kościoła i konieczności
reformy. Te rozmowy były dla apostołowania środkiem daleko
skuteczniejszym, niż propaganda dzisiejsza przy pomocy książek i
dzienników, bo w krzewieniu nowych myśli nic nie zastąpi żywego
słowa.

O Waldensach często rozpowszechniano bardzo brzydkie opowieści;
oszczerstwo jest bronią zbyt dostępną, aby nie miało kusić
przeciwników, staczających z sobą ciężkie walki. Przypisywano im
hańbiącą wspólnotę żon, o co ongi oskarżano pierwszych chrześcijan. W
rzeczywistości siłą Waldensów były ich cnoty, żywo kontrastujące z
występkami kleru.

Najpotężniejszymi i najbardziej nieprzejednanymi wrogami Kościoła
byli Katarowie. Otwarci, zuchwali, częstokroć oświeceni i wojowniczy,
mający śród zwolenników swoich serca dzielne i umysły potężne, byli
Katarowie
w wieku trzynastym heretykami w całej wieloznaczności słowa. Bunt ich
nie był, jak u pierwszych Waldensów, buntem przeciwko poszczególnym
punktom nauki lub zasadom karności; ich doktryna podstawowa była
ustalonem zaprzeczeniem dogmatu katolickiego wogóle.

Pomimo, że herezja Katarów rozpowszechniona była po całych Włoszech i
kwitła nawet przed oczyma świętego Franciszka, ograniczam się do tej
krótkiej o niej wzmianki. Jeśli dzieło świętego Franciszka wchłonęło w
siebie bardzo wiele z ruchu Waldensów, to kataryzm pozostał mu
zupełnie obcym.

Tłumaczy się to w sposób naturalny faktem, że Franciszek nigdy nie
chciał zajmować się doktryną. Dla niego wiara należy nie do dziedziny
umysłowej, ale do dziedziny moralnej i ma być uświęceniem serca. Czas
spędzony na dogmatyzowaniu wydawał mu się straconym.

Pewien rys z życia brata Egidjusza poucza nas, jak lekce pierwsi
bracia Mniejsi ważyli sobie teologję. Dnia pewnego wobec świętego
Bonawentury brat ten, może nie bez ironji, zawołał: "Ach, cóż uczynić
mamy my nieuczeni prostacy, aby zasłużyć na dobroć Boga?" --- "Wiesz,
mój bracie --- odparł sławny doktór, --- że dość jest kochać Pana".
--- "Jesteś tego zupełnie pewien --- pytał Egidjusz, --- sądzisz, że
prosta kobiecina może podobać się Bogu tak samo, jak mistrz
teologji?" --- Otrzymawszy odpowiedź twierdzącą, Egidjusz wybiegł i
przywoławszy starą żebraczkę, pouczał ją głosem donośnym: "Raduj
się, biedna starowino, bowiem jeśli kochasz Boga, możesz w królestwie
niebieskiem stać wyżej od brata Bonawentury."

Katarowie nie wywarli tedy wpływu bezpośredniego na świętego
Franciszka, ale nic nie mówi tak wyraziście o rozprzężeniu myśli w
owym czasie, jak ten wskrzesły manicheizm.

Do jakiegoż wyjałowienia i ogłupienia dojść musiała religijna myśl
włoska, aby ten zlepek myśli buddyjskiej, mazdejskiej i gnostycznej
mógł był nad nią zapanować?!

Doktryna katarowska polegała na przeciwieństwie dwóch zasad: złej i
dobrej. Pierwsza stworzyła materję, druga
dusze, które przechodzą z pokolenia w pokolenie, z jednego ciała w
drugie, aby dojść zbawienia. Materja jest przyczyną i siedliskiem
zła; wszelkie zetknięcie się z nią kala; w konsekwencji Katarowie
wyrzekli się małżeństwa i własności i zalecali samobójstwo. Wszystko
to było pomieszane z mitami kosmogonicznemi, bardzo powikłanemi.

Zwolennicy ich dzielili się na 2 kategorje: czystych, czyli
doskonałych i wierzących, którzy byli prozelitami drugiego stopnia i
których obowiązki były bardzo proste. Adepci właściwi byli
wtajemniczani przez ceremonję t.\ zw.\ *consolamentum*, czyli
wkładanie rąk, co miało sprowadzać na nich Ducha Pocieszyciela. Byli
śród nich tacy zapaleńcy, że po ceremonji zagładzali się na śmierć,
aby nie wyjść z tego stanu łaski.

W Langwedocji, gdzie zwykle nazywano ich Albigensami, posiadali oni
organizację, która obejmowała całą Europę środkową, utrzymując na
wielu miejscach kwitnące szkoły, uczęszczane przez dzieci szlachty.

We Włoszech byli niemniej potężni: w Concorrezo, niedaleko Monzy w
Lombardji i w Bagnolo posiadali dwie kongregacje, różniące się nieco
od kongregacji langwedockiej.

Przedewszystkiem zaś Medjolan był ośrodkiem, z którego
rozprzestrzeniali się po całym półwyspie Włoskim, jednając prozelitów
nawet w najbardziej oddalonych okręgach Kalabrji.

Stan anarchji, w jakim kraj się znajdował, sprzyjał temu ruchowi.
Papiestwo zbyt było zajęte niweczeniem konwulsyjnych wysiłków
Hohenstaufów, aby w walce z herezją mogło było postępować z należytą
wytrwałością i konsekwencją. To też nowinki heretyckie docierały aż do
bazyliki Lateraneńskiej: gdy w roku 1209 Otton IV zjechał na
koronację do Rzymu, znalazł tam szkołę, w której manicheizm wykładano
publicznie.

Pomimo swej energji, Inocenty\ III nie zdołał opanować zła,
krzewiącego się w państwie kościelnem. Przykład Viterbo mówi aż nadto
o trudnościach tłumienia odszczepieństwa:
w marcu roku 1199 papież pisał do kleru i ludu tego miasta,
przypominając i zaostrzając kary, wymierzone przeciw herezji. Pomimo
to Patarinowie[^patarinowie] zdobyli większość w roku 1205 i mogli byli wybrać
na konsula jednego z pośród swoich.

[^patarinowie]: Patarinami --- gałganiarzami nazywano Katarów po całych Włoszech,
    prawdopodobnie dlatego, że w głównem swem siedlisku, w Mediolanie,
    gnieździli się w dzielnicy, zamieszkanej przeważnie przez handlarzy
    starzyzną. --- (Przyp. tłum.)

Gniew papieża nie miał wówczas granic; rzucił przeciw miastu
piorunującą bullę, w której zagroził mu zagładą i nakazał miastom
okolicznym rzucić się na nie, jeśli w przeciągu dni piętnastu nie da
zadośćuczynienia.

Nie zdało się to na nic; Patarinowie byli niepokojeni tylko pozornie i
trzeba było obecności samegoż papieża, aby rozkazy jego zostały
wypełnione, a domy heretyków i ich wspomagaczy zburzone (jesienią roku
 1207).

Zgnieciony na jednem miejscu, bunt podnosił łeb na stu innych; w owym
czasie herezja triumfowała wszędzie: w Ferrarze, Weronie, Rimini,
Florencji, Prato, Faenzy, Treviso, Placencji. To ostatnie miasto
wygnało kler i pozostawało przez trzy lata bez księży.

Viterbo jest oddalone od Asyżu o jakie 80 kilometrów, Orvieto tylko o
40, a zamęt był w obu równie poważny. Pewien szlachcic rzymski,
Pietro Parentio, sprawujący tam rządy z ramienia papieża, postanowił
wytępić Patarinów. Został zamordowany.

Lecz Franciszek nie potrzebował udawać się tak daleko, aby widzieć
heretyków. W Asyżu było tak samo, jak w miastach sąsiednich. W roku
1203 wybrało to miasto na podestę heretyka imieniem Giraldo di
Gilberto i pomimo napomnień z Rzymu uparło się zatrzymać go na czele
miasta aż do wygaśnięcia jego mandatu (1204).

Inocenty\ III, który wówczas nie miał był jeszcze do czynienia z
wyzywającym uporem miasta Viterbo, użył perswazji i wysłał do Umbrji
kardynała Leona de Sainte-Croix, z którym spotkamy się jeszcze
niejednokrotnie w tej pracy.

Następcy Giralda i pięćdziesięciu przedniejszych obywateli złożyli
zobowiązanie honorowe w jego ręce i przysięgli wierność Kościołowi.

Widać z tego, w jakim stanie wrzenia znajdował się półwysep Włoski w
tych pierwszych latach XIII stulecia. Moralne wykolejenie kleru
musiało snać być wielkie, skoro umysły z takim zapałem zwróciły się ku
manicheizmowi.

Włochy mogą być wdzięczne św.\ Franciszkowi: były one zarażone
kataryzmem niegorzej od Langwedocji, a oczyszczenia dokonał
Franciszek. Nie tracił czasu na wykazywanie nicości doktryny
katarowskiej przy pomocy syllogizmów i tez teologicznych, lecz
wzniósłszy się od jednego rozmachu na wyżyny życia religijnego, olśnił
oczy współczesnych ideałem nowego życia, wobec którego zczezły
wszystkie te sekty dziwaczne, jak ptaki nocne, spłoszone pierwszym
promieniem słońca.

Część swej mocy zawdzięczał święty Franciszek temu, że zasadniczo
unikał polemiki, która mniej lub więcej zawsze jest pewną postacią
pychy duchowej, pogłębiającej jeno przepaść, którą mniema zasypywać.
Prawda nie potrzebuje obrony, gdyż narzuca się duszom sama. Jedyną
bronią, której używać chciał przeciw złej woli, była świątobliwość
życia, tak dalece wypełnionego miłością, aby tych, którzy go otaczali,
mogło oświecać, ogrzewać i zniewalać do pokochania.

Zniknięcie kataryzmu we Włoszech bez wstrząśnień, a przedewszystkiem
bez inkwizycji, jest więc wynikiem pośrednim ruchu franciszkańskiego
i to wynikiem nie najmniejszym. Na głos reformatora umbryjskiego
Włochy opamiętały się i odzyskały swój zdrowy rozum i pogodny humor;
wyrzuciły z myśli swojej pesymizm i śmierć, jak krzepki organizm
wyzbywa się zarazków chorobotwórczych.

Zaznaczyłem już, jak dalece inicjatywa Franciszka podobną jest do
inicjatywy Ubogich z Lijonu. Myśl jego dojrzewała w środowisku
przesyconem ich myślami, które mogły były przeniknąć go bez jego
 wiedzy. Proroctwa Opata
kalabryjskiego wywarły nań wpływ równie głęboki, jak trudny do
określenia.

Żyjąc na krańcu Włoch i jak gdyby na progu Grecji, Joachim de Flore
tworzy ostatnie ogniwo szeregu mnichów-proroków, którzy w ocienionych
laurami pustelniach na południu półwyspu następowali po sobie w ciągu
niemal czterech set lat. Najsławniejszym z nich był święty Nil,
odludek przypominający Jana Chrzciciela, żyjący w pustyni, ale
opuszczający ją nagle, gdy obowiązki sędziowskie powoływały go gdzie
indziej. Pewnego dnia ujrzano go na ulicach Rzymu, gdzie papieżowi i
cesarzowi zapowiadał rozpętanie gniewu Bożego.

Rozproszeni śród samotni alpejskich Bazylikaty, zmuszeni byli ci
pustelnicy wspinać się coraz wyżej, aby uciec przed nadciągającemi
gromadami mieszkańców, którzy niepokojeni przez piratów, osiadali w
górach. Żywot pędzili między niebem a ziemią, mając na widnokręgu dwa
morza. Niepokojeni obawą przed korsarzami i zgiełkiem wojen, których
echo dobiegało aż do nich, zwracali się ku przyszłości. Czasy
wielkich trwóg są czasami wielkich nadziei; niewoli babilońskiej
zawdzięczamy wraz z kontynuacją Izajasza, obrazy, któremi duch
człowieczy nie przestaje się zachwycać; prześladowania Nerona
obdarzyły nas Objawieniem świętego Jana, a burze XII-go stulecia
Ewangelją Wieczną.

Nawrócony po życiu pełnem uciech, Joachim de Flore podróżował długo po
Ziemi Świętej, po Grecji, i bawił w Konstantynopolu. Powróciwszy do
Włoch, zaczął, acz laik, kazać w okolicach miast Rende i Cosenzy.
Później wstąpił do Cystersów w Cortale, około Catanzaro, i złożył tam
śluby zakonne. Wybrany niebawem, wbrew swym odmowom i pomimo
ucieczki, na opata klasztoru, po kilku latach zatęsknił za samotnością
i udał się do papieża Lucjusza\ III, aby uwolnił go od obowiązków
(1181) i pozwolił mu poświęcić życie pracom, nad któremi rozmyślał.
Papież wysłuchał jego prośby i nawet pozwolił mu udać się gdzie
będzie mu się podobało, dla dokonania zamierzonych prac. Wówczas
zaczęta się dla Joachima tułaczka z klasztoru do klasztoru, która
przywiodła go aż do Werony w Lombardji, gdzie znalazł się z pobliżu
papieża Urbana\ III.

Gdy powrócił na południe, otoczyli go uczniowie, pragnący słuchać
jego objaśnień najbardziej niezrozumiałych ustępów Biblji. Chcąc nie
chcąc, musiał ich przyjąć, rozmawiać z nimi, ułożyć dla nich regułę i
wreszcie osiedlić ich w samem sercu gór Sila, tego Czarnego Lasu
Italji, naprzeciwko najwyższego szczytu, w przesmykach, których
milczenie przerywa tylko szmer rzek Arvo i Neto, biorących swój
początek niedaleko stamtąd. Nowy Athos otrzymał nazwę Fiore (kwiat),
symbolizując przejrzyście nadzieje założyciela.

Tam wykończył ostatecznie pisma, które po pięćdziesięciu latach
zapomnienia miały się były stać źródliskiem wszystkich herezyj i
strawą wszystkich dusz, zaniepokojojonych o zbawienie chrześcijaństwa.

Ludzie pierwszej połowy trzynastego stulecia, zbyt zajęci czem innem,
nie spostrzegli zrazu, że źródło duchowne, z którego pili, spływało
ze śnieżnych szczytów Kalabrji.

Z wpływami mistycznemi bywa tak zawsze; mają one w sobie coś
wnikliwego, co wymyka się ocenie dokładnej. Po spotkaniu się z sobą
dwóch dusz wybranych, byłyby one w wielkim kłopocie, gdyby miały
zanalizować i wyrazić wrażenia, jakich wzajemnie doznały, obcując z
sobą. Tak samo jest z epokami; niezawsze rozumieją one najlepiej tych,
którzy przemawiają do nich najczęściej i najgłośniej; ani nawet tych,
u których stóp niby uczennice wierne siadają dzień w dzień.
Częstokroć, słuchając swoich zwykłych mistrzów, spotykają one nagle
mistrza nieznanego; zrozumieją zaledwie kilka słów z tego, co
powiedział, nie wiedzą skąd wychodzi i dokąd podąża, a już gubią go z
oczu. Lecz kilka tych słów przenika je do głębi, jak coś swojego,
miesza je i niepokoi.

Tak było z Joachimem de Flore. Myśli jego rozsiane po świecie przez
jego rozentuzjazmowanych uczni, cicho kiełkowały
w sercach. Przywracając ludziom nadzieję, wracał im siłę. Myśleć
znaczy już tyle, co działać; w cieniu stuletnich jodeł, które
otaczały jego celę, samotny pustelnik z Fiore pracował nad odnowieniem
Kościoła z taką żarliwością, jak reformatorzy, którzy przyszli po
nim.

Dalekim jest wszelako od wyżyn, proroków izraelskich; zamiast, jak
oni, rzucić się w głąb nieba, trzyma się uparcie tekstów, które
objaśnia metodą alegoryczną i z których dzięki takiemu objaśnieniu
wydobywa dziwactwa nie do uwierzenia. Kilka stronic tej lektury
zamęcza czytelnika najcierpliwszego, lecz na tej roli, spalonej przez
dociekania teologiczne, wysuszającej bardziej, niż wiatr pustyni,
choć na pierwsze spojrzenie nie widać nic prócz kamieni i ostów,
spostrzega się wreszcie czarującą oazę, a w jej cieniu znajduje się
wypoczynek i marzenia.

Egzegeza Joachima de Flore jest czemś w rodzaju filozofji historji,
której śmiałe rysy pobudzać musiały wyobraźnię. Zgoła osobliwie życie
ludzkości dzieli się na trzy okresy: w pierwszym, któremu panował
Ojciec, ludzkość żyła pod rygorem Zakonu; w drugim, nad którym panował
Syn, żyła pod władzą Łaski; nad trzecim zapanuje Duch i ludzkość żyć
będzie pełnią Miłości. Pierwszy okres jest okresem posłuszeństwa
niewolniczego, drugi --- okresem posłuszeństwa synowskiego, trzeci ---
okresem Wolności. W pierwszym nad życiem panowała trwoga; w drugim
życie spoczywa na wierze; w trzecim gorzeć będzie miłością.
Pierwszemu przyświecały gwiazdy, drugi spogląda na rumieniącą się
zorzę, trzeci ujrzy jasność dnia. Pierwszy rodził pokrzywy, drugi
darzy różami, trzeci będzie wiekiem lilji.

Gdy wyobrazimy sobie, że w myśli Joachima trzeci okres, wiek Ducha,
nadchodził właśnie, to zrozumiemy wesele, z jakiem witano słowa,
przywracające radość duszom wzburzonym jeszcze obawami
milenarycznemi.[^milenaryzm]

[^milenaryzm]: Milenaryzm, po grecku chiliazm od słowa *chilloi* --- tysiąc.
    Nadzieje pierwszych i późniejszych chrześcijan, wyczekujących
    tysiącletniego królowania Bożego, wyraża nazwa powyższa. Około roku
    1000 po Chrystusie powszechną była obawa, że rok 1000-ny będzie końcem
    świata. --- (Przyp. tłum.)

Święty Franciszek znał bezwątpienia te promienne nadzieje. Kto wie
nawet, czy to nie Wieszcz kalabryjski upoił serce jego miłością. Gdyby
tak było, to byłby on nietylko poprzednikiem, lecz ojcem duchownym
Franciszka.

Tak czy owak, Franciszek znalazł w tej myśli Joachima wiele
pierwiastków, które, oczywiście bezwiednie, stać się miały podwaliną
jego dzieła.

Szlachetna wzgarda dla wszelkiej wiedzy, którą ujawniał, a którą
pragnął wpoić swemu Zakonowi, była dla Joachima jednym z rysów,
charakteryzujących nową erę.

"Prawda, --- powiada Joachim, --- która jest ukrytą dla uczonych,
objawia się dzieciom; dialektyka zamyka to, co było otwartem,
zaciemnia to, co było jasnem; jest ona matką rozpraw nieużytecznych,
współzawodnictwa i bluźnierstwa. Wiedza nie buduje, ale umie burzyć,
jak to widać na owych pisarzach kościelnych, nadętych pychą i
zarozumiałością, którzy siłą swego rozumowania wpadają w
odszczepieństwo."

Widziano, że powrót ku prostocie ewangelicznej narzuca się duszom;
wszystkie odnoża herezji zgodne były pod tym względem z pobożnymi
katolikami, lecz nikt nie mówił o tem w sposób tak franciszkański, jak
Joachim de Flore. Z dobrowolnego ubóstwa czynił nietylko jeden z
rysów, charakteryzujących wiek lilji, lecz na kartkach swoich pism
mówi o niem ze wzruszeniem tak silnem, tak głęboko przeżytem, że
święty Franciszek nie mógł tego nie powtórzyć. Mnich idealny, którego
nam opisuje, a który za całe bogactwo posiada tylko lutnię, czyż to
nie franciszkanin przed swojem pojawieniem się, czyż to nie marzenie
*Biedaczyny* z Asyżu?

Odczucie przyrody wybucha w nim także z siłą niezrównaną. Dnia
pewnego kazał w kaplicy, pogrążonej w ciemnościach niemal zupełnych,
bo niebo zawalone było chmurami. Nagle jasność się czyni, słońce rzuca
snopy promieni, kościół przepełnia się blaskiem; Joachim przerywa
kazanie, pozdrawia słońce, intonuje *Veni Creator*
i wyprowadza swoich słuchaczy, aby podziwiali piękno rozsłonecznionych
błoni.

Nie byłoby w tem nic dziwnego, gdyby Franciszek około roku 1205
słyszał był o tym proroku, ku któremu zwracały się już dusze
niektóre, o tym pustelniku, który spoglądając w niebo, rozmawiał z
Jezusem, jak przyjaciel rozmawia z przyjacielem, lecz który umiał
schodzić na ludzki padół, aby pocieszać strapionych, a stygnące
policzki konających ogrzewać na własnej piersi.

Na drugim końcu Europy, w sercu Niemiec, te same przyczyny wywoływały
takie same skutki; nadmiar cierpienia ludu i beznadziejność dusz
pobożnych zrodziła mistycyzm apokaliptyczny, który zdaje się mieć
tajemną łączność z ruchem, jaki nurtował półwysep Italski.

Takie same wizje przyszłości, takie same niepokojące oczekiwania
nowych kataklizmów łączą się tu z widokami odmłodnienia Kościoła.

"Wołaj głosem potężnym --- powiada do świętej Elżbiety z Schönau (zm.\ 1164)
jej anioł opiekuńczy --- aby słyszały wszystkie narody: Biada!
Bowiem cały świat pogrążył się w ciemnościach. Winnica Pańska
zniszczona, niema nikogo, ktoby ją uprawiał! Wysłał Pan do niej
robotników, ale okazali się gnuśnymi. Głowa Kościoła jest chora, a
członki martwe... Pasterze Kościoła mego, wy śpicie, ale ja was
przebudzę! Królowie ziemi, krzyk nieprawości waszej doszedł aż do
mnie."

"Sprawiedliwość Boża --- powiada święta Hildegarda (zm.\ 1178) --- znowu
nastanie; ostatnia z siedmiu epok, symbolizowanych przez siedem dni
stworzenia, nadeszła, sądy Boże dokonają się; cesarstwo i papiestwo,
podupadłe w bezbożności, runą razem... Lecz wtedy na ruinach ukaże
się nowy lud Boży, lud proroków oświeconych zwyż, żyjący w ubóstwie i
samotności. Objawią się wówczas tajemnice Boże i wypełni się słowo
proroka Joela: Duch święty spuści na lud rosę proroctwa, mądrości i
świętości; poganie, żydzi, dzieci świata tego i niewierni tłumnie
nawracać się
będą; wiosna i pokój panować będą na ziemi odrodzonej, anioły ufnie
powrócą, aby mieszkać śród ludzi."

Nadzieje te okazały się niezupełnie zawodnemi. Pod wieczór życia
prorok de Flore mógł był zaintonować swoje *Nunc dimittis*[^nunc-dimittis], a
chrześcijaństwo mogło było przez lat kilka zwracać się, zdumione, ku
Asyżowi, niby ku nowemu Betlejem.

[^nunc-dimittis]: Luk. 2, 29: "Teraz odpuść, Panie, sługę twego w pokoju".\
    Słowa Symeona po ujrzeniu Dzieciątka Zbawiciela.
    --- (Przyp. tłum.)

