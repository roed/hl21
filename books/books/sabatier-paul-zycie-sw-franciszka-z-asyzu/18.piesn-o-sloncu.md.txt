﻿# Pieśń o słońcu

:::{.subtitle}
Jesień 1224 --- jesień 1225.
:::

Nazajutrz po świętym Michale (30 września 1224), Franciszek opuścił
Alwerno, aby udać się do Porcjunkuli. Zbyt był wyczerpany, aby mógł
był odbyć podróż tę pieszo i dlatego hrabia Orlando oddał konia na
jego usługi.

Łatwo wyobrazić sobie wzruszenie stygmatyzowanego, gdy żegnał się z
górą, na której rozegrał się dramat miłości i bólu, jako dokonanie
ostatecznego, całkowitego zjednoczenia istoty jego z Ukrzyżowanym.

<div lang="it" class="versei">
| Amor, amor, Gesu desideroso,
| Amor voglio morire.
| Te abrazando
| Amor dolce Gesu meo sposo,
| Amor, amor, la morte te domando,
| Amor, amor, Gesu si pietoso
| Tu me te dai in te transformato
| Pensa chi'io vo spasmando
| Non so o io me sia
| Gesu speranza mia
| Ormai va, dormi in amore.[^amor-amor]
</div>

[^amor-amor]: <div class="verse">
    | Miłość! Miłość! Jezu upragniony!
    | Miłość pragnę umrzeć,
    | Tuląc się do Ciebie.
    | Słodka Miłości, Jezu mój Oblubieńcze.
    | Miłość! Miłość! Jezusie Ty zbożny!
    | Oddaj się mi zamienionemu w Ciebie
    | Pewno zmysły stracę.
    | Nie wiem nic o sobie;
    | Jezu nadziejo moja,
    | Przyjdź, odpocząć w Miłości!
    </div>
    
    (Wolny przekład tłum.)

Tak śpiewał Jacopone z Todi w upojeniu równie gorącem.

Jeśli można zaufać dokumentowi, niedawno ogłoszonemu, to brat Maciej,
jeden z dwóch, którzy pozostali na górze Alwerno, utrwalił na piśmie
wspomnienia dnia tego.

W drogę wybrano się wczesnym rankiem. Dawszy braciom zlecenie,
Franciszek miał dla wszystkich i wszystkiego bądź spojrzenie, bądź
słowo: dla skał, dla kwiatów, dla drzew, dla brata sokoła, który miał
prawo i przywilej wchodzenia do celi każdej chwili i który każdego
ranka przybywał o pierwszem świtaniu, aby mu przypomnieć godzinę
nabożeństwa.

Potem mała gromadka wkroczyła na ścieżę, wiodącą ku Monte-Acuto. Gdy
przybyli na wzgórek, z którego po raz ostatni rzucić można spojrzenie
na Alwerno, Franciszek zsiadł z wierzchowca i, ukląkłszy na ziemi,
zwrócił się ku niemu: --- Żegnam cię --- rzekł --- góro Boża, góro
święta,
<span lang="la">*mons coagulatus, mons pinguis,
mons in quo bene placitum est Deo habitare*</span>[^mons-coagulatus];
żegnam cię, góro Alwerno, niechaj cię błogosławi
Bóg, Ojciec, Syn i Duch Święty, zostań w pokoju, nigdy już się nie
zobaczymy.

[^mons-coagulatus]: ...góro urodzajna na której Bogu
    podobało się mieszkać

Kogóż ta scena tak naiwna nie chwyci za serce swą wielką tkliwością?
Pożegnanie, którego słowa włoskie, jako niewystarczające, musiał
Franciszek uzupełnić językiem mistycznym brewjarza, aby móc wyrazić
uczucia swoje, zostało przezeń niezawodnie wypowiedziane.

W kilka minut później skała ekstazy zniknęła. Schodzenie w dolinę
odbywa się szybko. Bracia postanowili udać się na noc do Monte Casale,
małej pustelni, położonej nad Borgo San-Sepolcro. Wszyscy, nawet ci,
co pozostać mieli na Alwernie, towarzyszyli jeszcze Mistrzowi. Sam on,
pochłonięty marzeniami wnętrznemi, nie dostrzegał tego, co się działo
dokoła niego i nie zauważył nawet hałaśliwego entuzjazmu, jaki
rozpętywał jego przejazd przez miasteczka, bardzo liczne w
sąsiedztwie Tybru.

W Borgo San Sepolcro urządzono mu istną owację, która nie zbudziła go
jednakże. Potem, gdy już był daleko od tego miasta, zapytał swego
towarzysza, jakby po nagłem ocknieniu, czy rychło tam przybędą.

Pierwszy wieczór, spędzony w Monte Casale, zaznaczył się cudem.
Franciszek uzdrowił brata, który był opętany. Nazajutrz rankiem,
postanowiwszy spędzić dni kilka w tej pustelni, odesłał braci z
Alwernu i konia hrabiego Orlanda.

W jednem z miasteczek, przez które przejeżdżał dnia poprzedniego,
kobieta pewna, nie mogąc porodzić, od dni kiiku znajdowała się między
życiem a śmiercią. Otoczenie jej dowiedziało się o przejeździe
Świętego, gdy ten był już zbyt daleko, aby można było go dogonić.
Wielką była radość tych biednych ludzi, gdy rozeszła się pogłoska, że
ma przejeżdżać z powrotem. Pobiegli na jego spotkanie i byli strasznie
rozczarowani, gdy ujrzeli tylko braci. Nagle zabłysła im zbawcza
myśl: wziąwszy uzdę, poświęconą dotknięciem rąk Franciszka, zanieśli
ją nieszczęśliwej chorej, która, położywszy ją na swem ciele,
porodziła natychmiast i bez bólu.

Cud ten, opowiedziany przez powołanych, świadczy o wielkości
entuzjazmu dla osoby Franciszka śród prostego ludu.

Po kilkudniowym pobycie w Monte-Casale udał się Franciszek z bratem
Leonem do Citta di Castello. Uzdrowił tam kobietę, dotkniętą
strasznemi cierpieniami nerwowemi i przez cały miesiąc kazał w tem
mieście i jego okolicach. Była już prawie zima, gdy wybrał się w
dalszą drogę. Pewien wieśniak pożyczył mu swego osła, ale drogi były
tak złe, że przed nocą nie udało się dotrzeć do jakiegokolwiek
schronienia. Biedni wędrowcy musieli spędzić noc pod skałą; schronisko
było nad wyraz nędzne, wiatr zasypywał je śniegiem i nieszczęsny
wieśniak, ziębnąc, miotał wstrętne przekleństwa i lżył Franciszka,
lecz on mówił z takiem weselem, że rozgniewany właściciel osła
zapomniał wreszcie o chłodzie i swym złym humorze.

Nazajutrz przybył Święty do Porcjunkuli. Zdaje się, że zabawił tam
bardzo krótko i natychmiast udał się na południe Umbrji, aby tam
kazać.

Dokładnych wiadomości o tej jego wyprawie nie posiadamy. Towarzyszył
mu brat Eljasz, który nie ukrywał przed nim zaniepokojenia o jego
życie, widząc go wyczerpanym do ostateczności.

Od powrotu z Syrji (w sierpniu 1220) Franciszek słabł coraz bardziej,
ale gorliwość jego rosła z dnia na dzień. Ani cierpienia, ani błagania
braci nie zdołały go powstrzymać od pracy; siedząc na ośle,
przebiegał od trzech do czterech miasteczek w ciągu dnia. Taka
nadmierna praca sprowadziła cierpienie jeszcze przykrzejsze od
poprzednich, grożąc mu utratą wzroku.

W tym czasie rozruchy zmusiły Honorjusza III do opuszczenia Rzymu (pod
koniec kwietnia 1225). Po kilku tygodniach, spędzonych w Tivoli,
przeniósł się papież do Rieti, gdzie pobyt jego miał się przedłużyć aż
do końca r.\ 1226.

Wraz z papieżem i jego dworem przybyło do tego miasta kilku
znakomitych lekarzy. Kardynał Ugolino, który towarzyszył papieżowi,
dowiedziawszy się o chorobie Franciszka, wezwał go do Rieti, aby go
poddać leczeniu. Pomimo jednak nalegań brata Eljasza, Franciszek wahał
się długo, czy ma przyjąć to zaproszenie. Sądził on, że chory powinien
zdać się w chorobie zupełnie i jedynie na wolę Ojca niebieskiego.
Czemże bo jest cierpienie dla duszy, oddanej Bogu?

Ostatecznie brat Eljasz zdołał go jednak przekonać i podróż była
postanowiona, lecz przedtem Franciszek zapragnął udać się do świętej
Klary, aby pożegnać się z nią i nieco u niej wypocząć.

Pobyt jego u świętego Damjana trwał dłużej, niż było zamierzone (od
końca lipca do początku września 1225\ r.). Po przybyciu do tego
umiłowanego przezeń klasztoru cierpienia jego wzmogły się ogromnie. W
ciągu dwóch tygodni był tak dalece ślepym,
że nie dostrzegał nawet światła. Zabiegi,
któremi starano się złagodzić jego cierpienie, nie doprowadziły do
niczego, ponieważ co dzień w ciągu długich godzin płakiwał. Mawiał,
że roni łzy pokuty, lecz były to i łzy żalu. O, jakże inaczej
płakiwał teraz, niż ongi w chwilach wzruszeń natchnionych, gdy łzy
spływały po jego twarzy, opromienionej radością! Widywano go w takich
chwilach, jak podnosił dwa drewienka i przygrywając sobie na nich,
niby na skrzypcach, improwizował piosenki w języku francuskim,
wypowiadając się w ten sposób z nadmiaru swych uczuć.

To promieniowanie natchnienia i nadziei zniknęło. Rachel opłakuje
dzieci swoje i nie chce być pocieszona, że ich niemasz. We łzach
świętego Franciszka jest to samo
<span lang="la">*quia non sunt*</span>[^quia-non-sunt]
żalu po synach duchownych.

[^quia-non-sunt]: Że ich niemasz. Mat. 2, 18.

Lecz jeśli istnieje ból nieuleczalny, to niema bólu takiego, któryby
nie mógł wzmóc się i złagodnieć zarazem, gdy jest znoszony w bliskości
serc kochających.

Pod tym względem towarzysze jego nie mogli być dlań wielką pomocą.
Pociecha moralna możliwa jest tylko wtedy, gdy równy pociesza równego,
lub gdy dwa serca łączy tak wielkie uczucie mistyczne, że rozumieją
się i uzupełniają.

--- Ach, gdybyż bracia wiedzieli, jak bardzo cierpię --- rzekł święty
Franciszek na kilka dni przed otrzymaniem stygmatów --- jakaż litość i
jakie współczucie obudziłoby się w nich!

Oni zaś, widząc, że ten, który z wesela uczynił obowiązek, staje się
coraz bardziej smutnym i trzyma się zdała, mniemali, że ulega pokusom
djabelskim.

Klara odgadła istotę udręki. U świętego Damjana przyjaciel jej
przeżywał całą swą przeszłość. Ileż wspomnień budziło w nim jedno
spojrzenie. Oto drzewo oliwne, do którego jako rycerz wspaniały
przywiązywał swego wierzchowca; tam znowu ławka kamienna, na której
siadywał
przyjaciel jego, kapłan ubogiej kaplicy; dalej schowek, w którym
ukrywał się przed gniewem swego ojca, a nadewszystko świątyńka z
tajemnicą Krzyża rozstrzygającej godziny.

Ożywiając obrazy promiennej przeszłości, Franciszek pomnażał boleść
swoją. Atoli nie wszystko mówiło doń językiem śmierci i żalu. Była
przy nim Klara, równie stanowcza i płomienna jak niegdyś.
Przemieniona ongi przez uwielbienie, była dziś przemienioną przez
współczucie. Siedząc u stóp tego, którego kochała bardziej, niż kochać
można na ziemi, odczuwała rany jego duszy i przygnębienie serca. Cóż
wobec tego znaczył płacz, który wzmógł się tak, iż w ciągu dwóch
tygodni pozbawił go wzroku! Nadchodziło uspokojenie,
dziewica-pocieszycielka miała przywrócić mu pogodę.

Przedewszystkie zatrzymała go u siebie i sama, wziąwszy się do dzieła,
zbudowała dlań z trzciny obszerną celę w ogrodzie klasztoru, aby miał
zupełną swobodę.

Jakże miał nie przyjąć gościnności tak bardzo franciszkańskiej!
Niestety, gościnność ta była franciszkańską aż nadto; legiony szczurów
gnieździły się w tym zakątku ogrodu i w nocy wskakiwały nawet na łóżko
Franciszka, nie dając mu w cierpieniu jego ani chwili wytchnienia.
Bliskość przyjaciółki pozwoliła mu jednakże zapomnieć o tem wszystkiem
niebawem. I tym razem jeszcze przywróciła mu ona wiarę i męstwo.
Mawiał on, że jeden promień słońca może rozproszyć gęste mroki.

Tymczasem dawny Franciszek budził się powoli, i śród poszumu jodeł i
drzew oliwnych siostry słyszały nieraz echo piosnek nieznanych,
dolatujących z celi trzcinowej.

Pewnego dnia, po długiej rozmowie z Klarą, zasiadł przy stole
klasztornym. Ledwie zaczęto jeść, gdy Franciszek uległ porywowi
ekstazy.

<span lang="it">*Laudato sia lo Signore!*</span>[^laudato-sia]
--- zawołał, ocknąwszy się.
Była to chwila narodzin *Pieśni o Słońcu*.

[^laudato-sia]: Niech będzie pochwalony Pan!

## Tekst {-}

<div lang="it">
[Incipiunt Laudes Creaturarum Qua Fecit Beatus Franciscus
Ad Laudem Et Honorem Dei Cum Esset Infirmus
Ad Sanctum Damianum.]{.smallcaps}

<div class="verse">
| *Altissimu*, onnipotente, bon Signore,
  tue so' le laude,
  la gloria e l'onore et onne benedictione.
| Ad te solo, altissimo,
  se konfano et nullu homo
  ene dignu te mentovare.
| Laudato sie, mi signore,
  cum tucte le tue creature,
  spetialmente messor lo frate sole,
  lo quale jorna, et allumini noi per lui.
  Et ellu è bellu e radiante cum grande splendore,
  de te, altissimo, porta significatione.
| Laudato si, mi signore,
  per sora luna e le stelle,
  in celu l'ai formate clarite et pretiose et belle.
| Laudato si, mi signore,
  per frate vento et per aere
  et nubilo et sereno et onne tempo,
  per lo quale a le tue creature dai sustentamento.
| Laudato si, mi signore, per sor aqua,
  la quale è multo utile et humile et pretiosa et casta.
| Laudato si, mi signore, per frate focu,
  per lo quale ennallumini la nocte,
  et ello è bello et iocundo et robustoso et forte.
| Laudato si, mi signore, per sora nostra matre terra,
  la quale ne sustenta et governa,
  et produce diversi fructi con coloriti flori et herba.
| Laudato si, mi Signore,
  per quilli ke perdonano per lo tuo amore,
  et sostengo infirmitate et tribulatione.
| Beati quilli ke sosterrano in pace,
  ka dete, altissimo, sirano incoronati.
| Laudato si mi signore per sora nostra morte corporale,
  da la quale nullu homo vivente po scappare:
  guai a quilli ke morrano ne le peccata mortali;
  beati quilli che trovàra ne le tue santissime voluntati,
  ka la morte secunda nol farrà male.
| Laudate et benedicete mi signore
  et ringratiate et serviateli cum grande humilitate
</div>
</div>

## Tłumaczenie. {-}

[Zaczyna się pochwała stworzenia, którą ułożył
błogosławiony Franciszek na chwałę i cześć Boga, gdy był chory u
świętego Damjana.]{.smallcaps}

<div class="verse">
| *Najwyższy*, wszechmocny, dobrotliwy panie, twoją jest chwała, sława i
  wszelkie błogosławieństwo, tobie, samemu, Najwyższy, przystoją, a
 żaden człowiek nie jest godzien nazwać ciebie.
| Pochwalony bądź, panie, ze wszystkiem twojem stworzeniem,
  a najprzód z naszym możnym bratem słońcem, które dzień stwarza, a ty
  świecisz przez nie; a jest piękne i promienieje wielkim blaskiem;
  twojem Najwyższy, ono podobieństwem.
| Pochwalony bądź, panie przez brata księżyc i siostry gwiazdy,
  w niebie utworzyłeś je jasne i cenne i piękne.
| Pochwalony bądź, panie przez brata wiatr, przez powietrze i chmurność,
  przez jasność i wszelką pogodę,
  przez którą swym stworzeniom dajesz utrzymanie.
| Pochwalony bądź, panie, przez siostrę wodę, tak bardzo pożyteczną,
  pokorną, cenną i czystą.
| Pochwalony bądź, panie, przez brata ogień, którym
  rozjaśniasz noc, a jest piękny i wesoły, krzepki i mocny.
| Pochwalony bądź, panie, przez naszą siostrę matkę ziemię,
  która nas chowa i karmi,
  a rodzi różne owoce z ziołami i barwnem kwieciem.
| Pochwalony bądź, panie, przez tych, co przebaczają
  dla twej miłości,
  i znoszą słabości i utrapienie.
| Błogosławieni, którzy trwają w pokoju, bowiem przez ciebie,
  Najwyższy, będą uwieńczeni.
| Pochwalony bądź, panie, przez siostrę naszą śmierć cielesną,
  której człek żywy żaden ujść nie może; biada tym, którzy konają w
  grzechu śmiertelnym, błogosławieni, którzy zgodzą się z twą
 najświętszą wolą, bo im śmierć wtóra zaszkodzić nie może.
| Chwalcie i błogosławcie pana, dzięki mu czyńcie i służcie mu z wielką
  pokorą.
</div>

----

Radość, wielka jak ongi, powróciła do Franciszka. W ciągu całego
tygodnia nie pamiętał o brewjarzu, a dni spędzał na powtarzaniu
*Pieśni o Słońcu*.

Podczas pewnej nocy bezsennej usłyszał głos, mówiący do niego:
"Gdybyś miał wiarę, jako ziarno gorczyczne, rzekłbyś do tej góry:
przenieś się stąd, i przeniosłaby się".--- Górą tą były zaiste jego
cierpienia i pokusy, aby szemrał i grążył się w beznadziei. --- Uczyń
mi, Panie, wedle słowa twego --- odrzekł z całego serca, i zaraz
poczuł się, jakby wyzwolonym.

Niebawem miał spostrzec, że góra się nie przeniosła, lecz że w ciągu
kilku dni oczy jego tak były od niej odwrócone, iż zapomniał o jej
istnieniu.

Powziął był zamiar wezwania do siebie brata Pacyfika, króla wierszy,
który poprawiłby jego pieśń. Chciał przydzielić mu kilku braci, aby
chodzili z nim od miasta do miasta, każąc. Po kazaniu mieli śpiewać
*Pieśń o Słońcu*, a potem przemawiać do tłumu: --- Jesteśmy grajkami
Bożymi. Żądamy nagrody za kazanie i za pieśń, a tą nagrodą będzie
wytrwanie wasze w pokucie. Bo czyż słudzy Boży --- dodał --- nie są
jakoby grajkami, mającymi budzić serca ludzkie i wskazywać im drogę ku
radości duchownej?

Odrodził się w nim Franciszek dawny, laik, poeta, artysta.

Pieśń pochwały stworzenia jest bardzo piękna, ale brak jej jednej
zwrotki, a zwrotka ta, jeśli nie znalazła się na ustach Franciszka, to
była napewno w jego sercu:

<div class="verse">
| Bądź pochwalony, panie, za siostrę Klarę;
  uczyniłeś ją milczącą, krzątliwą i delikatną,
  i przez nią w sercach naszych płonie światłość twoja.
</div>

