﻿# Kapituła generalna r.\ 1217

Poczynając od Zielonych Świąt r.\ 1217 dane chronologiczne, dotyczące
życia św. Franciszka, są tak liczne, że niemal wykluczają możność
błędu.

Tego samego nie można, niestety, powiedzieć o osiemnastu miesiącach,
poprzedzających te czasy, a mianowicie o miesiącach od jesieni roku
1215 do Zielonych Świąt r.\ 1217. Jesteśmy tu pozostawieni prawie
wyłącznie domysłom.

Ponieważ w owym czasie Franciszek nie przedsiębrał żadnej wyprawy poza
granice Włoch, przeto wydaje się prawdopodobnem, że czasu tego użył na
pracę ewangelizacyjną we Włoszech środkowych i utrwalenie podstaw
swego dzieła. Być może, iż podczas soboru lateraneńskiego (od 11 do 30
listopada 1215\ r.) bawił w Rzymie, ale życiorysy pierwotne nic o tem
nie mówią. Sobór zajmował się niezawodnie sprawą nowego zakonu, ale
powtórzył tylko radę papieża z przed lat pięciu, aby Zakon wybrał dla
się jedną z reguł istniejących. Bawił podówczas w Rzymie święty
Dominik, który, starając się o zatwierdzenie swego zakonu, otrzymał
radę podobną i zastosował się do niej natychmiast. Stolica święta
byłaby chętnie przyznała Braciom Mniejszym ustawy osobliwe, gdyby ci
za podstawę przyjęli byli regułę św.\ Benedykta. Naprzykład klaryski, z
wyjątkiem przebywających u świętego Damjana, zachowując imię i
niektóre zwyczaje osobliwe, stosować się musiały do reguły
benedyktyńskiej.

Wbrew usilnym naleganiom, Franciszek postanowił zachować regułę
własną, Można przypuścić, że dla porozumiewania się w tych sprawach
bawił w Perugji w lipcu 1216\ r., gdy umarł Inocenty\ III.

W każdym razie wtedy właśnie kapituły nabrały wielkiego znaczenia.
Kościół, spoglądający na rozkwit Zakonu z uczuciami sprzecznemi, nie
mógł pozostawać nadal biernym świadkiem ruchu tak potężnego; należało
go wyzyskać.

Do dzieła tego Ugolino przygotowany był wspaniale. Gdy w roku 1216
zmarł Jan de Saint-Paul, czuwający nad Braćmi Mniejszymi z polecenia
Inocentego III, kardynał pośpieszył ofiarować Franciszkowi swoją
opiekę, która przyjęta została z wdzięcznością. Nad tą nadzwyczajną
usłużnością Ugolina rozwodzą się obszernie Trzej Towarzysze. Stało
się to niezawodnie w lecie r.\ 1216, bezpośrednio po śmierci Jana de
Saint-Paul.

Bardzo możliwe, że 29 maja r.\ 1216 odbyła się pierwsza kapituła w
obecności Ugolina. Popełniając błąd, bardzo w historji pospolity,
większość autorów franciszkańskich odnosiła wiele zdarzeń
różnoczesnych do jednej daty; sprawy, łączące się z kilku pierwszemi
uroczystemi zgromadzeniami Zakonu, połączono z jednem, typowem,
nazwanem *kapitułą na matach*. W rzeczywistości wszystkie zgromadzenia
Braci Mniejszych, odbywane w ciągu długich lat, zasługują na tę
nazwę.

Bracia zbierali się podczas największych upałów, sypiali pod gołem
niebem lub w szałasach z trzciny. Nie żałujmy ich, nic bowiem nie
dorówna wspaniałości jasnych nocy letnich w Umbrji. Czasem w Prowancji
doznawać można ich przedsmaku, lecz jeśli w Baux, na skale des Doms
lub w Sainte Baume widok jest równie wspaniały i potężny, brak mu
pieszczotliwej słodyczy i tych wiewów życia, które tam zachwycają i
czarują.

Mieszkańcy wsi i miasteczek okolicznych przybywali tłumnie na te
zgromadzenia, aby widzieć ceremonje, asystować przy przywdziewaniu
habitu przez ich krewnych lub przyjaciół, słyszeć wezwania Świętego i
dostarczać Braciom potrzebnego pożywienia.

Przypomina to bardzo żywo <span lang="en">*camp meeting*</span>,
tak ulubione przez
Amerykan. W liczbie uczestników, podawanej przez legendy na kilka
tysięcy, a potraktowanej nawet przez franciszkanina, O.\ Papiniego,
żartami wątpliwej wartości, niema niezawodnie takiej przesady, jakby
ktoś mógł mniemać.

Pierwsze te zgromadzenia, w których uczestniczyli wszyscy bracia,
odbywały się pod gołem niebem, w obecności tłumów, przybyłych z
bardzo daleka, a były zgoła niepodobne do późniejszych kapituł
generalnych, istnych konklawe o niewielkiej liczbie mandatarjuszów,
załatwiających na tajnych posiedzeniach wyłącznie sprawy Zakonu.

Za życia Franciszka cel tych zebrań był wyłącznie religijny. Udawano
się na nie nie po to, aby mówić o sprawach ogólnych lub zastanawiać
się nad nominacją ministra generalnego, lecz po to, aby umacniać się
wzajemnie we wspólnocie radości, w przykładach i smutkach innych
braci.

Cztery pierwsze lata po r.\ 1216 stanowią ważny etap w rozwoju ruchu
umbryjskiego: w czasie tym Franciszek walczył o samorządność. Mamy tu
do czynienia z odcieniami dość subtelnemi, których nie dostrzegali
zarówno pisarze kościelni jak i ich przeciwnicy, bowiem jeśli
Franciszek unikał pozoru buntu, to jednocześnie nie chciał
kompromitować swej niezależności, doskonale przewidując, że wszelkie
przywileje, jakiemi mógł był obdarzyć go dwór rzymski, nie warte były
ofiary z wolności.

Niestety, musiał wreszcie zgodzić się na owe złocone kajdany, przeciw
którym nie przestał protestować aż do ostatniego tchnienia. Bylibyśmy
wszakże skazani na zupełne niezrozumienie jego dzieła, gdybyśmy
zamknęli oczy na przemoc moralną, jakiej doznawał ze strony papiestwa.

Wystarczy rzucić okiem na zbiór buli, dotyczących franciszkanów, aby
zdać sobie sprawę, z jakim żarem walczył on przeciw przywilejom, tak
gorliwie poszukiwanym zwykle przez inne zakony.

W legendach odnajdujemy niezliczone rysy, świadczące niedwuznacznie o
pogardzie, jaką żywił dla tych przywilejów.
Nawet jego najbliżsi niezawsze rozumieli te jego skrupuły.

> --- Czyż nie widzisz --- rzekli doń pewnego razu --- że częstokroć
> biskupi nie pozwalają nam kazać, zmuszają nas do pozostawania
> całemi dniami bez dzieła, zanim wreszcie wolno nam głosić słowo Boże?
> Byłoby lepiej otrzymać na ten cel przywilej papieski, a byłoby to z
> korzyścią dla dusz.
>
> Pokorą i szacunkiem --- odrzekł z ożywieniem --- pragnę nawrócić
> przedewszystkiem prałatów, gdy bowiem ujrzą nas kornych i pełnych czci
> dla siebie, sami prosić nas będą, abyśmy kazali i nawracali lud...
> Co do mnie, to nie proszę Boga o żaden przywilej, prócz jednego, abym
> nie posiadał żadnego, abym był pełen szacunku dla wszystkich ludzi i
> nawracał ich, jak tego żąda nasza reguła, nietyle słowy, ile
> przykłady naszemi.

Czy Franciszek miał, czy nie miał racji, odrzucając przywileje kurji,
jest zagadnieniem nie należącem do historji. Jasnem jest, że w
postawie swojej stale trwać nie mógł: Kościół zna tylko wiernych lub
zbuntowanych. Lecz właśnie kompromisy tego rodzaju zatrważają serca
najszlachetniejsze, które chciałyby, aby przyszłość rodziła się z
przeszłości bez przesileń i wstrząśnień.

Kapituła r.\ 1217 zaznaczyła się zorganizowaniem ostatecznem misyj
franciszkańskich: Włochy i inne kraje podzielone zostały w celu ich
ewangelizacji na pewną ilość prowincyj, z których każda posiadała
swego ministra prowincjonalnego. Od czasu wstąpienia na tron papieski
(18 lipca 1216), Honorjusz III starał się ożywić w ludzie gorliwość
dla krucjaty. Nie zadowoliły go w tej mierze kazania, więc uciekał się
nawet do proroctw, zapewniając, że za jego pontyfikatu Ziemia święta
zostanie odzyskana. Odnowiona na skutek tych wezwań gorliwość, która
rozprzestrzeniła się nawet w Niemczech, wywarła wpływ głęboki na Braci
Mniejszych. Tym razem Franciszek, przez pokorę, nie stanął na czele
braci, wyznaczonych do misji w Syrji, lecz dał im za przewodnika
znamienitego Eljasza, przebywającego zrazu we Florencji, gdzie miał
sposobność do okazania swych wysokich przymiotów.

Brat ten, który rychło wysunie się na pierwszy plan tej historji,
zajmował jedno z najniższych miejsc w zgromadzeniu.

Nie znamy czasu i okoliczności jego wstąpienia do Zakonu i to
doprowadziło mnie do przypuszczenia, że jest on tym przyjacielem
Franciszka, który służył mu za powiernika przed jego nawróceniem się
ostatecznem. Wiemy tylko, że, będąc młodym, pracował w Asyżu,
wyrabiając sienniki i ucząc czytać kilkoro dzieci; następnie spędził
czas jakiś w Bolonji jako <span lang="la">*scriptori*</span>
nagle spotykamy go śród Braci Mniejszych, spełniającego najtrudniejsze zadania.

Przeciwnicy jego głoszą na wyścigi, że był najtęższą głową swego
stulecia; niestety, przy dzisiejszym stanie źródeł nie mamy możności
wypowiedzenia się co do jego czynów. Oświecony i energiczny, żądny
odgrywania pierwszorzędnej roli w sprawie naprawy religijnej, miał on
już z góry ustalony plan realizowania swych zamiarów i dlatego dążył
prosto do celu, napoły religijnego, napoły politycznego. Pełen
uwielbienia i uznania dla Franciszka, chciał on ukarnić i utrwalić
ruch odnowicielski. W kole franciszkańskiem, w którem bracia Leon,
Jałowiec, Egidjusz i tylu innych reprezentowali ducha wolności,
religję prostych i pokornych, słoneczną poezję Umbrji, brat Eljasz
był przedstawicielem ducha uczoności i ducha kościelnego,
roztropności i rozumu.

Miał on wielkie powodzenie w Syrji i przyjął do Zakonu jednego z
najdroższych dla Franciszka uczni, mianowicie Cezarego ze Spiry,
który później, w przeciągu niespełna dwóch lat (1221--1223) pozyskać
miał dla ruchu całe Niemcy południowe i który krwią swą
przypieczętował wierność dla ścisłego przestrzegania reguły, bronionej
przezeń nawet przeciw samemu Eljaszowi.[^cezary-ze-spiry]

[^cezary-ze-spiry]: Uwięziony na rozkaz Eliasza został pobity
    gdy znajdował się poza więzieniem.
    Skutkiem tego pobicia umarł.

Cezary ze Spiry to wspaniały przykład duchów dręczonych i
niepokojonych przez ideał, tak licznych w stuleciu trzynastem, i
poszukujących zaspokojenia męczącego ich pragnienia najprzód w wiedzy,
następnie zaś w życiu religijnem. Jako uczeń scholastyka Konrada,
pragnął współdziałać w naprawie Kościoła; będąc jeszcze laikiem,
głosił
swoje ideje, nie bez powodzenia, ponieważ pewne grono pań w Spirze
zaczęło wieść nowe życie. Gdy obudziło to gniew w ich mężach,
kaznodzieja w obawie zemsty z ich strony, musiał uciec do Paryża, stąd
udał się na Wschód; tam w kazaniu Braci Mniejszych odnalazł własne
pragnienia i marzenia. Ten przykład poucza nas, jak powszechnym był w
duszach stan wyczekiwania, gdy zaczęło się głoszenie ewangelji
franciszkańskiej i jak dalece miała ona drogi wszędzie przygotowane.

Powróćmyż atoli do kapituły r.\ 1217. Bracia wysłani do Niemiec pod
przewodem Jana z Penny, nawet w części nie mogli pochlubić się takiem
powodzeniem, jakiego doznał Eljasz i jego towarzysze; nie znali oni
zupełnie języka kraju, w którym mieli kazać. Być może, iż Franciszek
nie zdawał sobie sprawy z tego, że jeśli język włoski mógł od biedy
wystarczyć w krajach śródziemnomorskich, to inaczej miały się rzeczy w
Europie środkowej.

Powodzenie grupy, wysłanej do Węgier, było nie większe. Bardzo często
bracia misjonarze musieli pozbyć się całego swego przyodziewku i
rozdać go chłopom i pasterzom, którzy nimi pomiatali; chcieli ich w
ten sposób zjednać dla siebie. Lecz nie rozumiejąc mieszkańców i sami
przez nich nierozumiani, musieli rychło pomyśleć o powrocie do Włoch.
Winniśmy wdzięczność autorom franciszkańskim za przekazanie nam
wiadomości o tych niepowodzeniach i za to, że nie próbowali
przedstawić nam braci jako za natchnieniem Bożem posiadających nagle
wszystkie języki, jak o tem później tak często opowiadano.

Ci z braci, którzy wysiani zostali do Hiszpanji, także nie uniknęli
prześladowań. Kraj ten, tak samo jak południe Francji, był nawiedzony
przez herezje, ale tłumiono je tam gwałtownie. Franciszkanie,
podejrzani, że nie są dobrymi katolikami i prześladowani z tego
powodu, znaleźli schronienie u królowej portugalskiej, Urraki, która
na miejsce pobytu wyznaczała im Coimbrę, Guimarrens, Alenquer i
Lizbonę.

Sam Franciszek wybierał się do Francji. Kraj ten z powodu swej
gorliwości dla najświętszego Sakramentu pociągał go osobliwie.
Być może, iż do kraju, od którego otrzymał był
imię i rycerskie sny swej młodości, pociągało go bezwiednie wszystko
to, co w życiu jego było poezją, śpiewem, muzyką, wnziosłem
marzeniem.

Nieco z tych wzruszeń, jakich doznawał, przedsiębiorąc tę nową misję,
przeniknęło do opowiadań biografów. Czuje się w nich drżenie pełne
zarazem słodyczy i niepokoju, bicie serca śmiałego rycerza, który
cały w zbroi wybiera się w drogę o pierwszem świtaniu, bada widnokrąg
i doznaje zaniepokojenia wobec rzeczy nieznanych, ale zarazem pełen
jest radości, bowiem wie, iż dzień ten poświęcony będzie
sprawiedliwości i miłości.

Poeta włoski nazwał pielgrzymkami miłości zarówno wyprawy rycerskie
jak i podróże, przedsiębrane przez marzycieli, artystów lub świętych
do tych zakątków ziemi, które bezustannie olśniewają ich wyobraźnię
jako ojczyzna z wyboru. Taką była wyprawa, którą przedsiębrał
Franciszek.

> --- Idźcie --- mówił do towarzyszących mu braci --- i wędrujcie po
> dwóch, a bądźcie łagodni i pokorni, zachowując przystojne
> milczenie, modląc się. w sercach swoich i unikając słów próżnych i
> zbędnych. W podróży tej bądźcie równie skupieni, jakbyście się
> znajdowali zamknięci w pustelni lub w celi swojej, bowiem
> wszędzie, gdziekolwiek bawimy lub idziemy, niesiemy celę swoją z
> sobą: brat ciało jest celą naszą, a dusza jest pustelnicą, która
> ją zamieszkuje, aby się modlić i rozmyślać.

Przybywszy do Florencji, zastał tam kardynała Ugolino, wysłanego przez
papieża jako legata do Toskany. aby głosił tu krucjatę i przedsiębrał
co należy dla zapewnienia jej powodzenia.

Franciszek nie oczekiwał pewno takiego przyjęcia, jakiego doznał ze
strony prałata. Ten bowiem, zamiast go zachęcić, odwodził go od
powziętego zamiaru.

> --- Nie chcę, bracie, abyś udawał się za góry; dużo jest prałatów,
> którzy myślą tylko o tem, aby ci sprawiać trudności u dworu
> rzymskiego. Lecz ja i inni kardynałowie, którzy kochamy twój
> Zakon, pragniemy popierać i pomagać ci, pod warunkiem jednak,
> abyś nie oddalał się z tej prowincji.
> 
> --- Lecz, panie, co za wstyd dla mnie, gdybym, wysyłając braci swoich w
> dal, miał pozostawać gnuśnie tutaj i nie dzielić z nimi wszystkich
> trudów, jakie ponoszą.
> 
> --- Dlaczegóż tedy wysłałeś braci tak daleko, narażając ich na śmierć
> głodową, i na wszelkie niebezpieczeństwo?
> 
> --- Sądzisz, panie, --- odparł Franciszek z żarem i jakby porwany
> natchnieniem proroczem --- że Bóg zbudził braci tylko dla tego
> kraju? Zaprawdę, powiadam tobie, że Bóg powołał ich do budzenia i
> dla zbawienia wszystkich ludzi, oni zaś zdobywać będą dusze
> nietylko w krajach ludzi wierzących, lecz nawet pośród
> niewiernych.

Podziw i uwielbienie, jakie słowa te zbudziły w Ugolinie, nie
doprowadziły go jednak do zmiany poglądu. Nalegał tak bardzo, że
Franciszek wybrał się z powrotem do Porcjunkuli: nie chodziło tu o
sam pomysł wyprawy. Kto wie, czy radość, którą obiecywał sobie z
ujrzenia Francji, nie utwierdziła go w myśli, iż winien jej się zrzec.
Dusze, niepokojone potrzebą ofiary, doznają często tego rodzaju
skrupułów; wyrzekają się radości najbardziej godziwych, aby ofiarować
je Bogu.

Nie wiemy, czy natychmiast po tej rozmowie, czy też dopiero roku
następnego, Franciszek postawił na czele misjonarzy, wysłanych do
Francji, brata Pacyfika.

Będąc utalentowanym poetą, miał Pacyfik przed swojem nawróceniem tytuł
księcia poezji i był uwieńczony na kapitolu przez cesarza. Pewnego
razu, gdy odwiedzał jednę ze swoich krewnych, zakonnicę w San
Severino, w Marchji Ankońskiej, Franciszek przybył także do tego
klasztoru i kazał z tak świętą gwałtownością, że poeta uczuł się
przebity mieczem, o którym mówi Pismo święte, mieczem przenikającym
aż do szpiku kości, a rozeznawającym uczucia i myśli serca.[^do-zydow]
Nazajutrz przywdział habit i otrzymał swe imię symboliczne.

[^do-zydow]: Do Żydów 4, 12.

W podróży do Francji towarzyszył mu brat Anioł z Pizy, który w roku
1224 miał stanąć na czele pierwszej wyprawy do Anglji.

Wysyłając braci do Francji, nie mógł był pomyśleć Franciszek, że z
kraju tego, który pociągał go tak nieprzeparcie, wyjdzie ruch
sprzeniewierzający się jego marzeniu, że Paryż zgubi Asyż. A czasy te
niebardzo były odległe: w niewiele lat później miał Biedaczyna ujrzeć
część swej rodziny duchownej, jak idzie drogą zapomnienia o pokorze
swego imienia, o swych zaczątkach i swych ślubach, aby zdobywać ułudne
laury wiedzy.

Franciszkanie owych czasów mieli zwyczaj obierania siedzib w
sąsiedztwie wielkich miast: Pacyfik i jego towarzysze osiedli w
Saint-Denis. Nie posiadamy żadnych szczegółów o ich działalności,
która była bardzo płodną, ponieważ w niewiele lat później umożliwiła
wyprawę do Anglji, uwieńczoną powodzeniem zupełnem.

Rok następny (1218) spędził Franciszek na wędrówkach
ewangelizacyjnych po Półwyspie. Niepodobna, oczywiście, śledzić tych
jego wędrówek, których plan zależał od nagłych natchnień lub wskazówek
równie fantastycznych jak ta, która, skłoniła go do udania się do
Sieny. Bolonja, Alwerno, dolina Rieti, Sacro-Speco od świętego
Benedykta do Subiaco, Gaeta, San Michele na górze Gargano widywały go
pewnie w tym czasie, lecz wskazówki o jego pobycie w tych miejscach
są zbyt rozproszone i chwiejne, aby mogły znaleźć miejsce w ramach
historji.

Bardzo jest możliwe, że w tym czasie bawił także w Rzymie: stosunki
jego z Ugolinem były częstsze, niż się zwykle sądzi. Opowiadania
biografów nie powinny mylić nas w tym względzie: jest zgoła
naturalnem, że w trzech lub czterech ważnych datach zamykamy często
wszystko, co wiemy o danym człowieku. Zapominamy o całych latach życia
ludzi najlepiej nam znanych i najbardziej kochanych, a wspomnienia
swoje grupujemy dokoła kilku faktów uderzających, które jaśnieją tem
żywiej, im zupełniejszym staje się mrok dokoła nich. Słowa Jezusowe,
wypowiadane przy wielu sposobnościach, połączone zostały w jedną mowę,
Kazanie na górze. Krytyka winna w takich razach być subtelną i do
ciężkiej artylerji argumentacji naukowej dodawać trochę wnikliwej
intuicji.

Teksty są rzeczą świętą, lecz nie należy robić z nich fetyszów. Nikt
dzisiaj, wbrew świętemu Mateuszowi, nie przedstawia sobie Jezusa jako
wypowiadającego jednym tchem kazanie na górze. Podobnie w
opowiadaniach, przekazanych nam o stosunkach świętego Franciszka z
Ugolinem, znajdujemy się co chwila w ślepej uliczce, potykamy się o
wskazówki przeczące sobie, jeśli chcemy stosunki te sprowadzić do
dwóch lub trzech spotkań, jakiemi przedstawiają się nam na pierwsze
spojrzenie.

Gdy sprawę stawiamy prosto, trudności te znikają i każde ze
sprzecznych opowiadań przedstawia fragment, który po złączeniu z
innemi, tworzy całość organiczną, żywą i psychologicznie prawdziwą.

Poczynając od chwili, do której doszliśmy, trzeba będzie wyznaczyć
Ugolinowi rolę jeszcze ważniejszą, niż dotąd. Zaczyna się walka
ostateczna między ideałem franciszkańskim, chimerycznym być może,
lecz wzniosłym, a polityką kościelną, która doprowadzi do tego, że
Franciszek trochę przez pokorę, trochę przez zniechęcenie, czując
zbliżającą się śmierć, zrzeknie się kierowania swoją rodziną
duchowną.

Ugolino powrócił do Rzymu pod koniec r.\ 1217. W ciągu zimy następnej
podpisywał bulle najważniejsze. Czas swój poświęcał badaniu sprawy
nowych zakonów i wezwał Franciszka do siebie. Pamiętamy, z jaką
otwartością oświadczył Franciszkowi we Florencji, że wielu prałatów
stara się szkodzić u papieża jego Zakonowi. Jest widocznem, że
powodzenie Zakonu i jego postawa, przypominająca herezję wbrew
wszelkim zapewnieniom przeciwnym, niezależność Franciszka, który nie
myśląc nawet starać się o potwierdzenie udzielonego mu przez
Inocentego III przyzwolenia ustnego i wyłącznie tymczasowego, rozsyłał
braci swoich na wszystkie cztery strony świata, --- wszystko to
musiało kler niepokoić.

Ugolino, który lepiej od kogokolwiek znał Umbrję, Toskanę, Emilję,
Marchję Ankońską, wszystkie te krainy, w których kazanie
franciszkańskie budziło echa najpotężniejsze, musiał także zdawać
sobie sprawę z potęgi tego nowego
ruchu i z konieczności zapanowania nad nim. Czuł on, że najlepszym
środkiem do obalenia uprzedzeń, jakie papież i Święte Kollegium żywić
mogli przeciw Franciszkowi, będzie przedstawienie go kurji.

Franciszka onieśmieliła zrazu myśl przemawiania do namiestnika Jezusa
Chrystusa, lecz na skutek nalegań swego opiekuna zdecydował się pójść
do Rzymu i dla większej pewności wyuczył się na pamięć wszystkiego, co
powiedzieć zamierzał.

Ugolino nie był jednakże zupełnie pewnym skutków tego kroku. Tomasz
Celano mówi, że pożerał go niepokój. Cierpiał za Franciszka, którego
naiwna wymowa mogła była narazić się na bardzo wiele w salach pałacu
Lateraneńskiego. W niepokoju o Franciszka było dużo niepokoju
osobistego, gdyż niepowodzenie jego pupila mogło było zaszkodzić mu
bardzo. Kłopot jego stał się tem większym, gdy Franciszek, przybywszy
do najwyższego kapłana, zapomniał wszystko, co chciał był powiedzieć.
Przyznał się do tego z wielką prostotą i idąc za głosem natchnienia,
przemówił z takim naiwnym żarem, że zgromadzenie zostało pokonane.

Biografowie milczą o skutkach praktycznych tego posłuchania. Nie
trzeba dziwić się temu, gdyż celem ich jedynym jest zbudowanie
czytelnika. Pisali oni, gdy apoteoza ich mistrza była dokonana i
uważali za niewłaściwe przypominanie trudności, napotykanych przezeń
w ciągu pierwszych lat.

Stolica święta musiała czuć się mocno pomieszaną wobec tego dziwnego
człowieka, który ujarzmiał swą wiarą i pokorą, lecz którego niepodobna
była nakłonić do posłuszeństwa kościelnego.

Święty Dominik znajdował się w Rzymie w tym samym czasie i był
zasypywany dowodami przychylności papieża. Wiemy, że gdy Inocenty III
wezwał go, aby wybrał jedną z reguł już istniejących, Dominik,
porozumiawszy się ze swymi braćmi, wybrał regułę św.\ Augustyna. To też
Honorjusz nie skąpił mu przywilejów. Nie jest wykluczonem,
że Ugolino starał się przezeń wpłynąć na świętego Franciszka.

Kurja zdawała sobie doskonale sprawę z tego, że Dominik, którego
zakon liczył wówczas zaledwie kilka dziesiątków członków, stanowi
wielką potęgę moralną swej epoki, lecz nie doznawała ona względem
niego uczuć tak pomieszanych, jakie budził w niej Franciszek.

Połączyć oba zakony, rzucić na ramiona dominikanów brunatną suknię
Biedaka z Asyżu i opromienić ich popularnością Braci Mniejszych;
pozostawić tym ostatnim ich imię, habit, a nawet pozór reguły własnej,
uzupełnionej jednakże regułą świętego Augustyna, oto pomysł, który
musiał Ugolina pociągać w sposób osobliwy, a który przy uległości
Franciszka miał widoki urzeczywistnienia.

Dnia pewnego, po długiem, świątobliwem naleganiu. Dominik skłonił
Franciszka do dania mu swego sznurka i natychmiast opasał się nim. ---
Bracie --- dodał --- pragnąłbym gorąco, aby zakon twój połączył się z
moim, tworząc w Kościele jedno zgromadzenie. --- Lecz Brat Mniejszy
postanowił pozostać tem, czem był i uchylił się od tej propozycji.
Odczuwał on tak dobrze potrzeby wieku i Kościoła, że w niespełna trzy
lata później, Dominik, ulegając nieprzepartemu prądowi, widział się
zmuszonym do przekształcenia swego zakonu kanoników świętego
Augustyna na zakon mnichów żebrzących, których ustawa była odbiciem
franciszkańskiej.

W kilka lat później dominikanie dożyli zadośćuczynienia, że Bracia
Mniejsi zmuszeni byli poczynić w swych pracach wielkie ustępstwa na
rzecz wiedzy. W ten to sposób już w zaraniu swego istnienia obie
rodziny zakonne współzawodniczyły z sobą, przenikały się wzajemnemi
wpływami, nigdy jednakże nie zatracając zupełnie śladów swego
pochodzenia. Działalność jednej streszczała się w ubóstwie i kazaniu
laickiem, działalność drugiej w nauce i kazaniu klerykalnem.

