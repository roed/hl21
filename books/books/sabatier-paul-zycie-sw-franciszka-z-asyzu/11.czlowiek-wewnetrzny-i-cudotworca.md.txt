﻿# Człowiek wewnętrzny i cudotwórca

Wyprawa misyjna, podjęta za zachętą świętej Klary, a tak poetycznie
rozpoczęta kazaniem do ptaków w Bevagni, była, zdaje się,
nieprzerwanym dla Franciszka triumfem. Legenda bierze go w
niepodzielne władanie; czy chce, czy nie chce, cuda biegną śladem jego
kroków; nawet bez jego wiedzy, przedmioty, któremi się posługiwał,
mają własności cudowne; miasteczka wychodzą procesjonalnie na jego
spotkanie, a biografowie stają się echem owych włoskich uroczystości
religijnych, wesołych, popularnych, hałaśliwych i pełnych słońca,
czem różnią się tak bardzo od podobnych uroczystości, troskliwie
organizowanych przez ludy północne.

Z Alviano przybył Franciszek niezawodnie do Narni, jednego z
najrozkoszniejszych miasteczek Umbrji, które po zdobyciu wolności
komunalnej budowało właśnie katedrę. Zdaje się, że miasteczko to było
mu szczególniej miłem, podobnie jak miasteczka sąsiednie.

Stamtąd udał się prawdopodobnie w dolinę Rieti, gdzie Greccio,
Fonte-Colombo, San-Fabiano, Sant-Eleuthero, Poggio-Buscone przechowują
więcej śladów jego pobytu, niż okolice Asyżu.

Tomasz Celano nie podaje nam żadnych wskazówek co do drogi przebytej,
lecz rozwodzi się obszernie o powodzeniu Apostoła na pograniczu
Ankony, a przedewszystkiem w Ascoli. Czy ludność tych okolic pamiętała
jeszcze o wezwaniach, z któremi Franciszek i Egidjusz przybyli do
niej przed laty sześciu (1209), czy też przypuścić należy, że była ona
osobliwie przygotowana do zrozumienia nowej Ewangelji? Jakąkolwiek
jest przyczyna tego, nigdzie nie ujawniono podobnego entuzjazmu;
skutek kazań był taki, że trzydziestu neofitów bezpośrednio pod ich
wpływem przywdziało habit.

Marchja Ankońska stać się miała prowincją franciszkańską w pełni
znaczenia wyrazu. Tam są pustelnie Offida, San-Severino, Macerata,
Forano, Cingoli, Fermo, Massa i wielu innych, w których ubóstwo
znajdować miało w ciągu całego jednego stulecia swych głosicieli i
męczenników; stamtąd-to wyszli Jan z Alwernu, Jakób z Massy, Konrad z
Offidy, Anioł Clareno i legjony tych rewolucjonistów bezimiennych,
marzycieli, proroków, którzy od czasu braci *wyrwanych* w roku 1244
przez generała Zakonu, Krescencjusza de Jesi, nie przestawali się
mnożyć, a swoim dumnym oporem wobec wszelkich władz, zapisali jedną z
najpiękniejszych kart historji religijnej wieków średnich.

Powodzenie to, napełniając radością duszę Franciszka, nie obudziło w
nim najmniejszego odruchu pychy. Nigdy człowiek nie posiadał większej
władzy nad sercami, ponieważ nigdy kaznodzieja nie mówił mniej o
samym sobie. Pewnego razu brat Maciej postanowił wystawić skromność
jego na próbę.

> --- Dlaczego ty? Dlaczego ty? Dlaczego ty? --- powtarzał kilka razy, jak
> gdyby podrwiwał z Franciszka. --- Czegóż to chcesz, bracie? ---
> zawołał wreszcie Franciszek. --- Chcę wiedzieć, dlaczego wszyscy
> biegają za tobą, dlaczego każdy chce cię widzieć, słyszeć,
> słuchać, chociaż nie jesteś ani piękny, ani uczony, ani
> pochodzisz ze szlachetnego rodu? Czem to się dzieje, że wszyscy
> chcą iść za tobą?
> 
> --- Słysząc te słowa, błogosławiony Franciszek pełen radości wzniósł
> oczy ku niebu i przez długą chwilę trwał w zamyśleniu; potem
> ukląkł, chwaląc i błogosławiąc Boga z żarem nadzwyczajnym.
> Wreszcie zwrócił się do Macieja: --- Chcesz wiedzieć, dlaczego
> wszyscy idą za mną? Chcesz wiedzieć? Oto dlatego, że oczy
> Najwyższego tak chciały: spoglądają one bezustannie na dobrych i
> na złych i ponieważ te najświętsze oczy nie znalazły śród
> grzeszników żadnego, któryby był ode mnie mniejszy, nędzniejszy i
> bardziej
> grzeszny, wybrały mnie, abym dokonał cudownego dzieła, które Bóg
> przedsięwziął; wybrał mnie, dlatego, iż nie znalazł człeka
> marniejszego, i dlatego, że postanowił zaćmić szlachectwo i
> wielkość, moc, piękno i mądrość świata.

Odpowiedź ta rzuca promień światła na serce Franciszka: posłannictwem
jego jest przypomnieć światu jeszcze raz dobrą nowinę, zwiastowaną
ubogim; celem jego jest podjęcie tego dzieła mesjanicznego, które
ujrzała Dziewica z Nazaretu, gdy śpiewała swoje *Magnificat*, ów śpiew
miłości i wolności, w którego westchnieniach przesuwa się wizja nowego
porządku społecznego.

Przypomina on, że szczęście człowieka, pokój jego serca, radość życia,
nie spoczywają ani w pieniądzach, ani w wiedzy, ani w mocy, lecz w
woli prostej a szczerej. Pokój ludziom dobrej woli!

Rolę, jaką odegrał w Asyżu w zatargach swych współobywateli, byłby
pragnął odegrać także na całej reszcie Półwyspu, bowiem nikt dotąd
nie marzył o odnowie społecznej bardziej doskonałej, lecz chociaż cel
jego podobny był do celu, zamierzanego przez wielu rewolucjonistów,
którzy przyszli po nim, to jednak środki jego są zupełnie odmienne:
jedyną jego bronią była miłość.

Wydarzenia omyliły go. Z wyjątkiem oświeconych Marchji Ankońskiej i
*Fraticellów* Prowancji, uczniowie jego świadomie spaczyli jego myśl.

Kto wie wszakże, czy nie powstanie ktoś i nie podejmie na nowo jego
dzieła? Czyż szał zwodniczej spekulacji niedość kosztował ofiar? Czyż
mało jest śród nas takich, którzy widzą, że przepych jest tylko ułudą?
Że życie, aczkolwiek jest walką, nie jest przecie morderczym zapasem
dzikich zwierząt o zdobycz, ale walką o boskość, ukazującą się nam w
postaci prawdy, piękna, miłości. Któż wie, czy ten konający wiek
dziewiętnasty nie dźwignie się ze swego łoża, aby swemu następcy
przekazać słowo męskiej wiary?[^nadzieja-autora]

[^nadzieja-autora]: Pierwsze wydanie tego dzieła ukazało się w roku 1893.
    Twierdzenie, że autor mylił się w swej nadziei, byłoby przedwczesnem.
    --- (Przyp. tłum.)

Tak, Mesjasz się zjawi. Ten, którego przepowiedział Joachim de Flore,
a który rozpocząć ma nowy okres dziejów ludzkości, przyjdzie.
*Nadzieja nie pohańbia nigdy*. W naszych Babilonach współczesnych i w
chatkach naszych wiosek zbyt wiele jest dusz wzdychających słowy
hymnu wielkiej wigilji: <span lang="la">*Rorate coeli desuper et nubes pluant
Justum*</span>[^rorate-coeli], abyśmy nie mieli wierzyć, że bliski jest dzień boskiego
poczęcia.

[^rorate-coeli]: Spuście niebiosa rosę z góry a obłoki niech
    kropią sprawiedliwego.\
    Jez. 45, 8.

Wszelki początek jest tajemniczy, nietylko w świecie materji, lecz
jeszcze bardziej w życiu świątobliwości, o tyle wyższem od innych
postaci życia. W modlitwie znajdował Franciszek źródło potrzebnej siły
duchowej i dlatego szukał samotności i milczenia. Acz umiał walczyć
śród ludzi, aby zdobywać ich dla wiary, lubił --- wyrażając się słowy
Celana --- wzlatać ptakiem i budować gniazdo na górze.

Dla ludzi szczerze pobożnych modlitwa mówiona usty i wyrażana słowy
jest jedynie niższą postacią prawdziwej modlitwy. Nawet wtedy, gdy
modlitwa jest szczerą i skupioną, a nie mechanicznem wymawianiem słów.
stanowi ona dopiero przegrywkę dla dusz, których nie zabił
materjalizm religijny.

Do pobożności nic nie jest podobnem tak, jak miłość. Formuły
modlitewne równie niezdolne są wyrazić wzruszenia duszy, jak wzory
listów miłosnych nie potrafią wypowiedzieć uniesień rozkochanego
serca. Dla prawej pobożności jak dla głębokiej miłości wszelka
formuła stanowi już pewien rodzaj profanacji.

Modlić się, to znaczy rozmawiać z Bogiem, wznosić się ku Niemu, aby
mógł zstąpić ku nam, obcować z Nim. Jest to akt skupienia,
rozmyślania, wymagający wysiłku najbardziej osobistego.

Tak pojęta modlitwa jest matką wszelkiej wolności i wyswobodzenia.

Choćby była rozmową duszy z samą sobą, rozmowa taka jest tem niemniej
rdzeniem potężnych osobistości.

U świętego Franciszka, jak u Jezusa, modlitwa nosi piętno wysiłku,
który czyni z niej akt moralny w najwyższem znaczeniu słowa. Aby
poznać do gruntu takich ludzi, trzebaby móc towarzyszyć im, iść za
Jezusem na owe wzgórza, na których spędzał noce. Trzej wybrani, Piotr,
Jakub, Jan, towarzyszyli mu razu pewnego, lecz dla opisania tego, co
widzieli i tego, co męskie *sursum corda* dorzuciło do promiennej,
tajemniczej wielkości Uwielbianego, uciec się musieli do języka
symbolu.

Podobnie miała się rzecz z świętym Franciszkiem. Dla niego, jak i dla
jego Mistrza, modlitwa była komunją z Ojcem niebieskim, zjednoczeniem
boskości z człowieczeństwem, a raczej usiłowaniem człowieka czynienia
dzieła Bożego; modlitwa to nietylko bierne, zrezygnowane i bezsilne
*Fiat!* --- lecz wzniosłe a mężne oświadczenie: --- Oto, Panie, gotowym
czynić wolę twoją!

Są niezgłębione moce w duszy ludzkiej, ponieważ jest w niej sam Bóg.
Czy Bóg jest pojęty jako transcendentny, czy jako immanentny; czy jako
Jeden, Stwórca, Pierwiastek wiekuisty i niewzruszalny, lub czy jest,
jak mówią uczeni z za Renu, idealną objektywacją naszego ja, bohater
człowieczeństwa nad tem się nie zastanawia. Śród wrzawy bitewnej
żołnierz nie filozofuje o prawdziwości lub fałszywości uczucia
patrjotycznego; uzbraja się i walczy, narażając życie. Żołnierze walk
duchownych podobnie skupiają swe siły w modlitwie, rozmyślaniu,
kontemplacji, natchnieniu. Wszyscy: poeci, artyści, wtajemniczyciele,
święci, prawodawcy, prorocy, wodzowie narodów, uczeni, filozofowie
--- czerpią z tego samego źródła.

Lecz nie bez trudu przychodzi duszy łączenie się z Bogiem, lub jeśli
kto woli, odnajdywanie samej siebie. Modlitwa tylko wówczas jest boską
komunją, gdy jednocześnie jest walką. Wiedział już o tem patrjarcha
izraelski, który spał niedaleko Bethel; Bóg przechodzący ujawnia imię
swoje tylko tym, którzy go zatrzymają i świętą przemocą wydrą mu Jego
tajemnicę. Błogosławieństwa udziela Bóg dopiero po długiej walce.

Ewangelja przedstawia nam charakter modlitw Jezusowych słowem, nie
dającem się przetłumaczyć; przyrównywa ona walkę, poprzedzającą
dobrowolne ofiarowanie się Jezusa, do agonji:
<span lang="la">*Factus in agonia*</span>.[^factus-in-agonia]
Możemy rzec, iż życie jego było jedynem pasmem pokuszeń, walk,
modlitwy, ponieważ słowa te wyrażają tylko różne stopnie napięcia
ducha.

[^factus-in-agonia]: [Łuk. 22, 44](http://biblehub.com/text/luke/22-44.htm):
    <span lang="grc">*γενόμενος ἐν ἀγωνίᾳ*</span>...\
    Vulgata: <span lang="la">*factus in agonia*</span>...\
    Wujek: *będąc w ciężkości*...\
    Inni: *będąc w boju*...\
    Francuzi tłumaczą: <span lang="fr">*étant en agonie*</span>...
    --- (Przyp. tłum.)

Jak Mistrz, tak też uczniowie i naśladowcy jego mogą przezwyciężyć
duszę swoją tylko dzięki wytrwałości. Lecz słowa te, pozbawione treści
dla różnych bractw pobożnych, w życiu genjuszów religijnych miały
znaczenie tragiczne.

Pod względem historycznym niema nic bardziej fałszywego od postaci
świętych, zdobiących nasze kościoły, z ich milutką postawą, smętnemi
minami i tem czemś anemicznem i suchotniczem, co postaci te czyni
wprost niemęskiemi. Są to raczej pobożni alumni, wychowani w duchu
świętego Alfonsa Liguorego lub świętego Ludwika Gonzagi, lecz nie
święci bojownicy, szturmujący bramy nieba.

Dotykamy jednej z najbardziej delikatnych stron życia Franciszka: jego
stosunku do potęg djabelskich. Obyczaje i wyobrażenia uległy zmianom
tak głębokim w sprawach dotyczących istnienia djabła i jego stosunków
z ludźmi, że niepodobna wprost wyobrazić sobie, iż tak wiele myśli
poświęconych było ongi demonom.

Najlepsze umysły wieków średnich wierzyły niezachwianie w istnienie
Ducha złego, w jego bezustanne przekształcanie się, kuszenie ludzi i
wikłanie ich w sidła grzechu. Jeszcze w szesnastem stuleciu Luter,
który podkopał tyle wierzeń, nie przestaje wierzyć w osobiste
istnienie szatana, w czary, zaklęcia i opętanie.

Dostrzegając w duszach swoich bezmiar nędzy i wielkości, słysząc
odzywające się w nich czasem harmonijne echo wezwań do życia wyższego,
przegłuszane wnetże rykiem
zwierzęcia, przodkowie nasi nie mogli nie szukać wyjaśnienia tych
walk; tłumaczyli je sobie jako boje demonów z Bogiem.

Djabeł jest księciem demonów, jak Bóg jest księciem aniołów; zdolni do
wszelkiego przeobrażania się, toczą z sobą walkę, która trwać będzie
aż do końca czasów, a ta walka straszliwa zakończy się zwycięstwem
Boga. W przebiegu tej walki każdy człowiek przez całe swe życie jest
przedmiotem zabiegów ze strony każdego z tych przeciwników, zaś o
dusze najpiękniejsze toczy się, naturalnie, walka najgorętsza.

W ten to sposób święty Franciszek wraz ze swojem stuleciem tłumaczył
sobie zaniepokojenie, przestrach i lęk, nawiedzające jego serce, tak
też rozumiał nadzieję, pociechę i radość, tych stałych gości swej
duszy. Wszędzie, gdziekolwiek odnajdujemy jego ślady, tradycja
miejscowa przechowała pamięć strasznych napaści, jakiemi napastował
go kusiciel.

Zdaje się, iż nie trzeba tu przypominać owego faktu elementarnego, że
gdy z biegiem czasu zmieniają się obyczaje, człowiek ulega niemniej
zadziwiającym przemianom. Jeśli zależnie od wykształcenia i rodzaju
życia pewien zmysł osiągnąć może wrażliwość, wprawiającą w zdumienie
nasze codzienne doświadczenie --- np. słuch u muzyka, dotyk u
ociemniałego itd. --- można z tego wnioskować, jak dalece niektóre
zmysły mogły być ongi bardziej wysubtelnione, niż dzisiaj. Przed kilku
stuleciami dorośli ulegali takim samym złudzeniom wzrokowym, jakim
dzisiaj ulegają dzieci najsamotniejszych wioszczyn. Drżący listek,
powiew wiatru, niewyjaśniony szelest, wywołują ułudny obraz, w którego
rzeczywistość wierzy się niezachwianie. Człowiek tworzy niepodzielną
całość; przeczulenie woli wywołuje nadwrażliwość czucia; jedno
uwarunkowuje drugie i dlatego ludzie, żyjący w czasach rewolucji,
wyrastają nad zwykłą miarą. Byłoby śmiesznem, gdybyśmy dla pozoru
prawdy chcieli mierzyć tych ludzi miarą naszych społeczeństw
współczesnych, bowiem są oni naprawdę półbogami tak w dobrem jak i w
złem.

Legendy bywają niezawsze niedorzecznemi. Ludzie roku 1793 są nam
jeszcze zgoła bliscy, lecz całkiem słusznie stali się przedmiotem
legendy. Śmiesznymi są ci, co sądzą ich niby jakichś poczciwych
mieszczuchów, mających czas do rozważania każdego ranka, jak się ubrać
i co spożyć na obiad, podczas gdy ludzie ci zmuszeni bywali dziesięć
razy na dzień do powzięcia ważnych postanowień, od których zależało
wszystko: los ich idei, a czasem nawet los ojczyzny. Przez bardzo
długi czas historycy dostrzegali w nich tylko część prawdy, a to
dlatego, że nie zwrócili uwagi na fakt, iż prawie każdy z tych
działaczy był jednocześnie poetą, demagogiem, prorokiem, tyranem,
bohaterem, męczennikiem. Pisanie historji równa się nieprzerwanemu
tłumaczeniu i wyjaśnianiu. Ludzie trzynastego wieku nie byli w stanie
oddzielić sprawy zewnętrznej od wewnętrznego stanu ich duszy. To, co
my uważamy za wynik rozważań, im wydawało się dziełem natchnienia;
zamiast naszych słów: pożądanie, popęd, namiętność, używali oni słowa:
pokusa. Atoli różnica w wyrazie nie powinna mylić nas tak dalece,
abyśmy lekceważyli lub uważali za błędną pewną część ich życia
duchowego, oceniając ją ciasnym i zaciemniającym racjonalizmem.

Świętemu Franciszkowi wydawało się nieraz, że walczy z djabłem.
Straszliwe demony piekła etruskiego błąkały się jeszcze po lasach
Umbrji i Toskany, lecz podczas, gdy dla współczesnych i kilku uczni
Franciszka zjawy, cuda i opętania są sprawami codziennemi, dla niego
są one wyjątkiem i pozostają na dalszem tle. W ikonografji świętego
Benedykta i większości świętych popularnych djabeł zajmuje miejsce
wybitne; w życiu świętego Franciszka znika tak zupełnie, że w długiej
serji fresków Giotta w Asyżu nie spotykamy się z nim ani razu.

Podobnież wszelka teurgja i taumaturgja zajmuje w jego życiu miejsce
najpośledniejsze. W Ewangelji Jezus daje swym apostołom moc wypędzania
duchów nieczystych i uzdrawiania wszelkiej choroby; słowa te, tworzące
część jego reguły, Franciszek pojmował z pewnością bez wszelkiej
przenośni. Sądził, że czyni cuda i czynić je usiłował,
lecz jego myśl religijna była zbyt czysta, aby był uważał cud za coś
innego jak środek do łagodzenia cierpień ludzkich. Ani razu nie
uciekł się do cudu jako do środka, mającego uzasadnić prawdziwość
jego posłannictwa lub potwierdzić jego ideję. Jego takt wskazywał mu
na to, że dusze godne są tego, aby je pozyskiwano lepszemi środkami.
Ten zupełny brak cudowności godzien jest tem baczniejszej uwagi, że
sprzeciwiał się zupełnie dążnościom jego wieku.

Czytajcie żywot jego świętego ucznia, Antoniego Padewskiego (zm.\ 1231),
a przekonacie się, że jest on nudnym spisem cudów, uzdrowień,
wskrzeszeń. Możnaby rzec, że jest to raczej prospekt aptekarza, który
wynalazł nowy środek leczniczy, niż wezwanie do nawrócenia i życia
wyższego. Może to interesować chorych i bigotów, lecz nie porwie ani
serca ani sumienia.

Na usprawiedliwienie Antoniego Padewskiego możnaby przytoczyć fakt,
że stosunki jego z Franciszkiem były bardzo nikłe. U uczni pierwszych
czasów, którzy mieli możność przeniknięcia myśli mistrza aż do głębi,
spotykamy tę samą szlachetną pogardę dla cudowności. Wiedzieli oni aż
nadto dobrze, że radość prawdziwa nie polega na zdumiewaniu świata
cudami, na przywracaniu wzroku ślepym, a nawet wskrzeszaniu takich, co
byli martwi przez dni cztery, lecz na tej miłości, która prowadzi aż
do poświęcenia siebie.
<span lang="la">*Mihi absit gloriari nisi in cruce Domini*.</span>[^mihi-absit].

[^mihi-absit]: Gal. 6, 14: Nie daj Boże, abym się chlubić miał jeno
    w krzyżu Pana.

To też brat Egidjusz modlił się do Boga jako o łaskę, aby mu nie dał
czynić cudów; w nich i w namiętności wiedzy widział on sidła, w które
wpadają pyszni, wypaczający prawdziwe posłannictwo Zakonu.

Cuda świętego Franciszka są wszystkie czynami miłości. W czasach
przewrotów krzewią się szczególniej choroby nerwowe, owe cierpienia,
mające pozór tajemniczości, i takich to chorób zleczył Franciszek
najwięcej. Słodycz
jego spojrzenia, pełna współczucia i mocy zarazem, jak gdyby płynąca z
głębi serca, pozwalała częstokroć zapomnieć o cierpieniu tym, którzy
go widzieli.

Oko uroczne jest zabobonem mniej niedorzecznym, niż się pospolicie
mniema. Słusznie rzekł Jezus, iż dość jest spojrzeć, aby zcudzołożyć.
Są atoli inne spojrzenia, jak np. spojrzenie rozmyślającej Marji,
godne wszelkiego poświęcenia, ponieważ w niem człowiek się oddaje,
poświęca i ofiaruje.

Cywilizacja stępiła potęgę spojrzenia. Część wychowania światowego
polega na tem, aby oczy nauczyć kłamać, uczynić je zimnemi i pozbawić
blasku, lecz natury proste nie umieją wyrzec się tej mowy serca,
która promieniami swemi rozdaje życie i zdrowie.

--- Pewien brat --- opowiada Celano --- znosił nieopisane męki;
często z pianą na ustach tarzał się po ziemi, uderzając się o
przeszkody, a widok jego budził grozę; potem ciało jego sztywniało,
a gdy poleżał chwilę wydłużony, wił się i skręcał straszliwie. Czasem
nawet, leżąc na ziemi i nogami dotykając głowy, podrzucany był w górę
na wysokość człowieka. --- Franciszek poszedł go odwiedzić i uzdrowił
go.

Lecz są to wyjątki, naogół bowiem Święty opierał się naleganiom swych
towarzyszy, gdy ci żądali odeń cudu.

Jednem słowem możemy rzec, że pobożność Franciszka polega na ścisłej
łączności jego duszy z boskością, osiągalnej przez modlitwę; żywość, z
jaką ogląda on swój ideał, czyni zeń mistyka. Franciszek znał istotnie
upojenie i wolność mistycyzmu, nie należy atoli zapominać o tem, co go
od mistycyzmu dzieli, mianowicie o jego zapale apostolskim.

W pobożności jego jest kilka rysów osobliwych, na które trzeba zwrócić
uwagę.

Przedewszystkiem jego swoboda względem obserwancji. Franciszek odczuwa
doskonale, ile próżności i pychy mieści się naogół w pobożności. Widzi
on w niej niebezpieczeństwo, ponieważ człowiek, przestrzegający
ściśle przepisy religijne, naraża się na zapomnienie o najwyższym
nakazie miłości. Jeśli zakonnik poddaje się postom nadmiernym i
znajduje źródło uciechy w tem, że jest podziwiany przez prostaczków,
wtedy dzieło pobożności staje się istnym grzechem. To też w
przeciwieństwie do innych założycieli zakonów w regule swojej poszedł
w kierunku złagodzenia obserwancji.

Nie można uważać tego za prosty przypadek, ponieważ musiał nawet
walczyć ze swoimi uczniami, aby zapewnić zwycięstwo swej zasadzie.
Otóż właśnie ci przedewszystkiem, którzy skłonni byli do uchylania
się od przestrzegania ślubu ubóstwa, pragnęli na oczach ludzi
wypełniać niektóre praktyki pobożne.

--- Grzesznik może pościć --- rzekł wówczas Franciszek --- może się
modlić i umartwiać się, lecz nie może być wiernym Bogu. --- Piękne
słowo i godne nawet tego, który głosił cześć Bożą w duchu i w
prawdzie, bez świątyń, bez kapłanów, tak, aby każde ognisko domowe
było ołtarzem, a każdy wierny kapłanem.

W każdej religji formalizm obrzędowy przybiera postać wymuszoną i
ponurą. Faryzeusze wszystkich czasów układają twarze w taki wyraz,
aby nikt nie mógł powątpiewać o ich pobożności. Franciszek nietylko
że nie znosił tych grymasów, ale wesołość i radość zaliczał do
obowiązków religijnych.

Jakże można być smutnym, gdy w sercu ma się niewyczerpany skarb życia
i prawdy, który staje się tem bogatszym, im więcej zeń się czerpie?
Jakże można się smucić, gdy pomimo niezliczonych upadków nie
przestajemy posuwać się naprzód? Dusza wierząca, która rozwija się i
rośnie, doznaje takiej samej radości, jaka jest udziałem dziecka,
uszczęśliwionego tem, iż drobne jego członki, krzepnąc, pozwalają mu
czynić coraz większe wysiłki.

To też słowo "radość" jest tem, które z pióra pisarzy franciszkańskich
spływa najczęściej, mistrz ich bowiem uczynił z radości jeden z
przepisów reguły.

Zbyt dobrym był wodzem, aby nie wiedział, że armja wesoła bywa zawsze
zwycięską. W dziejach pierwszych misyj franciszkańskich
nie brak wybuchów śmiechu, który brzmi szczerze i jasno.

Wieki średnie wyobrażamy sobie częstokroć daleko posępniejszemi, niż
były w rzeczywistości. Cierpiano wtedy wiele, lecz wyobrażenie
cierpienia nigdy nie było oddzielone od wyobrażenia winy. Pojmowano
tedy cierpienia jako pokutę lub doświadczenie, a ból, tak rozumiany,
tracił raniące ostrze. Przenikała go jasność nadziei.

Część radości swojej czerpał Franciszek z komunji. Dla sakramentu
eucharystji miał on cześć, przenikającą niewymownem uczuciem całe jego
jestestwo i napełniającą jego oczy łzami radości. Była to ta cześć,
która niejednej z najpiękniejszych dusz śród ludzkości ułatwiała
znoszenie udręk i niedoli życia. W stuleciu trzynastem litera
dogmatu nie była tak skostniałą, jak obecnie, lecz całe piękno,
prawda i moc utajona w wiekuistym pokarmie, ustanowionym przez
Jezusa, była wtedy żywą we wszystkich sercach.

Eucharystja była naprawdę pokarmem dusz. Jak ongi wędrowcy,
zmierzający do Emaus, tak też w chwilach, gdy rodzą się cienie
wieczorne, gdy nieuchwytne smutki nawiedzają duszę, gdy budzące się
zwidzenia nocne zdają się czaić za każdą naszą myślą, ojcowie nasi
widywali boskiego tajemniczego towarzysza, przybywającego do nich;
pochłaniali jego słowa, czuli moc, zstępującą w ich serca, a gdy cała
ich istota gorzała wnętrznym ogniem, szeptali nanowo: "Zostań z
nami, Panie, boć się ma ku wieczorowi i dzień się już nachylił".[^zostan-z-nami]

[^zostan-z-nami]: Łuk. 24, 29.

Prośby ich bywały często wysłuchane.

