﻿# Stygmaty

:::{.subtitle}
Rok 1224.
:::

Dolina górnego biegu Arno tworzy w samym środku Włoch krainę osobliwą,
Casentino, która w ciągu całych stuleci żyła swojem własnem życiem,
będąc niby wyspą pośród Oceanu.

Arno płynie stąd ku południowi przez wąski przesmyk, a Apeniny okalają
całą krainę pasmem niedostępnych gór.[^dolina-gornego-arno]

[^dolina-gornego-arno]: Przesmyki, wiodące do Casentina,
    znajdują się wszystkie na
    wysokości około 1000 metrów.
    Jeszcze w latach ostatnich nie było tam
    żadnej drogi, zasługującej na to miano.

Równina ta, mająca około czterdziestu kilometrów średnicy, usiana jest
wesołemi miasteczkami, pięknie położonemi na wzgórzach, śród których
wije się Arno. Oto Bibbiena, Poppi, starożytna Romena, opiewana
przez Dantego, Camaldoli, a dalej na wysokim grzbiecie górskim
Chiusi, niegdyś stolica kraju, z ruiną zamku hrabiego Orlanda.

Ludność jest miła i uprzejma: góry osłaniały ją przed wojnami,
wszędzie przeto, gdzie rzucić okiem, dostrzega się tylko ślady pracy,
dostatku i miłego wesela. Co chwila powstaje złudzenie, że jest się w
jednej z dolin Vivarais lub Prowancji. Roślinność nad brzegami Arna
jest wybitnie południowa, drzewa oliwne i morwowe oplata winorośl. Na
najniższych zboczach gór przeplatają się pola zbożowe z łąkami, wyżej
rosną kasztany i dęby, a jeszcze wyżej sosny, świerki, modrzewie, nad
któremi wznoszą się nagie skały.

Wśród wszystkich szczytów jeden osobliwie zwraca na się uwagę; podczas
gdy inne są krągłe i jakby stłoczone, ten wznosi się osobno, smukły i
dumny: to Alwerno.[^alwerno]

[^alwerno]: Góra ta ma 1\ 269 metrów wysokości.
    Po włosku nazywa się <span lang="it">*la Verna*</span>,
    po łacinie <span lang="la">*Alvernus*</span>.
    Etymologia na której ostrzył się dowcip uczonych
    jest dość prosta, słowo <span lang="it">*vernare*</span>,
    używane przez Dantego znaczy ziębić, marznąć.

Zdawałoby się, że to wielki kamień, spadły z nieba. Jest to istotnie
głaz błędny, umieszczony na szczycie, niby skamieniała arka Noego na
górze Ararat. Olbrzymi stóg bazaltu dźwiga na płaszczyźnie swego
wierzchołka wysokie świerki i rozłożyste buki, a wąska ścieżyna jest
jedyną do nich drogą.

Taka-to była ta samotnia, ofiarowana Franciszkowi przez Orlanda, i
niejednokrotnie odwiedzana przez obdarowanego, który szukał na niej
wypoczynku i skupienia.

Siedząc na jednym z kamieni Penny[^penna], mógł słyszeć już tylko wiatr
szumiący w drzewach, lecz w blaskach wschodu lub zachodu mógł widzieć
większość okolic, w których rozsiewał ziarno Ewangelji: Romanję i
Marchję Ankońską, znikające na widnokręgu w falach Adrjatyku; Umbrję,
a nieco dalej Toskanę, gubiące się w wodach morza Śródziemnego.

[^penna]: Nazwa najwyższego punktu szczytowej płaszczyzny.
    Oddalona zaledwie o trzy kwadranse drogi od klasztoru
    a nie o dwie i pół godziny jak to mniemają zacni mnisi.
    Informacją tą służymy turystom... i pielgrzymom.

Wrażenie na tej górze nie jest przygnębiające jak w Alpach: wędrowca
otacza tu niewymowna słodycz i spokój; jest się dość wysoko, aby
spoglądać na ludzi z góry, lecz niedość, aby zapomnieć o ich
istnieniu.

Prócz rozległego widnokręgu miał tu Franciszek wiele innych
przedmiotów zachwytu. W lesie tym, jednym z najpiękniejszych w
Europie, gnieżdżą się legjony ptactwa, które nie płoszone przez
nikogo, jest dziwnie poufałe. Subtelne wonie płyną od ziemi, okrytej
borakami i liszajowcami, śród których krzewi się fantastyczne mnóstwo
subtelnych, wiotkich cyklamenów.

Franciszek zapragnął powrócić tutaj po kapitule r.\ 1224. Zgromadzenie
to, odbyte na początku czerwca, było ostatniem, w którem uczestniczył.
Nowa reguła została na niem przekazana ministrom, a misja do Anglji
postanowiona.

W pierwszych dniach sierpnia Franciszk był w drodze ku Alwerno. Szło z
nim tylko kilku braci, Maciej, Anioł i Leon. Pierwszy z nich miał za
obowiązek prowadzić małą tę gromadkę i starać się, aby nie musiała
myśleć o niczem prócz modlitwy.

Po dwóch dniach podróży trzeba było postarać się o osła dla
Franciszka, który był tak osłabiony, że nie mógł podróżować pieszo.

Starając się o tę przysługę, bracia nie zatajali, oczywiście, imienia
mistrza swego, to też wieśniak, do którego zwrócono się, uważał, że
powinien sam poprowadzić osła. Przebywszy kawałek drogi, zapytał: ---
Czy to prawda, że jesteś bratem Franciszkiem z Asyżu? Radzę ci tedy,
--- dodał po otrzymaniu odpowiedzi twierdzącej --- abyś się starał być
tak dobrym, za jakiego cię ludzie uważają, iżby nie zostali zawiedzeni
w swych oczekiwaniach. --- Franciszek zsiadł natychmiast z osła i
padłszy przed wieśniakiem, dziękował mu z wielką serdecznością.

Tymczasem nadeszła najgorętsza pora dnia. Wieśniak, wyczerpany
wędrówką, zapominał powoli o radości z niespodziewanego spotkania.
Bowiem spiekota słoneczna budzi udrękę pragnienia nawet wtedy, gdy się
kroczy po boku świętego. Zaczynał już żałować swej usłużności, gdy
Franciszek palcem wskazał mu źródło dotąd nieznane i nie odnalezione
później.

Stanęli wreszcie u stóp ostatniego urwiska. Zanim jęli wspinać się na
nie, zatrzymali się pod wielkim dębem dla wypoczynku, i wnetże gromady
ptactwa zleciały się, okazując swą radość łopotem skrzydeł i
śpiewaniem. Poskakując dokoła Franciszka, siadały mu na głowie,
barkach i ramionach. --- Widzę --- rzekł uradowany do swych
towarzyszy, --- iż Pan nasz Jezus Chrystus chce, abyśmy
zamieszkali na tej górze samotnej, ponieważ bracia i siostry ptaki
okazują tak wielką radość z naszego przybycia.

Góra ta stała się dla niego Taborem i Kalwarją zarazem, nie należy
przeto dziwić się, że zakwitały tu legendy jeszcze liczniejsze, niż w
całem poprzedniem jego życiu. Większość tych legend oddycha czarem
kwiatuszków różowych i pachnących, przytulonych wstydliwie do stóp
jodeł Alwernu.

Wieczory letnie są tutaj niezrównanie piękne; przyroda, jakby duszona
żarem dnia, ożywa wieczorem na nowo. W drzewach, załomach skał, w
murawie, budzą się tysięczne głosy i dostrajają harmonijnie do poszumu
wielkich lasów, lecz śród tych wszystkich głosów żaden się nie
wyróżnia, żaden nie zakłóca melodji, kołyszącej do marzeń. Oko błądzi
po horyzoncie, płonącjon hieratycznemi barwami w ciągu długich godzin
po zachodzie słońca, a szczyty Apeninów, tęczujące blaskami, budzą w
duszy to, co poeta franciszkański nazwał tęsknotą za wiecznemi
wzgórzami.

Franciszek odczuwał ją żywiej od kogokolwiek. Już pierwszego wieczoru
po przybyciu, siedząc w kole braci na wzgórku, zalecał im pobyt na tem
miejscu.

Zaduma natury budziła w sercach smutek, a głos mistrza zestrajał się
z tkliwością ostatnich blasków dnia. Mówił braciom o swej bliskiej
śmierci z żalem robotnika, zaskoczonego mrokiem nocy przed
dokończeniem dzieła, z westchnieniami ojca, drżącego o przyszłość
swych dzieci.

Sam on pragnął przygotować się na śmierć modlitwą i rozmyślaniem,
prosił ich więc, aby go chronili przed natrętami. Orlando, który już
udał się był do nich, aby ich powitać i ofiarować im swoje usługi,
kazał na życzenie Franciszka zbudować dlań naprędce chatkę z gałęzi
pod rozłożystym bukiem. Tutaj postanowił przebywać w odległości rzutu
kamieniem od cel, zamieszkanych przez towarzyszy. Brat Leon miał
obowiązek dostarczać mu co dzień wszystkiego potrzebnego.

Po tej pamiętnej rozmowie usunął się natychmiast do swej chatki, lecz
w kilka dni później, niepokojony zapewne przez pobożną ciekawość
braci, usunął się dalej w głąb lasu i rozpoczął w dniu Wniebowzięcia
Marji Panny post na cześć świętego Michała Archanioła i wojska
niebieskiego.

Genjusz ma wstydliwość miłującego serca. Poeta, artysta, święty,
potrzebują samotności, gdy nawiedza ich Duch. Wszelki wysiłek myśli,
wyobraźni, woli, jest modlitwą, a modlący się szuka ustronia.

Biada człowiekowi, który w głębi serca nie posiada żadnej z tych
tajemnic, ukrywanych przed wszystkimi, ponieważ niepodobna wyrazić
ich słowem, a wyrażone, zostają niezrozumiałemi.
<span lang="la">*Secretum meum mihi!*</span>[^secretum-meum-mihi]
Jezus odczuwał to dobrze: upojenia Taboru są krótkotrwałe;
nie powinno się o nich opowiadać.

[^secretum-meum-mihi]: Tajemnica moja do mnie należy.

Wobec tych tajemnic duszy materjaliści i dewoci zachowują się
częstokroć jednakowo, domagając się ścisłości w sprawach, w których
ona najmniej jest możliwą.

Wierzący zapytuje, w którym zakątku Alwernu otrzymał Franciszek
stygmaty, czy serafin, który mu się ukazał, był to Jezus czy duch
niebiański i co mówił, wyciskając mu owe znamiona? I nie rozumie już
owej godziny, w której Franciszek uginał się po nadmiarem bólu i
miłości, jak nie rozumie jej materjalista, chcący widzieć i dotykać
ziejącej rany.

Spróbujmy uniknąć takiej przesady. Posłuchajmy, co mówią dokumenty i
nie zadawajmy im gwałtu, aby wydrzeć im to, o czem nie mówią i o czem
mówić nie mogą.

Przedstawiają nam one Franciszka, dręczonego niepokojem o przyszłość
Zakonu i bezmiernem pragnieniem czynienia nowych postępów duchownych.

Pożerany był żarliwością świętych, potrzebą ofiary, która wydarła
świętej Teresie jej okrzyk namiętny: "Cierpieć,
lub umrzeć!" Gorzko wyrzucał sobie, iż okazał się niegodnym męczeństwa
i że nie mógł oddać się za Tego. który oddał się za nas.

Dotykamy tutaj jednego z pierwiastków najpotężniejszych i najbardziej
tajemniczych życia chrześcijańskiego. Można go nie rozumieć, ale nie
należy zaprzeczać jego istnienia. Jest on korzeniem prawdziwego
mistycyzmu. Wielką nowiną, przyniesioną światu przez Jezusa, było to,
że, czując się doskonale zjednoczonym z Ojcem niebieskim, wzywał
wszystkich ludzi do połączenia się z nim, a przezeń z Bogiem. "Jam
jest winna macica, wyście latorośle. Kto mieszka we mnie, a ja w nim,
ten wiele owocu przynosi, bo beze mnie nic czynić nie możecie".[^jan-15]

[^jan-15]: Jan 15, 5.

Chrystus nietylko wzywał do tej jedności, ale dał jej odczucie.
Wieczorem w przeddzień swej śmierci ustanowił jej sakrament, a niema
bodaj sekty, któraby nie wyznawała, że komunja jest symbolem,
podstawą i celem życia religijnego zarazem. Od dziewiętnastu stuleci
chrześcijanie, różniący się we wszystkich innych sprawach, nie
przestawali spoglądać ku temu, który ustanowił obrzęd czasów nowych.

W przededniu zgonu wziął chleb, a łamiąc go, rozdawał uczniom i
mówił: *"Bierzcie i jedzcie, bo to jest ciało moje"*.

Przedstawiając jedność z sobą, jako podstawę życia nowego, Jezus
pragnął pouczyć braci swoich, że jedność ta polega na uczestnictwie w
jego pracach, walkach i cierpieniach: "Kto chce być uczniem moim,
niechaj weźmie krzyż swój i naśladuje mnie".

Święty Paweł przeniknął pod tym względem tak dobrze myśl Mistrza, że
w kilka lat później mógł był wydać okrzyk mistycyzmu, do jakiego nikt
po nim nie dotarł: "Z Chrystusem jestem ukrzyżowany, a żyję nie ja,
ale żyje we mnie Chrystus!" Okrzyk ten nie jest u niego słowem
odosobnionem, jest on samym ośrodkiem jego świadomości religijnej;
ba, mówi on jeszcze więcej, narażając się na to,
że zgorszy niejednego chrześcijanina: "Dopełniam ostatków ucisków
Chrystusowych na ciele mojem za ciało jego, które jest Kościół."[^kolos-i-24]

[^kolos-i-24]: Do Kolos. I, 24.

Sądzę, że nie było zbędnem przytoczenie tych szczegółów dla ukazania,
jak dalece Franciszek, odnawiający w ciele swojem mękę Chrystusa,
zbliża się do tradycji apostolskiej.

W pustelniach Alwernu, jak ongi u świętego Damjana, Jezus ukazywał się
mu w postaci Ukrzyżowanego, Męża boleści.

Że wylewy jego uczuć zostały nam przekazane w formie poetyckiej i
niedokładnej, nie powinno nas zadziwiać; byłoby dziwnem, gdyby było
inaczej. W porywach miłości Bożej są *ineffabilia*, które
nietylko że nie mogą być opowiedziane i zrozumiane, lecz nawet
przypomniane przez tego, kto ich doznawał.

Na górze Alwerno Franciszka pożerało bardziej, niż kiedykolwiek,
gorące pragnienie cierpienia dla Jezusa i z Jezusem. Dni jego
upływały na pobożnych ćwiczeniach w skromnej kapliczce, zbudowanej na
górze, i na rozmyślaniach w głębi lasów. Zdarzało mu się nawet, że,
zapominając o kaplicy, całe dni trawił samotny w jakiemś skalnem
ustroniu i w sercu swem przeżywał pamiątkę Golgoty. Innym razem całe
godziny spędzał u stóp ołtarza, rozczytując się w Ewangelji i prosząc
Boga, aby ukazał mu drogę, którąby iść miał.

Księga otwierała się prawie zawsze w miejscu, opowiadającem o męce
Pańskiej, a ta prosta zbieżność, dość zresztą zrozumiała, wywoływała w
nim zaniepokojenie.

Wizja Ukrzyżowanego opanowywała tem bardziej wszystkie jego myśli, że
zbliżało się święto Podwyższenia Krzyża świętego (14\ września), święto
dzisiaj podrzędne, lecz w trzynastem stuleciu obchodzone z gorliwym
zapałem jako uroczystość, patronująca krucjatom.

Franciszek podwoił surowość postów i żar modlitw, całkowicie
przemieniony w Jezusa przez miłość i litość ---
powiada jedna z legend. Noc poprzedzającą święto spędził samotny na
modlitwie, niedaleko pustelni.

Gdy nadszedł ranek, miał widzenie. W ciepłych promieniach
wschodzącego słońca, które po chłodzie nocy ożywiło jego ciało,
dostrzegł nagle dziwną postać.

Serafin na rozpostartych skrzydłach leciał ku niemu z przestworza i
napełniał go rozkoszą niewymowną. Pośrodku widzenia ukazał się krzyż,
a serafin był doń przybity. Gdy wizja zniknęła, Franciszek po rozkoszy
pierwszej chwili uczuł ostry ból. Zmieszany aż do głębi swej istoty,
zastanawiał się nad znaczeniem tego wszystkiego, gdy w tem ujrzał na
ciele swojem stygmaty Ukrzyżowanego.

