﻿# Stopnie nawrócenia

:::{.subtitle}
Od wiosny 1204 do wiosny 1206.
:::

Po powrocie do Asyżu, Franciszek rozpoczął nanowo dawne życie; być
może, iż chciał nawet powetować sobie czas stracony. Zabawy, gry,
biesiady, rozkiełzanie, zaczęły się nanowo. Oddał się tak
niepodzielnie uciechom, aż zaniemógł poważnie.

W ciągu długich tygodni widział śmierć z tak bliska, że przesilenie
fizyczne sprowadziło przesilenie moralne. Tomasz Celano zachował
pewien rys z czasów rekonwalescencji Franciszka: odzyskiwał on powoli
siły i zaczynał wychodzić z domu, gdy dnia pewnego postanowił pójść
dalej, aby spokojnie nacieszyć się widokiem przyrody i nanowo wziąć w
posiadanie skarb życia. Wsparty na lasce, udał się w stronę bramy
miejskiej.

Najbliższą bramą, nazwaną *Porta Nuova*, jest właśnie ta, za którą
roztaczają się najpiękniejsze widoki. Wnet po wyjściu za bramę ma się
przed sobą wielką równinę; wypukłość terenu zakrywa oczom miasto, od
którego nie dochodzi żaden szmer. Przed spoglądającym biegnie w
licznych załomach droga do Foligno; w lewo imponujące masy góry
Subasio; w prawo cały rozdół umbryjski ze swemi osadami rolnemi,
wioskami, omglonemi wzgórzami, a na zboczach tych wzgórków sosny,
cedry, dęby, oliwki i winna latorośl roztaczają niezrównane wesele i
ożywienie. Cała kraina polśniewa pięknem, lecz pięknem zestrojonem
harmonijnie
i zgoła ludzkiem, jakby uczynionem dla podobania się człowiekowi.

Franciszek miał nadzieję, że w widoku tym odnajdzie rozkoszne wrażenia
swych lat pacholęcych. Z podnieconą wrażliwością ozdrowieńców wdychał
wiewy wiośniane, ale oczekiwana odnowa wewnętrzna nie nadchodziła.
Cała roześmiana wokół przyroda miała dlań tylko wyrazy melancholji.

Sądził, że powiewy tej ukochanej krainy zabiorą z sobą ostatnie
dreszcze gorączki, a oto sercem jego zawładnęło przygnębienie po
stokroć boleśniejsze od cierpienia cielesnego. Żałosna pustka jego
życia ukazała mu się znagła, przeraziła go samotność wielkiej duszy, w
której brak ołtarza.

Wspomnienia życia minionego obiegły go nieznośną żałością: zaczynał
się brzydzić samym sobą; niegdysiejsze jego ambicje wydawały mu się
śmiesznemi, lub godnemi wzgardy. Powrócił do domu pod przygnębiającem
brzemieniem nowego cierpienia.

W godzinach takiej udręki moralnej człowiek szuka ucieczki w miłości,
lub w wierze.

Niestety rodzina i przyjaciele Franciszka nie zdolni byli go
zrozumieć. Co do religji, to była ona dla niego, jak dla większości
jego współczesnych, owym grubym fetyszyzmem o terminologji
chrześcijańskiej, który pewno nieprędko zniknie zupełnie. Dla
niektórych ludzi pobożność polega na tem, aby być w porządku w
stosunku do króla potężniejszego od innych, ale także bardziej
surowego i kapryśnego, którego nazywa się Bogiem. Przekonywa się go o
swej lojalności, jak się przekonywa innych monarchów, a mianowicie
tem, że się rozwiesza wszędzie jego obrazy i składa możliwie
regularnie podatki, pobierane przez jego ministrów. Kto skąpi, kto
oszukuje, ten naraża się na surową karę, ale dokoła króla nie brak
dworzan, którzy chętnie służą w potrzebie. Za odpowiednią nagrodą
podpatrzą chwilę pomyślną, aby zręcznie usunąć wyrok skazujący, albo w
chwili dobrego humoru księcia podsuną mu do podpisu
formułę zupełnego rozgrzeszenia, wiedząc, że w takich chwilach książę
podpisuje, nie patrząc.

W takiej pierwotnej religji żył dotąd Franciszek. Nie pomyślał więc
nawet, aby w niej szukać balsamu duchowego na zleczenie swych ran.
Pod świętym przymusem miał dojść do wiary czystej i śmiałej, lecz
droga do niej jest długa i usiana przeszkodami, a w chwili, o której
mowa, jeszcze się na niej nie znajdował, ba, nie podejrzewał nawet
jej istnienia. Wiedział tylko, że uciechy prowadzą do nicestwa, a
przesyt do gardzenia samym sobą.

Wiedział o tem, a jednak miał na nowo powrócić ku uciechom. Ciało
nasze jest tak słabe i tak skłonne do powracania na wydeptane ścieże,
że powraca na nie chętnie, jeśli energiczna wola nie oprze się temu.
Acz rozczarowany, Franciszek powrócił do dawnego życia.

Chciałże się rozerwać, zapomnieć o owym dniu gorzkich dumań? Możnaby
przypuścić, że tak, sądząc podług zapału, z jakim rzucił się do
urzeczywistniania swoich nowych zamierzeń.

Do realizacji marzeń o sławie nadarzała mu się sposobność: pewien
rycerz z Asyżu, być może jeden z tych, z którymi dzielił niewolę w
Perugji, wybierał się w podróż do Pulji pod rozkazy hrabiego Gentile,
który połączyć się miał z Gauthierem de Brienne, wojującym na południu
Włoch po stronie Inocentego III.

Sława Gauthiera była ogromna na całym półwyspie: uważano go za jednego
z najprzedniejszych rycerzy epoki. Serce Franciszka zadrżało: wydało
mu się, że przy boku podobnego bohatera niebawem będzie mógł okryć się
chwałą. Postanowił wyjechać i oddał się bezgranicznej radości.

Przygotowania do drogi czynił z ostentacyjną rozrzutnością. Jego
rynsztunek stał się dla swej książęcej wspaniałości przedmiotem
rozmów. Mówiono o nim tem więcej, że naczelnik wyprawy, zrujnowany
zapewne przez rewolucję r.\ 1202, lub wydatkami w długiej niewoli,
musiał poprzestać na daleko skromniejszym rynsztunku.

Lecz we Franciszku dobroć była już daleko większa, niż upodobanie
stroju. Swój ubiór wspaniały oddał rycerzowi
ubogiemu. Biografowie nie mówią nam, czy obdarowany był tym, któremu
Franciszek miał towarzyszyć.

Spoglądając na jego zabiegi i hałaśliwe przygotowania, można było
mniemać, że to syn wielkiego pana. Towarzysze jego czuli się
niezawodnie dotknięci takiem jego postępowaniem i postanowili
upokorzyć go okrutnie. Sam on ani przypuszczał, że budzi zawiść, i
dniem i nocą myślał tylko o przyszłej swej sławie. W marzeniach
widziało mu się, że jego dom rodzicielski uległ całkowitej przemianie:
miast gromad tkanin mniemał widzieć dokoła siebie błyszczące tarcze,
pozawieszane na ścianach obok broni wszelkiego rodzaju, niby w pałacu
wielkopańskim. Samego siebie widział w tym śnionym pałacu przy boku
pięknej i szlachetnie urodzonej małżonki i nie wątpił, że wizja ta
jest przepowiednią oczekującej go przyszłości. Nie widziano go też
nigdy dotąd tak rozmownym i promieniejącym, jak obecnie, a gdy
dopytywano go bezustannie, skąd pochodzi ta jego radość, odpowiadał
z zadziwiającą pewnością: "Wiem, że stanę się wielkim księciem".

Nadszedł nareszcie dzień wyjazdu. Franciszek na koniu, mając na
ramieniu małą tarczę paziowską, pożegnał z radością miasto rodzinne i
z małym swym orszakiem ruszył drogą, wijącą się po zboczach góry
Subasio, ku Spoleto.

Tymczasem cóż się stało? Źródła milczą, ograniczając się do
zaznaczenia, że tegoż samego dnia wieczorem Franciszek miał widzenie,
które skłoniło go do powrotu do Asyżu. Może będziemy niedalecy od
prawdy, jeśli przypuścimy, że na samym początku drogi Franciszek padł
ofiarą młodych panków, którzy zemścili się na synu Bernardonego za
jego książęce poczynanie sobie. Bo takich rzeczy nie darowują sobie
młodzieńcy dwudziestoletni; jeśli, jak twierdzą świadomi, uciechą jest
wyprowadzenie w póle człowieka nawet nieznanego, to okpienie młodego
franta musi być niemal boską rozkoszą dla ludzi mniemających, iż
wymierzają mu słuszną karę.

Przybywszy do Spoleto, Franciszek położył się do łóżka. Trawiła go
gorączka; w przeciągu kilku godzin marżenia jego rozpadły się w gruzy.
Nazajutrz był w drodze powrotnej do Asyżu.

Tak nieoczekiwany powrót jego narobił hałasu w małem miasteczku i był
bolesnym zawodem dla jego rodziców. Franciszek zdwoił miłosierdzie
względem ubogich i starał się unikać ludzi, lecz towarzysze jego
pozbiegali się niebawem ze wszystkich stron, w nadziei, że odnajdą w
nim niewyczerpane źródło zaspakajania ich szaleńczych potrzeb. Uległ
im.

Tym razem jednak zaszła w nim wielka zmiana. Ani uciechy, ani praca
nie mogły przykuć go do siebie na długo; część dnia spędzał zwykle na
błądzeniu po polach, częstokroć w towarzystwie pewnego przyjaciela,
bardzo niepodobnego do tych, jakich widywaliśmy dotąd dokoła niego.
Nie znamy go z imienia, lecz wedle pewnych oznak możnaby przypuścić,
że chodzi o Bombarona da Beviglie, przyszłego brata Eljasza.

Rozmyślania, rozpoczęte nazajutrz po chorobie, podjął na nowo, lecz
już z mniejszą goryczą. Jego serce i jego przyjaciel zgodnie
przekładali mu, że można przestać myśleć o uciechach i sławie a jednak
znaleźć sprawy godne poświęcenia im życia. Jest to chwila, w której
zdaje się w nim budzić myśl religijna. Od czasu, gdy zaczął
dostrzegać tę nową drogę, chciał się rzucić na nią w niepowstrzymanym
pędzie, jak na wszystko, co czynił. Bezustannie zapraszał swego
powiernika i zabierał go z sobą na ścieżki najustronniejsze.

Lecz walki wewnętrzne są niewypowiedzianie ciężkie. Walczący jest sam
i cierpi sam. Są to walki, toczone w nocy, tajemne i samotne, jak
walka w Bethel.

Dusza Franciszka była sposobna do toczenia tego tragicznego
pojedynku, a przyjaciel jego cudownie pojął rolę, jaka mu przypadła w
udziale. Udzielił Franciszkowi kilka rad, ale ostatecznie ograniczał
się do okazywania mu współczucia, idąc za nim wszędzie i nie żądając
dowiedzieć się więcej, niż mu Franciszek mógł powiedzieć.

Często zachodził Franciszek do groty w okolicy Asyżu, w której
siadywał sam. Ta jaskinia skalna, ukryta śród oliwek
miała stać się dla wiernych franciszkanów tern, czern Getsemane jest
dla chrześcijan.

Franciszek wylewał tam nadmiar swego serca w długich narzekaniach i
westchnieniach. Czasem na wspomnienie źle strawionej młodości ulegał
napadom trwogi, lecz najczęściej myśl jego zwracała się ku
przyszłości. Gorączkowo poszukiwał tej prawdy najwyższej, której
oddać się pragnął, szukał tej perły nieocenionej, o której mówi
Ewangelja: "Kto szuka, ten znajduje, kto prosi, otrzymuje, kto puka,
temu bywa otworzone".

Bladość jego twarzy, bolesny skurcz jego rysów mówiły aż nadto
wyraźnie, gdy po długich godzinach opuszczał jaskinię, jak żarliwemi
były jego prośby, jak mocnem pukanie.

Człowiek wewnętrzny, mówiąc językiem mistyków, nie był w nim
ukształtowany, ale potrzebny już był tylko drobny przypadek, aby
ostateczne zerwanie z przeszłością zostało dokonane; przypadek taki
zdarzył się niedługo.

Przyjaciele jego nie przestawali zabiegać o to, aby go skłonić do
rozpoczęcia na nowo dawnego życia. Dnia pewnego Franciszek zaprosił
ich wszystkich na wystawną ucztę. Mniemając, że odnieśli zwycięstwo,
obwołali go, jak zwykle, królem uczty.

Biesiada przeciągła się do późnej nocy, poczem biesiadnicy wyroili
się na ulice, napełniając je swemi śpiewami i zgiełkiem. Nagle
spostrzegli, że Franciszka śród nich niema. Po długich poszukiwaniach
znaleźli go wreszcie bardzo daleko, trzymającego jeszcze w dłoni berło
błazeńskiego króla, ale pogrążonego w tak głębokiej zadumie, iż
wydawał się przyrośniętym do ziemi i zgoła nieczułym na to, co się
działo dokoła niego.

--- Cóż to ci się stało? -- pokrzykiwali, biegając dokoła niego, aby
go zbudzić z zadumy. --- Czyż nie widzicie, że zamyśla pojąć żonę? ---
ozwał się głos jednego z towarzyszy zabawy. --- Tak jest --- odparł
Franciszek, zwracając się ku mówiącemu z uśmiechem, jakiego dotąd na
jego ustach nie widziano --- zamierzam pojąć żonę, piękniejszą,
bogatszą, czystszą, niż moglibyście sobie wyobrazić.

Odpowiedź ta oznacza w jego życiu wewnętrznem stopień rozstrzygający.
Tą odpowiedzią zerwał ostatnie nici, jakie łączyły go z uciechami
pospolitemi. Popatrzmy teraz, śród jakich walk, po zerwaniu ze
światem, zdołał oddać się Bogu.

Przyjaciele jego prawdopodobnie nie zrozumieli tego, co się stało, ale
odgadli przepaść, jaka rozwarła się między nim a nimi. Niebawem
wiedzieli już, czego się trzymać.

On sam zaś, nie potrzebując już oglądać się na nic, oddał się
bardziej, niż kiedykolwiek, swej namiętności samotnictwa.

Chociaż często opłakiwał zmarnowane lata życia i dziwił się, jak mógł
był nie czuć całej goryczy, ukrytej na dnie czarownego kielicha, to
jednak nie dał się pognębić daremnym żalom.

Ubodzy dochowali mu wiernej przyjaźni. Wiedział, że mają dla niego
takie uwielbienie, jakiego nie czuł się godnym, lecz było ono dlań
źródłem nieopisanej błogości. Dzięki ich wdzięczności, dzięki tej
przyjaźni nieśmiałej, która nie odważyła się i nie umiała się
wypowiedzieć, lecz którą wyczuwało jego serce, przyszłość stawała się
jasną; na cześć ich, dzisiaj niezasłużoną, zasłuży jutro, wszystko
uczyni, aby ją usprawiedliwić.

Dla zrozumienia tych uczuć trzeba sobie przedstawić, czem byli ubodzy
w takiej miejscowości, jak Asyż.

W kraju rolniczym ubóstwo nie pociąga za sobą z nieuniknioną
koniecznością zubożenia moralnego, znieprawiającego całą istotę ludzką
i czyniącego dzieło miłosierdzia tak trudnem.

Większość ubogich, których znał Franciszek, popadła w nędzę skutkiem
wojny, złych urodzajów, lub choroby. W wypadkach podobnych pomoc
materjalna jest rzeczą mniejszej wagi, bo ludziom takim potrzebny jest
przedewszystkiem wyraz życzliwości i współczucia. Franciszek miał w
tym względzie całe skarby do rozrzucenia między ubogich.

Odpłacono mu tę rozrzutność wzajemnością. Wszystkie niedole są
siostrami. Pomiędzy cierpieniami najróżniejszego pochodzenia powstaje
tajemne porozumienie. Ubodzy czuli, że ich przyjaciel także cierpi;
nie rozumieli dobrze dlaczego, lecz zapominali o własnym smutku,
współczując swemu dobroczyńcy. Boleść jest cementem miłości. Aby
kochać prawdziwie, trzeba zmieszać łzy własne ze łzami ukochanego.

Dotąd nie widzieliśmy, aby na Franciszka oddziałał był wpływ ściśle
kościelny. W sercu jego działał bezwątpienia ów zakwas wiary
chrześcijańskiej, który przenika nas nawet bez naszej wiedzy; lecz
praca przemiany wewnętrznej, która dokonywała się w nim, była dotąd
owocem intuicji osobistej.

Okres ten dobiegał końca. Myśl jego miała się ujawnić, a tem samem
miała otrzymać pieczęć okoliczności. W nauce chrześcijańskiej znajdzie
Franciszek wskazania, które dadzą formy ścisłe jego myślom dotąd
niejasnym, lecz jednocześnie znajdzie w niej także ramy, w których
myśl jego utraci nieco ze swej pierwotności i swej mocy: wino nowe
wlane zostanie do miechów starych.

Tymczasem zapanowywał w nim spokój; w kontemplacji przyrody znajdował
rozkosze, jakich doznawał ongi przelotnie, niemal nieświadomie, a
które obecnie nauczył się oceniać. Czerpał z nich nietylko spokój;
czuł, że w sercu jego rodzą się nowe współczucia, a wraz z niemi
potrzeba działania, poświęcenia się, nawoływania do zgody i miłości
tych miast, które rozsiadłszy się na wierzchołkach wzgórków,
spoglądały na siebie wrogo, jak bojownicy przed walką.

W chwili tej Franciszek z pewnością nie przewidywał nawet, czem sam
się stanie; lecz godziny te są niezawodnie najważniejszemi dla rozwoju
jego myśli. One to dały wolność jego poczynaniu, one owiały go wonią
pól, czyniąc myśl jego niepodobną zarówno do pobożności zakrystji, jak
i do pobożności salonów.

Mniej więcej w tym czasie odbył pielgrzymkę do Rzymu. Czy uczynił to
na skutek rady swego przyjaciela, czy był to akt pokuty, zadanej przez
spowiednika, czy wreszcie czyn samorzutny? Nie wiadomo. Sądził, być
może, iż odwiedzenie Świętych Apostołów, jak ongi mawiano, pozwoli mu
znaleźć odpowiedzi na wszystkie te pytania, które sobie zadawał.

Udał się tedy do Rzymu. Czy uległ tam pewnym wpływom religijnym? Jest
to możliwe, ponieważ biografowie jego opowiadają o bolesnem jego
zdziwieniu, jakiego doznał w bazylice Świętego Piotra, widząc, jak
skąpemi były ofiary pielgrzymów. Dlatego postanowił oddać księciu
apostołów wszystko, co miał i całą zawartość swego mieszka rzucił na
jego grobowiec.

Podróż ta zaznaczyła się wydarzeniem jeszcze ważniejszem. Częstokroć,
pocieszając ubogich, zadawał sobie pytanie, czy umiałby znosić nędzę;
nie zna się ciężaru brzemienia, jeśli się go przynajmniej na chwilę
nie wzięło na własne barki. Postanowił tedy przekonać się, jak to
jest, gdy się nie ma nic i gdy kawałek chleba otrzymuje się z rąk
miłosierdzia lub kaprysu przechodniów.

W przysionku bazyliki roiły się chmary żebraków; od jednego z nich
pożyczył sobie łachmany, dając mu w zamian swoje ubranie, i w
przeciągu całego dnia wyciągał ręę, zgłodniały.

Czyn ten był wielkiem zwycięstwem: był to triumf współczucia nad pychą
przyrodzoną. Po powrocie do Asyżu podwoił dobroć względem tych,
których zaiste prawdziwym był bratem.

Żywiąc uczucia podobne, rychło już musiał podpaść pod wpływ Ewangelji.

Przy wszystkich drogach okolicy miasta było wówczas, jak obecnie,
wiele kaplic. W wiejskich tych świątyńkach zdarzało mu się bardzo
często słuchać mszy sam na sam z odprawiającym ją kapłanem. Jeśli
weźmiemy pod uwagę skłonność natur prostych do przejmowania się
wszystkiem, co słyszą, jakby to się do nich osobiście odnosiło,
to zrozumiemy, jakich wzruszeń i uczuć doznawać musiał, gdy ksiądz,
odczytując ewangelję, zwracał się ku niemu. Ideał chrześcijański
objawiał mu się, przynosząc odpowiedzi na pytania jego tajemnych
zainteresowań. To też, gdy w kilka chwil później błądził śród drzew,
wszystkie myśli jego biegły ku ubogiemu Cieśli z Nazaretu, który
stając na jego drodze, mówił doń: "Ty pójdź za mną!"

Około dwóch lat upłynęło od chwili, w której doznał pierwszego
wstrząśnienia: życie pełne wyrzeczeń ukazywało mu się jako cel jego
wysiłków, czuł jednakże, że jego nowicjat duchowy nie jest skończony.
Pouczyło go o tem nagłe a gorzkie doświadczenie.

Dnia pewnego odbywał przejażdżkę konną, zajęty bardziej, niż
kiedykolwiek myślami o życiu zupełnego poświęcenia się, gdy na
skręcie drogi spotkał się oko w oko z trędowatym. Straszliwa choroba
budziła w nim zawsze odrazę nieprzezwyciężoną. Nie mógł się
powstrzymać od poruszenia, wyrażającego grozę i odruchowo ściągnął
lejce.

Wstrząs był potężny, upadek zupełny. Franciszek czynił sobie gorzkie
wyrzuty. Żywić tak piękne zamiary i okazać się tak małodusznym! Także
to łatwo rycerz Chrystusowy ma złożyć broń? Zawrócił na miejscu i
zeskoczywszy z konia, oddał zdumionemu nieszczęśliwcowi wszystkie
posiadane pieniądze, poczem ucałował jego rękę, jakby całował rękę
kapłana.

Nowe to zwycięstwo było w jego życiu duchowem --- jak to sam pojmował
--- wydarzeniem doniosłem.

Daleką bowiem jest droga od nienawidzenia zła do miłowania dobra.
Więcej, niż przypuszczamy, jest takich, którzy po bolesnych
doświadczeniach wyrzekli się tego, co dawne liturgje nazywają światem,
wraz z okazałością i pożądaniami jego, lecz większość śród nich nie
posiada w głębi serca ani odrobiny czystej miłości. Rozczarowania
pozostawiają w duszach pospolitych tylko straszliwe sobkostwo.

Zwycięstwo to było tak nagłem, że Franciszek postanowił je uzupełnić;
w kilka dni później udał się do przytuliska dla trędowatych.

Można sobie wyobrazić zdumienie nieszczęśliwych na widok
przybywającego do nich wspaniałego rycerza. Jeśli w dniach naszych
odwiedziny w szpitalu są dla chorych wielkiem wydarzeniem,
wyczekiwanem z gorączkową niecierpliwością, czemże musiało być
ukazanie się Franciszka wśród tych biednych wydziedziczonych! Trzeba
znać chorych opuszczonych, aby wiedzieć, czem bywa dla nich serdeczne
słowo, a czasem proste spojrzenie.

Wzruszony i uniesiony, Franciszek czuł, jak cała jego istota wnętrzna
drga wrażeniami dotąd niedoznawanemi. Po raz pierwszy słyszał
niewymowne wygłosy uznania, nie znajdującego słów dość gorących dla
wypowiedzenia się, uznania pełnego zachwytu i uwielbiającego
dobroczyńcę, niemal jak anioła przybywającego z nieba.

